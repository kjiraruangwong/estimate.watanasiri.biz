    function customerChange() {
        var customerId = $("#customerId").val();
        $.ajax({
            url: 'getProjects',
            type: 'post',
            data: {customerId: customerId},
            dataType: 'json',
            success: function (response) {
                var len = response.length;
                $("#projectId").empty();
                $("#projectId").append('<option value="">-- กรุณาเลือก --</option>');
                for (var i = 0; i < len; i++) {
                    var id = response[i]['id'];
                    var name = response[i]['name'];
                    $("#projectId").append("<option value='" + id + "'>" + name + "</option>");
                }
            }
        });

        $.ajax({
            url: 'getCustomerContactPersons',
            type: 'post',
            data: {customerId: customerId},
            dataType: 'json',
            success: function (response) {
                var len = response.length;
                $("#customerContactId").empty();
                $("#customerContactId").append('<option value="">-- กรุณาเลือก --</option>');
                for (var i = 0; i < len; i++) {
                    var id = response[i]['id'];
                    var name = response[i]['firstName'] + " " + response[i]['lastName'];
                    $("#customerContactId").append("<option value='" + id + "'>" + name + "</option>");
                }
            }
        });
    }

    function contactPersonChange() {
        var customerContactPersonId = $("#customerContactId").val();
        $.ajax({
            url: 'getCustomerContactPersonDetail',
            type: 'post',
            data: {customerContactPersonId: customerContactPersonId},
            dataType: 'json',
            success: function (response) {
                var contactTelNo = response.contactTelNo;
                var contactFaxNo = response.contactFaxNo;
                $("#contactTelNo").val(contactTelNo);
                $("#contactFaxNo").val(contactFaxNo);
            }
        });
    }

    function deleteMaterialRow(){
        var materialRowCount = parseInt($('#materialRowCount').val());
        if (materialRowCount == 1){
            alert("ไม่สามาราถลบวัตถุดิบทั้งหมดได้")
            exit;
        }
        $('#materialRow' + materialRowCount.toString()).remove();
        materialRowCount--;
        $('#materialRowCount').val(materialRowCount);
        calculateCost();
    }
    
    function sumColorMaterialCost(){
        var materialRowCount = parseInt($('#materialRowCount').val());
        var colorMaterialCostRowCount = 0;
        var colorMaterialCostId = "";
        var colorMaterialCostIdTemp = "";
        var isDuplicate = false; 
        var dataIndexTemp=0;
        var colorMaterialCheckArea = 0;
        var summaryArea = 0.0;
        var areaPerUnit = 0.0;
        var strSummary = "";
        var digit3 = "";
        var digit4 = "";
        var digit2 = "";
        var newDigit2 = "";
        var indexOfDecimal = 0;
        var decimalPart = "";
        var headerContent = '<thead><tr><th class="text-center">ลำดับ</th><th class="text-center">รายการ</th><th class="text-center col-md-2" colspan="2">จำนวน</th>';
        headerContent = headerContent + '<th class="text-center col-md-2">ราคา @</th><th class="text-center">รวมเป็นเงิน</th></tr></thead>';
        $("#ColorMaterialCostTable").html(headerContent);
        for (j = 1; j <= materialRowCount; j++) {
           colorMaterialCostId =  $('#colorMaterialCost' + j.toString()).val();
           if (colorMaterialCostId != ''){   
                isDuplicate = false;
                for (k = j-1; k > 0; k--) {
                    colorMaterialCostIdTemp =  $('#colorMaterialCost' + k.toString()).val();
                    if (colorMaterialCostIdTemp == colorMaterialCostId){
                        isDuplicate = true;                        
                    }                    
                }
                if (!isDuplicate){
                    colorMaterialCheckArea = 0;
                    summaryArea = 0;
                    areaPerUnit = 0;
                    for (l = 1; l <= materialRowCount; l++){
                        if (!isNaN($('#colorMaterialCost' + l.toString()).val()) && !isNaN($('#colorMaterialCost' + l.toString()).val())){
                            colorMaterialCheckArea = $('#colorMaterialCost' + l.toString()).val();
                            areaPerUnit = $('#areaPerUnit' + l.toString()).val();                        
                            if (colorMaterialCheckArea == colorMaterialCostId){
                                summaryArea = parseFloat(summaryArea) + parseFloat(areaPerUnit);
                            }
                        }
                    }
                    
                    strSummary = summaryArea.toString();
                    indexOfDecimal = strSummary.indexOf(".");
                    decimalPart = strSummary.substring(indexOfDecimal);
                    digit3 = strSummary.charAt(2);
                    digit4 = strSummary.charAt(3);
                    digit2 = strSummary.charAt(1);
                    summaryArea = parseFloat(summaryArea).toFixed(2);
                    // do it again after tofix2
                    strSummary = summaryArea.toString();
                    indexOfDecimal = strSummary.indexOf(".");
                    decimalPart = strSummary.substring(indexOfDecimal);
                    newDigit2 = strSummary.charAt(1);
                    if ((!isNaN(digit4) && (digit4 != "0")) || (!isNaN(digit3) && (digit3 != "0"))){
                        if (digit2 == newDigit2){
                            summaryArea = parseFloat(summaryArea) + parseFloat(0.01);                            
                        }
                    }
                    dataIndexTemp++;
                    colorMaterialCostRowCount++;
                    $.ajax({
                        url: 'getColorMaterialCostDetail',
                        type: 'post',
                        async : false,
                        data: {colorMaterialCostId: colorMaterialCostId,dataIndex:dataIndexTemp},
                        dataType: 'json',
                        success: function (response) {
                            var name = response.name;
                            var price = response.price;
                            var dataIndex = response.dataIndex;
                            var content = '<tbody><tr><td class="text-center">' + dataIndex + '.<input type="hidden" id="colorPrice' + dataIndex + '" value="' + colorMaterialCostId + '"></td>';
                            content = content + '<td class="col-md-4"><input type="text" class="form-control col-md-2 col-xs-2" disabled="true" value="' + name +'"></td>';
                            content = content + '<td><input type="text" id="colorMaterialCostAmt' + dataIndex + '" onchange="calculateColorCost(' + dataIndex + ')" class="form-control col-md-2 col-xs-2" value="' + summaryArea + '"></td><td>ตรม.</td><td><input type="text" id="colorMaterialPrice' + dataIndex + '" class="form-control col-md-2 col-xs-2" value="' + formatNumber(price) +'" disabled="true"></td>';
                            content = content + '<td align="right"><label id="colorMaterialCostSummary' + dataIndex + '">0.00</label></td></tr></tbody>'; 
                            $("#ColorMaterialCostTable").append(content);                        
                        }
                    });
                }                
           }                      
        }    
        $('#ColorMaterialCostRowCount').val(colorMaterialCostRowCount);        
    }
    
    function calculateColorCost(dataIndex){
        var colorPrice = $("#colorMaterialPrice" + dataIndex).val();
        var amount = $("#colorMaterialCostAmt" + dataIndex).val();
        var totalPrice = colorPrice * amount;
        $("#colorMaterialCostSummary" + dataIndex).text(parseFloat(totalPrice).toFixed(2));
        calculateCost();
    }
    
    function addMaterialRow(){
        var content = $('#materialRow1').html();
        var materialRowCount = parseInt($('#materialRowCount').val());    
        materialRowCount++;
        $('#materialRowCount').val(materialRowCount);
        content= "<tbody id='materialRow" + materialRowCount.toString() + "'>" + content + "</tbody>";
        content = content.split('1" id="').join(materialRowCount.toString() + '" id="');
        content = content.split('1" value="').join(materialRowCount.toString() + '" value="');
        content = content.split('<span id="materialRowOrder1">1.</span>').join('<span id="materialRowOrder' + materialRowCount.toString() + '">' + materialRowCount.toString() + '.</span>');
        content = content.split('1" value="').join(materialRowCount.toString() + '" value="');
        content = content.split('id="cutAmountTxt1"').join('id="cutAmountTxt' + materialRowCount.toString() + '"');
        content = content.split('id="colorMaterialCost1"').join('id="colorMaterialCost' + materialRowCount.toString() + '"');
        content = content.split('id="calculationPrice1"').join('id="calculationPrice' + materialRowCount.toString() + '"');
        content = content.split('id="specialCutBox1"').join('id="specialCutBox' + materialRowCount.toString() + '"');
        content = content.split('id="selectColumn1"').join('id="selectColumn' + materialRowCount.toString() + '"');
        content = content.split('id="userWidth1"').join('id="userWidth' + materialRowCount.toString() + '"');
        content = content.split('id="userHeight1"').join('id="userHeight' + materialRowCount.toString() + '"');
        content = content.split('id="materialAmountId1"').join('id="materialAmountId' + materialRowCount.toString() + '"');
        content = content.split('name="materialAmount1"').join('name="materialAmount' + materialRowCount.toString() + '"');        
        content = content.split('"materialChange(1);"').join('"materialChange(' + materialRowCount.toString() + ');"'); 
        content = content.split('"calculatePrice(1);"').join('"calculatePrice(' + materialRowCount.toString() + ');"');         
        content = content.split('originalWidth1').join('originalWidth' + materialRowCount.toString()); 
        content = content.split('originalHeight1').join('originalHeight' + materialRowCount.toString()); 
        content = content.split('unitName1').join('unitName' + materialRowCount.toString()); 
        content = content.split('fullArea1').join('fullArea' + materialRowCount.toString()); 
        content = content.split('weightPerItem1').join('weightPerItem' + materialRowCount.toString()); 
        content = content.split('materialCategory1').join('materialCategory' + materialRowCount.toString());  
        content = content.split('areaPerUnit1').join('areaPerUnit' + materialRowCount.toString()); 
        content = content.split('cutAmount1').join('cutAmount' + materialRowCount.toString());  
        content = content.split('totalArea1').join('totalArea' + materialRowCount.toString()); 
        content = content.split('weightPerUnit1').join('weightPerUnit' + materialRowCount.toString()); 
        content = content.split('totalWeight1').join('totalWeight' + materialRowCount.toString()); 

        $("#materialTable").append(content);        
        $('#cutAmountTxt' + materialRowCount).text(''); 
        $('#specialCutBox' + materialRowCount.toString()).empty();
        $('#specialCutBox' + materialRowCount.toString()).html('<label><input type="checkbox" name="specialCut' + materialRowCount.toString() + '" id="specialCut' + materialRowCount.toString() + '" value="Y" required="" class="flat" onclick="calculatePrice(' + materialRowCount.toString() + ');"></label>');
        $('#specialCut' + materialRowCount).prop("checked", false);
        $('#materialPriceId' + materialRowCount).find('option').remove().end().append('<option value="">-- กรุณาเลือก --</option>').val('')
        $('#calculationPrice' + materialRowCount).text('0.00'); 
        
        var options = $('#materialId1').html();
        $('#selectColumn' + materialRowCount.toString()).html('<select class="selectpicker show-tick form-control" data-live-search="true" onchange="materialChange(' + materialRowCount.toString() + ');" id="materialId' + materialRowCount.toString() + '">' + options + '/select>')
        $('#materialId' + materialRowCount.toString()).val('');
        $('#materialId' + materialRowCount.toString()).selectpicker('refresh');
        $('#specialCut' + materialRowCount.toString()).iCheck({checkboxClass: 'icheckbox_flat-green',radioClass: 'iradio_flat-green'});
    }
        
    function materialChange(materialSelectedId) {
        var materialId = $("#materialId" + materialSelectedId).val();
        $.ajax({
            url: 'getMaterialDetail',
            type: 'post',
            data: {materialId: materialId},
            dataType: 'json',
            success: function (response) {
                $("#materialPriceId" + materialSelectedId).empty();
                $("#materialPriceId" + materialSelectedId).append('<option value="">-- กรุณาเลือก --</option>');
                var price1 = response.price1;
                var datePrice1 = response.datePrice1;
                $("#materialPriceId" + materialSelectedId).append('<option value="' + price1 + '">' + formatNumber(price1) + ' (' + datePrice1 + ')</option>');

                var price2 = response.price2;
                var datePrice2 = response.datePrice2;
                $("#materialPriceId" + materialSelectedId).append('<option value="' + price2 + '">' + formatNumber(price2) + ' (' + datePrice2 + ')</option>');

                var price3 = response.price3;
                var datePrice3 = response.datePrice3;
                $("#materialPriceId" + materialSelectedId).append('<option value="' + price3 + '">' + formatNumber(price3) + ' (' + datePrice3 + ')</option>');

                var price4 = response.price4;
                var datePrice4 = response.datePrice4;
                $("#materialPriceId" + materialSelectedId).append('<option value="' + price4 + '">' + formatNumber(price4) + ' (' + datePrice4 + ')</option>');

                var price5 = response.price5;
                var datePrice5 = response.datePrice5;
                $("#materialPriceId" + materialSelectedId).append('<option value="' + price5 + '">' + formatNumber(price5) + ' (' + datePrice5 + ')</option>');
                
                var originalWidth = response.width;
                $("#originalWidth" + materialSelectedId).val(originalWidth);
                
                var originalHeight = response.length;
                $("#originalHeight" + materialSelectedId).val(originalHeight);                

                
                var fullArea = originalHeight * originalWidth;
                $("#fullArea" + materialSelectedId).val(fullArea);

                var unitName = response.unitName;
                $("#unitName" + materialSelectedId).val(unitName); 


                var weightPerItem = response.weightPerItem;
                $("#weightPerItem" + materialSelectedId).val(weightPerItem);
                
                var materialCategory = response.materialCategory;
                $("#materialCategory" + materialSelectedId).val(materialCategory);    

            }
        });
    }

    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    function calculatePrice(priceSelectedId) {
                
        var userWidth = $("#userWidth" + priceSelectedId).val(); 

        var userHeight = $("#userHeight" + priceSelectedId).val();

        var originalWidth = $("#originalWidth" + priceSelectedId).val(); 

        var originalHeight = $("#originalHeight" + priceSelectedId).val();

        var areaPerUnit = (userWidth * userHeight)/10000;
        $("#areaPerUnit" + priceSelectedId).val(areaPerUnit);         
                
        var pricePerItem = $("#materialPriceId" + priceSelectedId).val();
        var amount = $("#materialAmountId" + priceSelectedId).val();
        
        var totalArea = areaPerUnit * amount;
        $("#totalArea" + priceSelectedId).val(totalArea); 
        
        var fullArea =  $("#fullArea" + priceSelectedId).val();
        var weightPerItem = $("#weightPerItem" + priceSelectedId).val();
        
        var weightPerUnit = weightPerItem/(fullArea/10000) * areaPerUnit;
        weightPerUnit = parseFloat(weightPerUnit).toFixed(2);        
        $("#weightPerUnit" + priceSelectedId).val(weightPerUnit);
        
        var totalWeight = weightPerUnit * amount;
        $("#totalWeight" + priceSelectedId).val(totalWeight);
        
        var calculationPrice = 0;
        var isAlreadyCalculatedPrice = false;
        var cutAmount = 0;
        var unitName = $("#unitName" + priceSelectedId).val();
        if (unitName == "แผ่น"){
            var WidthCalc =  Math.floor(originalWidth / userWidth);
            var HeightCalc = Math.floor(originalHeight / userHeight);
            cutAmount = WidthCalc * HeightCalc;        
            if ($('#specialCut' + priceSelectedId).is(":checked")){
                $("#cutAmount" + priceSelectedId).val(cutAmount);
                $("#cutAmountTxt" + priceSelectedId).text(cutAmount);
            }else{
                var WidthCalc2 =  Math.floor(originalWidth / userHeight);
                var HeightCalc2 = Math.floor(originalHeight / userWidth);    
                var cutAmount2 = WidthCalc2 * HeightCalc2; 
                if (cutAmount2 > cutAmount){
                    $("#cutAmount" + priceSelectedId).val(cutAmount2);
                    $("#cutAmountTxt" + priceSelectedId).text(cutAmount2);  
                    cutAmount = cutAmount2;
                }else{
                    $("#cutAmount" + priceSelectedId).val(cutAmount);
                    $("#cutAmountTxt" + priceSelectedId).text(cutAmount);                
                }
            }
        }else if ((unitName == "เส้น")){
           
            cutAmount = Math.floor(originalHeight / userHeight);
            if (!$('#specialCut' + priceSelectedId).is(":checked")){
                calculationPrice = (pricePerItem / originalHeight) * userHeight *amount;
                calculationPrice = parseFloat(calculationPrice).toFixed(2); 
                isAlreadyCalculatedPrice = true;
            }          
            $("#cutAmount" + priceSelectedId).val(cutAmount);
            $("#cutAmountTxt" + priceSelectedId).text(cutAmount);   
                        
        }
        var cutAmountTxt = $("#cutAmountTxt" + priceSelectedId).text();
        
        if (!isNaN(cutAmountTxt) && !isNaN(amount)){
            cutAmountTxt = parseFloat(cutAmountTxt);
            amount = parseFloat(amount);
            if (amount > cutAmountTxt){
                alert("กรุณาใส่จำนวนที่ต้องการไม่มากไปกว่าจำนวนที่ตัดได้ค่ะ");
                $("#materialAmountId" + priceSelectedId).val(cutAmountTxt);
            }else{
                if (!isAlreadyCalculatedPrice){
                    calculationPrice = (pricePerItem / cutAmount) * amount;
                    calculationPrice = parseFloat(calculationPrice).toFixed(2);
                }

                $("#calculationPrice" + priceSelectedId).text(calculationPrice);
                calculateCost();                  
            }
        }              
    }

    function calculateCost(){
        var materialRowCount = parseInt($('#materialRowCount').val()); 
        var ColorMaterialCostRowCount = parseInt($('#ColorMaterialCostRowCount').val()); 
        var materialPrice = 0;
        var summaryPrice = 0.00;
        for (i = 1; i <= materialRowCount; i++) {
            materialPrice = $('#calculationPrice' + i).text();
            materialPrice = materialPrice.replace(",", "");
            if (!isNaN(materialPrice)){
                summaryPrice = summaryPrice + parseFloat(materialPrice);
            }
          }    
        var otherMatSum = 0;

        var wasteMaterialRowCount = parseInt($('#wasteMaterialRowCount').val());         
        for (p = 1; p<=wasteMaterialRowCount;p++){
            otherMatSum = $('#otherMatSum' + p.toString()).text();
            otherMatSum = otherMatSum.replace(",", "");
            if (isNaN(otherMatSum)){
                otherMatSum = 0;
            }else{
                otherMatSum = parseFloat(otherMatSum);
            }
            summaryPrice = summaryPrice + otherMatSum;           
        }   
        
        for (k = 1; k <= ColorMaterialCostRowCount; k++) {
            materialPrice = $('#colorMaterialCostSummary' + k).text();
            materialPrice = materialPrice.replace(",", "");           
            if (!isNaN(materialPrice)){
                summaryPrice = summaryPrice + parseFloat(materialPrice);
            }       
        }   
        
        var wasteMatSum = 0;
        for (m = 1; m<=2;m++){
            wasteMatSum = $('#wasteMatSum' + m.toString()).text();
            if (isNaN(wasteMatSum)){
                wasteMatSum = 0;
            }else{
                wasteMatSum = parseFloat(wasteMatSum);
            }
            summaryPrice = summaryPrice + wasteMatSum;           
        }  

        var materialCostPercent = 0;
        var materialCost = 0;
        var materialNetPrice = 0;
        materialCostPercent = $("#materialCostPercent").val();
        if (isNaN(materialCostPercent)){
            materialCostPercent = 0;
        }
        materialCost = (summaryPrice * materialCostPercent) / 100;
        materialCost = parseFloat(materialCost).toFixed(2);
        summaryPrice = parseFloat(summaryPrice).toFixed(2);
        materialNetPrice = parseFloat(materialCost) + parseFloat(summaryPrice);
        materialNetPrice = parseFloat(materialNetPrice).toFixed(2);
        $("#materialPrice").text(formatNumber(summaryPrice));
        $("#materialCost").text(formatNumber(materialCost));        
        $("#materialNetPrice").text(formatNumber(materialNetPrice));  
        calculateWage();
    }

    function laborCostChange(laborSelectedId) {
        var laborCostDetail = $("#laborCostId" + laborSelectedId).val();        
        var res = laborCostDetail.split(";");
        var laborCost = res[1];
        var laborAmountUnit = res[2];
        $("#laborCostPrice" + laborSelectedId).val(laborCost);
        $("#manAmount" + laborSelectedId).val("1");
        $("#laborAmount" + laborSelectedId).val("1");
        $("#laborTime" + laborSelectedId).val("1");
        $("#LM" + laborSelectedId).text(formatNumber(parseFloat(laborCost).toFixed(2)));
        $("#totalCost" + laborSelectedId).text(formatNumber(parseFloat(laborCost).toFixed(2)));
        $("#summaryLaborTime" + laborSelectedId).text("1.0");
        var laborCostText = $("#laborCostId" + laborSelectedId + " option:selected").text();
        if (laborCostText == "งานเชื่อม"){
            $("#laborTimeUnit" + laborSelectedId).text("งาน");
            $("#laborTimeUnitTotal" + laborSelectedId).text("งาน");            
        }
        $("#laborAmountUnit" + laborSelectedId).text(laborAmountUnit); 
        calculateTotalDirectLabor();
    }

    function laborTimeChange(laborSelectedId) {
        var laborCostDetail = $("#laborCostId" + laborSelectedId).val();
        var res = laborCostDetail.split(";");
        var laborCost = res[1];        
        var summaryLaborTime = $("#summaryLaborTime" + laborSelectedId).val();
        var manAmount = $("#manAmount" + laborSelectedId).val();
        var laborAmount = $("#laborAmount" + laborSelectedId).val();
        var laborTime = $("#laborTime" + laborSelectedId).val();
        
        var totalTime = laborAmount * laborTime * manAmount;
        $("#summaryLaborTime" + laborSelectedId).text(totalTime);
        var lm = laborCost;
        var totalCost = lm * totalTime;
        $("#totalCost" + laborSelectedId).text(formatNumber(parseFloat(totalCost).toFixed(2)));
        calculateTotalDirectLabor();
    }
    
    function calculateTotalDirectLabor(){
        var selectedCalculationTypeId = $("#calculationTypeId").val();
        if (selectedCalculationTypeId == 1){
            var directLaborRecordCount = $("#directLaborRecordCount").val();
            var totalDirectCost = 0;
            var totalCost = 0;
            for (i = 1; i <= directLaborRecordCount; i++) {
                totalCost = $('#totalCost' + i).text();
                totalCost = totalCost.replace(",", "");
                if (totalCost != ""){
                    totalCost = parseFloat(totalCost);
                    totalDirectCost = totalCost + totalDirectCost;                                
                }
            } 
            $("#totalDirectLabor").text(formatNumber(parseFloat(totalDirectCost).toFixed(2))); 
        }
        calculateWage();
    }
    
    function addDirectLaborRow(){
        var content = $('#directLaborRow1').html();
        
        var directLaborRecordCount = parseInt($('#directLaborRecordCount').val());    
        directLaborRecordCount++; 
        $('#directLaborRecordCount').val(directLaborRecordCount);        
        content= "<tr id='directLaborRow" + directLaborRecordCount.toString() + "'>" + content + "</tr>";
        content = content.split('1" value="').join(directLaborRecordCount.toString() + '" value="');
        content = content.split('onchange="laborTimeChange(1);"').join('onchange="laborTimeChange(' + directLaborRecordCount.toString() + ');"');
        content = content.split('onchange="laborCostChange(1);"').join('onchange="laborCostChange(' + directLaborRecordCount.toString() + ');"');        
        content = content.split('id="summaryLaborTime1"').join('id="summaryLaborTime' + directLaborRecordCount.toString() + '"');
        content = content.split('id="LM1"').join('id="LM' + directLaborRecordCount.toString() + '"');
        content = content.split('id="totalCost1"').join('id="totalCost' + directLaborRecordCount.toString() + '"');
        $("#directLaborTable").append(content);        
        $('#totalCost' + directLaborRecordCount.toString()).text('');
        $('#LM' + directLaborRecordCount.toString()).text(''); 
        $('#summaryLaborTime' + directLaborRecordCount.toString()).text(''); 
   
    }
    
    function deleteDirectLaborRow(){
        var directLaborRecordCount = parseInt($('#directLaborRecordCount').val());
        if (directLaborRecordCount == 1){
            alert("ไม่สามาราถลบค่าแรงทั้งหมดได้")
            exit;
        }
        $('#directLaborRow' + directLaborRecordCount.toString()).remove();
        directLaborRecordCount--;
        $('#directLaborRecordCount').val(directLaborRecordCount);  
        calculateTotalDirectLabor();
    }
    
    function addWasteMaterialRow(){
        var content = $('#wasteMaterialRow1').html();
        
        var wasteMaterialRowCount = parseInt($('#wasteMaterialRowCount').val());    
        wasteMaterialRowCount++; 
        $('#wasteMaterialRowCount').val(wasteMaterialRowCount);        
        content= "<tbody id='wasteMaterialRow" + wasteMaterialRowCount.toString() + "'>" + content + "</tbody>";
        content = content.split('wasteMaterialId1').join('wasteMaterialId'  + wasteMaterialRowCount.toString());
        content = content.split('otherMatAmount1').join('otherMatAmount'  + wasteMaterialRowCount.toString());
        content = content.split('otherMatUnit1').join('otherMatUnit'  + wasteMaterialRowCount.toString());
        content = content.split('otherMatPrice1').join('otherMatPrice'  + wasteMaterialRowCount.toString());
        content = content.split('otherMatSum1').join('otherMatSum'  + wasteMaterialRowCount.toString());        
        content = content.split('wasteMaterialSelectColumn1').join('wasteMaterialSelectColumn'  + wasteMaterialRowCount.toString());        
        content = content.split('onchange="javascript:otherMatCal(1);"').join('onchange="javascript:otherMatCal(' + wasteMaterialRowCount.toString() + ');"');
        content = content.split('id="wasteMaterialCounter1"').join('id="wasteMaterialCounter'  + wasteMaterialRowCount.toString() + '"');   
        
        $("#wasteMaterialTable").append(content);
        $("#wasteMaterialCounter" + wasteMaterialRowCount.toString()).html(wasteMaterialRowCount.toString() + ".");
        
        var options = $('#wasteMaterialId1').html();
        $('#wasteMaterialSelectColumn' + wasteMaterialRowCount.toString()).html('<select class="selectpicker show-tick form-control" data-live-search="true" onchange="javascript:wasteMatChange(' + wasteMaterialRowCount.toString() + ');" id="wasteMaterialId' + wasteMaterialRowCount.toString() + '">' + options + '</select>')
        $('#wasteMaterialId' + wasteMaterialRowCount.toString()).val('');
        $('#wasteMaterialId' + wasteMaterialRowCount.toString()).selectpicker('refresh');  
        $('#otherMatSum' + wasteMaterialRowCount.toString()).text('0.00');
    }
    
    function deleteWasteMaterialRow(){
        
        var wasteMaterialRowCount = parseInt($('#wasteMaterialRowCount').val());
        if (wasteMaterialRowCount == 1){
            alert("ไม่สามาราถลบวัตถุสิ้นเปลืองทั้งหมดได้")
            exit;
        }
        $('#wasteMaterialRow' + wasteMaterialRowCount.toString()).remove();
        wasteMaterialRowCount--;
        $('#wasteMaterialRowCount').val(wasteMaterialRowCount);  
        calculateCost();
    }
    
    function wasteMatCal(materialId){
        var wasteMatAmount = $('#wasteMatAmount' + materialId.toString()).val();
        if (isNaN(wasteMatAmount)){
            wasteMatAmount = 0;
        }
        var wasteMatPrice = $('#wasteMatPrice' + materialId.toString()).val();
        if (isNaN(wasteMatPrice)){
            wasteMatPrice = 0;
        }        
        var wasteMatSum = wasteMatAmount * wasteMatPrice;
        $('#wasteMatSum' + materialId.toString()).text(formatNumber(parseFloat(wasteMatSum).toFixed(2)));
        calculateCost(); 
    }
    
    function otherMatCal(materialId){
        var otherMatAmount = $('#otherMatAmount' + materialId.toString()).val();
        otherMatAmount = otherMatAmount.replace(",", "");
        if (isNaN(otherMatAmount)){
            otherMatAmount = 0;
        }
        var otherMatPrice = $('#otherMatPrice' + materialId.toString()).val();
        otherMatPrice = otherMatPrice.replace(",", "");
        if (isNaN(otherMatPrice)){
            otherMatPrice = 0;
        }        
        var otherMatSum = otherMatAmount * otherMatPrice;
        $('#otherMatSum' + materialId.toString()).text(formatNumber(parseFloat(otherMatSum).toFixed(2)));
        calculateCost(); 
    }
    
    function wasteMatChange(materialSelectedId) {
        var wasteMaterialId = $("#wasteMaterialId" + materialSelectedId).val();
        $.ajax({
            url: 'getWasteMaterialDetail',
            type: 'post',
            data: {wasteMaterialId: wasteMaterialId},
            dataType: 'json',
            success: function (response) {
                $("#otherMatPrice" + materialSelectedId).empty();
                $("#otherMatPrice" + materialSelectedId).append('<option value="">-- กรุณาเลือก --</option>');
                var price1 = response.price1;
                var datePrice1 = response.datePrice1;
                $("#otherMatPrice" + materialSelectedId).append('<option value="' + price1 + '">' + formatNumber(price1) + ' (' + datePrice1 + ')</option>');

                var price2 = response.price2;
                var datePrice2 = response.datePrice2;
                $("#otherMatPrice" + materialSelectedId).append('<option value="' + price2 + '">' + formatNumber(price2) + ' (' + datePrice2 + ')</option>');

                var price3 = response.price3;
                var datePrice3 = response.datePrice3;
                $("#otherMatPrice" + materialSelectedId).append('<option value="' + price3 + '">' + formatNumber(price3) + ' (' + datePrice3 + ')</option>');

                var price4 = response.price4;
                var datePrice4 = response.datePrice4;
                $("#otherMatPrice" + materialSelectedId).append('<option value="' + price4 + '">' + formatNumber(price4) + ' (' + datePrice4 + ')</option>');

                var price5 = response.price5;
                var datePrice5 = response.datePrice5;
                $("#otherMatPrice" + materialSelectedId).append('<option value="' + price5 + '">' + formatNumber(price5) + ' (' + datePrice5 + ')</option>');
            }
        });
    }
    
    function calculateWage(){        
        var materialPrice = 0; 
        materialPrice = parseFloat($('#materialPrice').text().replace(",", ""));
        var directLaberPercent = parseFloat($('#directLaberPercent').val());
        var dailyWage = parseFloat($('#dailyWage').val());
        var totalDirectLabor = materialPrice * directLaberPercent;
        var selectedCalculationTypeId = $("#calculationTypeId").val();
        var directLaborPrice = $("#directLaborPrice").text().replace(",", "");
        if (!isNaN(totalDirectLabor)){
            if (selectedCalculationTypeId == 0){
                $('#totalDirectLabor').text(formatNumber(parseFloat(totalDirectLabor).toFixed(2)));   
            }
        }else{
            if (selectedCalculationTypeId == 0){
                totalDirectLabor = 0.0;
                $('#totalDirectLabor').text(formatNumber(parseFloat(totalDirectLabor).toFixed(2)));   
            }
        }
        var wagePerHour = 0; 
        if (selectedCalculationTypeId == 0){
            wagePerHour = totalDirectLabor / (dailyWage /8 );
            if (!isNaN(wagePerHour)){
                $('#wagePerHour').text(formatNumber(parseFloat(wagePerHour).toFixed(2))); 
            }
        }else{
            totalDirectLabor = $("#totalDirectLabor").text().replace(",", "");            
            if (materialPrice > 0){
                directLaberPercent = (parseFloat(totalDirectLabor) / parseFloat(materialPrice)) * 100;
                $('#directLaberPercent').val(formatNumber(parseFloat(directLaberPercent).toFixed(2)));
            }

            wagePerHour = parseFloat(totalDirectLabor) / parseFloat(directLaborPrice);
            $('#wagePerHour').text(formatNumber(parseFloat(wagePerHour).toFixed(2))); 
        }
        calculateLaborGrandTotal();
    }
    
    function laborCostType1Change(laborCostType1SelectedId){        
        var laborCostValue = $("#laborCostType1Id" + laborCostType1SelectedId).val();
        var laborCostText = $("#laborCostType1Id" + laborCostType1SelectedId + " option:selected").text();
        var cosTypeMethod = $("#laborCostType1Method" + laborCostType1SelectedId).val();      
        if (laborCostText == "แพ็คกิ้ง"){
            $("#laborCostType1Method" + laborCostType1SelectedId).show();            
        }else{
            $("#laborCostType1Method" + laborCostType1SelectedId).hide();
            $("#laborCostType1Method" + laborCostType1SelectedId).val("");
            $("laborCostType1Percent" + laborCostType1SelectedId).text("");            
        }
        var res = laborCostValue.split(";");
        $("#laborCostType1UnitText" + laborCostType1SelectedId).text(res[2]);
        $("#laborCostType1UnitPriceText" + laborCostType1SelectedId).text(formatNumber(parseFloat(res[1]).toFixed(2)));   
        var unitPrice = parseFloat(res[1]);
        var costAmount = $("#laborCostType1Amount" + laborCostType1SelectedId).val();        
        var laborCostType1Total =  unitPrice * parseFloat(costAmount);       

        if (isNaN(laborCostType1Total)){
            laborCostType1Total = 0;
        }
        var materialPrice = parseFloat($("#materialPrice").text().replace(",", ""));
        var percentCalculate = 0;
        if ((!isNaN(materialPrice)) && (materialPrice > 0)){                    
            percentCalculate = (laborCostType1Total/materialPrice); 
            percentCalculate = percentCalculate * 100;
        }
        if ($("#laborCostType1Method" + laborCostType1SelectedId).val() == "1"){   
            $("#laborCostType1Percent" + laborCostType1SelectedId).text(formatNumber(parseFloat(percentCalculate).toFixed(2)) + "%");
        }else{
            $("#laborCostType1Percent" + laborCostType1SelectedId).text("");
        }  
    
        $("#laborCostType1Total" + laborCostType1SelectedId).text(formatNumber(parseFloat(laborCostType1Total).toFixed(2)));   
        calculateOverHead();
    }

    function calculateOverHead(){
        var overheadTotal = 0.00;
        var overHeadCount = parseInt($('#overheadRowCount').val());;
        var overHeadEach = 0;
        for (i = 1; i <= overHeadCount; i++) {             
            overHeadEach = $("#laborCostType1Total" + i).text().replace(",", "");   
            if (overHeadEach != ""){
                overheadTotal = parseFloat(overheadTotal) + parseFloat(overHeadEach); 
            } 
        }
        var overheadPercent = $("#overheadPercent").val();
        overheadTotal = (overheadTotal * overheadPercent)/100;
        $("#overHeadValue").text(formatNumber(overheadTotal.toFixed(2))); 
        calculateLaborGrandTotal();
    }
    
    function deleteOverheadRow(){
        var overheadRowCount = parseInt($('#overheadRowCount').val());
        
        if (overheadRowCount == 1){
            alert("ไม่สามาราถลบค่าโสหุ้ยทั้งหมดได้")
            exit;
        }
        $('#overheadRow' + overheadRowCount.toString()).remove();
        overheadRowCount--;
        $('#overheadRowCount').val(overheadRowCount);  
        calculateOverHead();        
    }
    
    function addOverheadRow(){
        var content = $('#overheadRow1').html();
        
        var overheadRowCount = parseInt($('#overheadRowCount').val());    
        overheadRowCount++; 
        $('#overheadRowCount').val(overheadRowCount);        
        content= "<tr id='overheadRow" + overheadRowCount.toString() + "'>" + content + "</tr>";
        content = content.split('laborCostType1Id1').join('laborCostType1Id'  + overheadRowCount.toString());
        content = content.split('laborCostType1Method1').join('laborCostType1Method'  + overheadRowCount.toString());
        content = content.split('laborCostType1Percent1').join('laborCostType1Percent'  + overheadRowCount.toString());
        content = content.split('laborCostType1Method1').join('laborCostType1Method'  + overheadRowCount.toString());
        content = content.split('laborCostType1Amount1').join('laborCostType1Amount'  + overheadRowCount.toString());        
        content = content.split('laborCostType1UnitText1').join('laborCostType1UnitText'  + overheadRowCount.toString());    
        content = content.split('laborCostType1Unit1').join('laborCostType1Unit'  + overheadRowCount.toString()); 
        content = content.split('laborCostType1UnitPriceText1').join('laborCostType1UnitPriceText'  + overheadRowCount.toString()); 
        content = content.split('laborCostType1Total1').join('laborCostType1Total'  + overheadRowCount.toString()); 
        content = content.split('onchange="javascript:laborCostType1Change(1);"').join('onchange="javascript:laborCostType1Change(' + overheadRowCount.toString() + ');"');                
        
        $("#overheadTable").append(content);        
        $('#laborCostType1Percent' + overheadRowCount.toString()).text('');
        $('#laborCostType1UnitText' + overheadRowCount.toString()).text('');
        $('#laborCostType1UnitPriceText' + overheadRowCount.toString()).text('');
        $('#laborCostType1Total' + overheadRowCount.toString()).text('');
        $('#laborCostType1Method' + overheadRowCount.toString()).hide();  
    }
    
    function laborCostType2Change(laborCostType2SelectedId){        
        var laborCostValue = $("#laborCostType2Id" + laborCostType2SelectedId).val();
        var laborCostText = $("#laborCostType2Id" + laborCostType2SelectedId + " option:selected").text();
        var cosTypeMethod = $("#laborCostType2Method" + laborCostType2SelectedId).val();      
        if ((laborCostText == "ค่าแรงติดตั้ง") || (laborCostText == "ค่าเก็บงาน") || (laborCostText == "ค่าขนส่ง")){
            $("#laborCostType2Method" + laborCostType2SelectedId).show();            
        }else{
            $("#laborCostType2Method" + laborCostType2SelectedId).hide();
            $("#laborCostType2Method" + laborCostType2SelectedId).val("");
            $("laborCostType2Percent" + laborCostType2SelectedId).text("");            
        }
        var res = laborCostValue.split(";");
        $("#laborCostType2UnitText" + laborCostType2SelectedId).text(res[2]);
        $("#laborCostType2UnitPriceText" + laborCostType2SelectedId).text(formatNumber(parseFloat(res[1]).toFixed(2)));   
        var unitPrice = parseFloat(res[1]);
        var costAmount = $("#laborCostType2Amount" + laborCostType2SelectedId).val();        
        var laborCostType2Total =  unitPrice * parseFloat(costAmount);       

        if (isNaN(laborCostType2Total)){
            laborCostType2Total = 0;
        }
        var materialPrice = parseFloat($("#materialPrice").text().replace(",", ""));
        var percentCalculate = 0;
        if ((!isNaN(materialPrice)) && (materialPrice > 0)){                    
            percentCalculate = (laborCostType2Total/materialPrice); 
            percentCalculate = percentCalculate * 100;
        }
        if ($("#laborCostType2Method" + laborCostType2SelectedId).val() == "1"){   
            $("#laborCostType2Percent" + laborCostType2SelectedId).text(formatNumber(parseFloat(percentCalculate).toFixed(2)) + "%");
        }else{
            $("#laborCostType2Percent" + laborCostType2SelectedId).text("");
        }  
    
        $("#laborCostType2Total" + laborCostType2SelectedId).text(formatNumber(parseFloat(laborCostType2Total).toFixed(2)));   
        calculateOperation();
    }

    function calculateOperation(){
        var operationTotal = 0.00;
        var operationCount = parseInt($('#operationRowCount').val());;
        var operationEach = 0;
        for (i = 1; i <= operationCount; i++) {             
            operationEach = $("#laborCostType2Total" + i).text().replace(",", "");   
            if (operationEach != ""){
                operationTotal = parseFloat(operationTotal) + parseFloat(operationEach); 
            } 
        }
        var operationPercent = $("#operationPercent").val();
        operationTotal = (operationTotal * operationPercent)/100;
        $("#operationValue").text(formatNumber(operationTotal.toFixed(2))); 
        calculateLaborGrandTotal();
        
    }
    
    function deleteOperationRow(){
        var operationRowCount = parseInt($('#operationRowCount').val());
        
        if (operationRowCount == 1){
            alert("ไม่สามาราถลบค่าดำเนินการทั้งหมดได้")
            exit;
        }
        $('#operationRow' + operationRowCount.toString()).remove();
        operationRowCount--;
        $('#operationRowCount').val(operationRowCount);  
        calculateOperation();        
    }
    
    function addOperationRow(){
        var content = $('#operationRow1').html();
        
        var operationRowCount = parseInt($('#operationRowCount').val());    
        operationRowCount++; 
        $('#operationRowCount').val(operationRowCount);        
        content= "<tr id='operationRow" + operationRowCount.toString() + "'>" + content + "</tr>";
        content = content.split('laborCostType2Id1').join('laborCostType2Id'  + operationRowCount.toString());
        content = content.split('laborCostType2Method1').join('laborCostType2Method'  + operationRowCount.toString());
        content = content.split('laborCostType2Percent1').join('laborCostType2Percent'  + operationRowCount.toString());
        content = content.split('laborCostType2Method1').join('laborCostType2Method'  + operationRowCount.toString());
        content = content.split('laborCostType2Amount1').join('laborCostType2Amount'  + operationRowCount.toString());        
        content = content.split('laborCostType2UnitText1').join('laborCostType2UnitText'  + operationRowCount.toString());    
        content = content.split('laborCostType2Unit1').join('laborCostType2Unit'  + operationRowCount.toString()); 
        content = content.split('laborCostType2UnitPriceText1').join('laborCostType2UnitPriceText'  + operationRowCount.toString()); 
        content = content.split('laborCostType2Total1').join('laborCostType2Total'  + operationRowCount.toString()); 
        content = content.split('onchange="javascript:laborCostType2Change(1);"').join('onchange="javascript:laborCostType2Change(' + operationRowCount.toString() + ');"');                
        
        $("#operationTable").append(content);        
        $('#laborCostType2Percent' + operationRowCount.toString()).text('');
        $('#laborCostType2UnitText' + operationRowCount.toString()).text('');
        $('#laborCostType2UnitPriceText' + operationRowCount.toString()).text('');
        $('#laborCostType2Total' + operationRowCount.toString()).text('');
        $('#laborCostType2Method' + operationRowCount.toString()).hide();  
    }
    
    function calculateLaborGrandTotal(){
        var grandTotalValue = 0.00;
        var overHeadValue = 0.00;
        var overheadTotal = 0.00;
        var overHeadCount = parseInt($('#overheadRowCount').val());;
        var overHeadEach = 0;
        for (i = 1; i <= overHeadCount; i++) {             
            overHeadEach = $("#laborCostType1Total" + i).text().replace(",", "");   
            if (overHeadEach != ""){
                overheadTotal = parseFloat(overheadTotal) + parseFloat(overHeadEach); 
            } 
        }
        var overHeadValue = $("#overHeadValue").text().replace(",", ""); 
        overheadTotal = overheadTotal + parseFloat(overHeadValue);     
        var operationValue = 0.00;
        var operationTotal = 0.00;
        var operationCount = parseInt($('#operationRowCount').val());;
        var operationEach = 0;
        for (i = 1; i <= operationCount; i++) {             
            operationEach = $("#laborCostType2Total" + i).text().replace(",", "");   
            if (operationEach != ""){
                operationTotal = parseFloat(operationTotal) + parseFloat(operationEach); 
            } 
        }
        var operationValue = $("#operationValue").text().replace(",", ""); 
        operationTotal = operationTotal + parseFloat(operationValue);  
        grandTotalValue = operationTotal + overheadTotal;
        var totalDirectLabor = $("#totalDirectLabor").text().replace(",", ""); 
        grandTotalValue = grandTotalValue + parseFloat(totalDirectLabor);        
        $("#labor_grandtotal").text(formatNumber(grandTotalValue.toFixed(2)));   
        var materialPrice = $("#materialPrice").text().replace(",", "");
        grandTotalValue = grandTotalValue + parseFloat(materialPrice);
        $("#labor_grandtotal_plus_mat").text(formatNumber(grandTotalValue.toFixed(2)));
        grandTotalValue = grandTotalValue - parseFloat(operationValue) - parseFloat(overHeadValue);
        $("#labor_grandtotal_plus_mat_no_cost").text(formatNumber(grandTotalValue.toFixed(2)));  
        predictionCalculate();
    }
    
    function predictionCalculate(){
        var labor_grandtotal_plus_mat = parseFloat($("#labor_grandtotal_plus_mat").text().replace(",", ""));
        var labor_grandtotal_plus_mat_no_cost = parseFloat($("#labor_grandtotal_plus_mat_no_cost").text().replace(",", "")); 
        var materialPrice = parseFloat($("#materialPrice").text().replace(",", "")); 
        var calculateCost = parseFloat($("#calculateCost").text().replace(",", "")); 
        if (isNaN(calculateCost)){
            calculateCost = 1;
            $("#calculateCost").text("1.0");
        }
        var inputPercentage = parseFloat($("#inputPercentage").val().replace(",", ""));     
        var inputAmtPercentage = parseFloat($("#inputAmtPercentage").val().replace(",", ""));    
        var divineBy080 = (labor_grandtotal_plus_mat_no_cost / 0.80);
        var divineBy075 = (labor_grandtotal_plus_mat_no_cost / 0.75);
        var divineBy070 = (labor_grandtotal_plus_mat_no_cost / 0.70);
        var divineBy065 = (labor_grandtotal_plus_mat_no_cost / 0.65);
        var divineBy060 = (labor_grandtotal_plus_mat_no_cost / 0.60);
        var divineBy055 = (labor_grandtotal_plus_mat_no_cost / 0.55);
        $("#divineBy080").text(formatNumber(divineBy080.toFixed(2)));  
        $("#divineBy075").text(formatNumber(divineBy075.toFixed(2)));  
        $("#divineBy070").text(formatNumber(divineBy070.toFixed(2)));  
        $("#divineBy065").text(formatNumber(divineBy065.toFixed(2)));  
        $("#divineBy060").text(formatNumber(divineBy060.toFixed(2)));  
        $("#divineBy055").text(formatNumber(divineBy055.toFixed(2)));
        var divineByInput = 0;
        if (!isNaN(inputPercentage)){
            divineByInput = (labor_grandtotal_plus_mat_no_cost / inputPercentage);            
        }
        $("#divineByInput").text(formatNumber(divineByInput.toFixed(2)));
        
        var amt020 = (labor_grandtotal_plus_mat * 0.20) * calculateCost;
        var amt025 = (labor_grandtotal_plus_mat * 0.25) * calculateCost;
        var amt030 = (labor_grandtotal_plus_mat * 0.30) * calculateCost;
        var amt035 = (labor_grandtotal_plus_mat * 0.35) * calculateCost;
        var amt040 = (labor_grandtotal_plus_mat * 0.40) * calculateCost;
        var amt045 = (labor_grandtotal_plus_mat * 0.45) * calculateCost;
        $("#amt020").text(formatNumber(amt020.toFixed(2)));  
        $("#amt025").text(formatNumber(amt025.toFixed(2)));  
        $("#amt030").text(formatNumber(amt030.toFixed(2)));  
        $("#amt035").text(formatNumber(amt035.toFixed(2)));  
        $("#amt040").text(formatNumber(amt040.toFixed(2)));  
        $("#amt045").text(formatNumber(amt045.toFixed(2)));        
        var amtByInput = 0.0;
        if (!isNaN(inputAmtPercentage)){
            amtByInput = (labor_grandtotal_plus_mat * (inputAmtPercentage /100)) * calculateCost;            
        }
        $("#amtByInput").text(formatNumber(amtByInput.toFixed(2)));
        
        var quo020 = (labor_grandtotal_plus_mat / calculateCost) + amt020;
        var quo025 = (labor_grandtotal_plus_mat / calculateCost) + amt025;
        var quo030 = (labor_grandtotal_plus_mat / calculateCost) + amt030;
        var quo035 = (labor_grandtotal_plus_mat / calculateCost) + amt035;
        var quo040 = (labor_grandtotal_plus_mat / calculateCost) + amt040;
        var quo045 = (labor_grandtotal_plus_mat / calculateCost) + amt045;
        $("#quo020").text(formatNumber(quo020.toFixed(2)));  
        $("#quo025").text(formatNumber(quo025.toFixed(2)));  
        $("#quo030").text(formatNumber(quo030.toFixed(2)));  
        $("#quo035").text(formatNumber(quo035.toFixed(2)));  
        $("#quo040").text(formatNumber(quo040.toFixed(2)));  
        $("#quo045").text(formatNumber(quo045.toFixed(2)));        
        var quoByInput = 0.0;
        if (!isNaN(inputAmtPercentage)){
            quoByInput = (labor_grandtotal_plus_mat / calculateCost) + amtByInput;          
        }
        $("#quoByInput").text(formatNumber(quoByInput.toFixed(2)));
        if (materialPrice > 0){
            var multipleTime020 = (quo020 / (materialPrice / calculateCost));
            var multipleTime025 = (quo025 / (materialPrice / calculateCost));
            var multipleTime030 = (quo030 / (materialPrice / calculateCost));
            var multipleTime035 = (quo035 / (materialPrice / calculateCost));
            var multipleTime040 = (quo040 / (materialPrice / calculateCost));
            var multipleTime045 = (quo045 / (materialPrice / calculateCost));
            $("#multipleTime020").text(formatNumber(multipleTime020.toFixed(2)));  
            $("#multipleTime025").text(formatNumber(multipleTime025.toFixed(2)));  
            $("#multipleTime030").text(formatNumber(multipleTime030.toFixed(2)));  
            $("#multipleTime035").text(formatNumber(multipleTime035.toFixed(2)));  
            $("#multipleTime040").text(formatNumber(multipleTime040.toFixed(2)));  
            $("#multipleTime045").text(formatNumber(multipleTime045.toFixed(2)));        
            var multipleTimeByInput = 0.0;
            if (!isNaN(inputAmtPercentage)){
                multipleTimeByInput = (quoByInput / (materialPrice / calculateCost));           
            }
            $("#multipleTimeByInput").text(formatNumber(multipleTimeByInput.toFixed(2)));
        }
        calculatePriceOffer();
    }
    
    function calculatePriceOffer(){
        var priceOffer = parseFloat($("#priceOffer").val().replace(",", ""));
        var calculateCost = parseFloat($("#calculateCost").text().replace(",", ""));
        var materialPrice = parseFloat($("#materialPrice").text().replace(",", "")); 
        var labor_grandtotal = parseFloat($("#labor_grandtotal").text().replace(",", ""));
        var overHeadValue = parseFloat($("#overHeadValue").text().replace(",", ""));
        var labor_grandtotal_plus_mat_no_cost = parseFloat($("#labor_grandtotal_plus_mat_no_cost").text().replace(",", ""));         
        var priceOfferPerUnit = priceOffer / calculateCost;
        var priceOfferMaterial = 0;
        var priceOfferWage = 0;
        $("#priceOfferPerUnit").text(formatNumber(priceOfferPerUnit.toFixed(2)));
        if ((!isNaN(priceOffer)) && priceOffer > 0){
            priceOfferMaterial = (materialPrice / (labor_grandtotal_plus_mat_no_cost / priceOffer))
        }
        $("#priceOfferMaterial").text(formatNumber(priceOfferMaterial.toFixed(2)));
        priceOfferWage = (labor_grandtotal - overHeadValue) / (labor_grandtotal_plus_mat_no_cost /  priceOffer);
        $("#priceOfferWage").text(formatNumber(priceOfferWage.toFixed(2)));
    }
    
    function changeCalculationType(){
        var selectedCalculationTypeId = $("#calculationTypeId").val();
        if (selectedCalculationTypeId == 0){
            $("#directLaborTableHead").hide(); 
            $("#directLaborTable").hide();   
            $("#directLaborTableButton").hide(); 
            calculateWage();
        }else{
            $("#directLaborTableHead").show(); 
            $("#directLaborTable").show(); 
            $("#directLaborTableButton").show();  
            calculateTotalDirectLabor();
        }
    }
