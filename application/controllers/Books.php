<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Books extends MY_Controller 
{

     public function index()
     {
         $autoload['libraries'] = array('database');
         $this->load->database();
         $data = array( 
            'firstname' => 'Mr.First', 
            'lastname' => 'Mr.Last',
            'password' => 'password', 
            'username' => 'Username'
         ); 
         
         $this->db->insert("test_user", $data);

         $query = $this->db->get("test_user"); 
         $records = $query->result();
         foreach($records as $r) { 
            echo "<tr>"; 
            echo "<td>".$r->firstname."</td>"; 
            echo "<td>".$r->lastname."</td>";  
            echo "<td>".$r->username."</td>"; 
            echo "<td>".$r->password."</td>";             
            echo "<tr>"; 
         }
        //$this->middle = 'books/index'; // passing middle to function. change this for different views.
        //$this->layout();
     }

}