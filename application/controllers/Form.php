
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Form extends MY_Controller {
  public function index() {
    $this->middle = 'form'; // passing middle to function. change this for different views.
    $this->layout();
  }
}