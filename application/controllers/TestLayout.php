
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class TestLayout extends MY_Controller {
  public function index() {
    $this->middle = 'TestLayout'; // passing middle to function. change this for different views.
    $this->layout();
  }
}