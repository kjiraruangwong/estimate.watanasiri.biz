<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Form extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model("CustomerModel");
        $this->load->model("ColorMaterialCostModel");
        $this->load->model("CustomerContactPersonModel");
        $this->load->model("CustomerTypeModel");
        $this->load->model("ProjectModel");
        $this->load->model("UnitModel");
        $this->load->model("ProductModel");
        $this->load->model("FinishingModel");
        $this->load->model("MaterialTypeModel");
        $this->load->model("MaterialModel");  
        $this->load->model("ColorMaterialCostModel");
        $this->load->model("LaborCostModel");    
        $this->load->model("WasteMaterialModel");  
    }

    public function directlabor() {
        $data = array();

        $c_query = $this->CustomerModel->selectAllData();
        if (!empty($c_query)) {
            $data['customers'] = $c_query;
        }

        $data['projects'] = array();


        $u_query = $this->UnitModel->selectAllData();
        if (!empty($u_query)) {
            $data['units'] = $u_query;
        }

        $pd_query = $this->ProductModel->selectAllData();
        if (!empty($pd_query)) {
            $data['products'] = $pd_query;
        }

        $ct_query = $this->CustomerTypeModel->selectAllData();
        if (!empty($ct_query)) {
            $data['customerTypes'] = $ct_query;
        }

        $fn_query = $this->FinishingModel->selectAllData();
        if (!empty($fn_query)) {
            $data['finishings'] = $fn_query;
        }

        $data['customerContactPersons'] = array();

        
        $mtt_query = $this->MaterialTypeModel->selectAllData();
        if (!empty($mtt_query)) {
            $data['materialTypes'] = $mtt_query;
        }        

        $mt_query = $this->MaterialModel->selectAllData();
        if (!empty($mt_query)) {
            $data['materials'] = $mt_query;
        } 
        
        $cmc_query = $this->ColorMaterialCostModel->selectAllData();
        if (!empty($cmc_query)) {
            $data['colorMaterialCosts'] = $cmc_query;
        }
        
        $lbc_query = $this->LaborCostModel->selectDirectLaborData();
        if (!empty($lbc_query)) {
            $data['laborCosts'] = $lbc_query;
        }
        
        $lbcType1_query = $this->LaborCostModel->selectType1Data();
        if (!empty($lbcType1_query)) {
            $data['laborType1Costs'] = $lbcType1_query;
        }

        
        $lbcType2_query = $this->LaborCostModel->selectType2Data();
        if (!empty($lbcType2_query)) {
            $data['laborType2Costs'] = $lbcType2_query;
        }        
        
        $wmt_query = $this->WasteMaterialModel->selectAllData();
        if (!empty($wmt_query)) {
            $data['wasteMaterials'] = $wmt_query;
        }
            
        $this->template['middle'] = $this->load->view($this->middle = '/calculation/form/directlabor', $data, true);
        $this->layout();
    }

    public function directlaborlist() {
        $this->middle = '/calculation/form/directlaborlist'; // passing middle to function. change this for different views.
        $this->layout();
    }
    
    public function getProjects() {
        $customerId = $this->input->post('customerId');
        $p_query = $this->ProjectModel->selectAllDataByCustomer($customerId);
 
        $projects_arr = array();
        if (!empty($p_query)) {
            $projects = $p_query; 

            foreach ( $projects as $row ){
                $id = $row->id;
                $name = $row->name;

                $projects_arr[] = array("id" => $id, "name" => $name);
            }
        }
        // encoding array to json format
        echo json_encode($projects_arr);       
    }
    
    public function getCustomerContactPersons(){
        $customerId = $this->input->post('customerId');
        $p_query = $this->CustomerContactPersonModel->selectAllDataByCustomer($customerId);
        $contact_arr = array();
        if (!empty($p_query)) {
            $contacts = $p_query; 

            foreach ( $contacts as $row ){
                $id = $row->id;
                $name = $row->firstName;
                $lastName = $row->lastName;
                $contact_arr[] = array("id" => $id, "firstName" => $name, "lastName" => $lastName);
            }
        }
        echo json_encode($contact_arr);         
    }    
    
    public function getMaterialDetail(){
        $materialId = $this->input->post('materialId');
        
        $records = $this->MaterialModel->selectData($materialId); 
        $row = $records[0];
        
        $dt1 = new Datetime($row->datePrice1); 
        $price1 = $row->price1;        
        $datePrice1 = $dt1->format('Y-m-d');;

        $dt2 = new Datetime($row->datePrice2);
        $price2 = $row->price2;        
        $datePrice2 = $dt2->format('Y-m-d');;
        
        $dt3 = new Datetime($row->datePrice3);  
        $price3 = $row->price3;        
        $datePrice3 = $dt3->format('Y-m-d');;
        
        $dt4 = new Datetime($row->datePrice4);        
        $price4 = $row->price4;
        $datePrice4 = $dt4->format('Y-m-d');;
        
        $dt5 = new Datetime($row->datePrice5);        
        $price5 = $row->price5;
        $datePrice5 = $dt5->format('Y-m-d');                      
        $json_result = ["price1" => $price1, "datePrice1" => $datePrice1,
                        "price2" => $price2, "datePrice2" => $datePrice2,
                        "price3" => $price3, "datePrice3" => $datePrice3,
                        "price4" => $price4, "datePrice4" => $datePrice4,
                        "price5" => $price5, "datePrice5" => $datePrice5,
                        "width" => $row->width, "length" =>  $row->length,
                        "unitName" => $row->unitName, "fullArea" => $row->area,
                        "weightPerItem" => $row->weight, "materialCategory" => $row->materialTypeName];
        // encoding array to json format
        echo json_encode($json_result);         
    }   
    
     public function getWasteMaterialDetail(){
        $wasteMaterialId = $this->input->post('wasteMaterialId');
        $records = $this->WasteMaterialModel->selectData($wasteMaterialId); 
        $row = $records[0];
        
        $dt1 = new Datetime($row->datePrice1); 
        $price1 = $row->price1;        
        $datePrice1 = $dt1->format('Y-m-d');;

        $dt2 = new Datetime($row->datePrice2);
        $price2 = $row->price2;        
        $datePrice2 = $dt2->format('Y-m-d');;
        
        $dt3 = new Datetime($row->datePrice3);  
        $price3 = $row->price3;        
        $datePrice3 = $dt3->format('Y-m-d');;
        
        $dt4 = new Datetime($row->datePrice4);        
        $price4 = $row->price4;
        $datePrice4 = $dt4->format('Y-m-d');;
        
        $dt5 = new Datetime($row->datePrice5);        
        $price5 = $row->price5;
        $datePrice5 = $dt5->format('Y-m-d');                      
        $json_result = ["price1" => $price1, "datePrice1" => $datePrice1,
                        "price2" => $price2, "datePrice2" => $datePrice2,
                        "price3" => $price3, "datePrice3" => $datePrice3,
                        "price4" => $price4, "datePrice4" => $datePrice4,
                        "price5" => $price5, "datePrice5" => $datePrice5];
        // encoding array to json format
        echo json_encode($json_result);         
    } 
    
    public function getCustomerContactPersonDetail(){
        $customerContactPersonId = $this->input->post('customerContactPersonId');

        $records = $this->CustomerContactPersonModel->selectData($customerContactPersonId); 
        $row = $records[0];
        $tel = $row->tel;
        $fax = $row->fax;        
        $json_result = ["contactTelNo" => $tel, "contactFaxNo" => $fax];
        // encoding array to json format
        echo json_encode($json_result);          
    }
    
    public function getColorMaterialCostDetail(){
        $colorMaterialCostId = $this->input->post('colorMaterialCostId');
        $dataIndex = $this->input->post('dataIndex');
        $records = $this->ColorMaterialCostModel->selectData($colorMaterialCostId);        
        $row = $records[0];
        $name = $row->name;
        $price = $row->price;        
        $json_result = ["name" => $name, "price" => $price, "dataIndex" => $dataIndex];
        // encoding array to json format
        echo json_encode($json_result);          
    }

}
