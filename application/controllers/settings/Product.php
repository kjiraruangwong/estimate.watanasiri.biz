<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model("ProductModel");
        $this->load->model("MainProductModel");
        $this->load->model("SubProductModel");
        $this->load->model("FinishingModel");
        $this->load->model("CustomerTypeModel");
        $this->load->model("MaterialTypeModel");
        $this->load->model("LaborCostTypeModel");
        $this->load->model("LaborCostModel");
        $this->load->model("UnitModel");
        $this->load->model("CostTypeModel");
        $this->load->model("ColorMaterialCostModel");
    }

    public function index() {
        $this->middle = '/settings/customer/index'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function productde() {
        if ($this->input->post('form-submitted') != null) {
                if (!is_dir('./assets/images/uploads/product_images/')) {
                    mkdir('./assets/images/uploads/product_images/', 0777, TRUE);
                }
                $config['upload_path'] = './assets/images/uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['file_name'] = $this->input->post('code') . ".jpg";
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('product_image')) {
                    $error = array('error' => $this->upload->display_errors());
                    redirect("/settings/product/productde?errUpload=1&code=" . $this->input->post('code') . "&message=" . $this->upload->display_errors());                    
                }          
            
            //Setting values for tabel columns
            if ($this->input->post('selected-id') != null){
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'customerTypeId' => $this->input->post('customerTypeId'),
                    'materialTypeId' => $this->input->post('materialTypeId'),
                    'mainProductId' => $this->input->post('mainProductId'),
                    'subProductId' => $this->input->post('subProductId'),
                    'width' => $this->input->post('width'),
                    'height' => $this->input->post('height'),
                    'depth' => $this->input->post('depth'),
                    'finishingId' => $this->input->post('finishingId'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );
                $this->ProductModel->updateData($this->input->post('selected-id'), $data);
            }else{ 
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'customerTypeId' => $this->input->post('customerTypeId'),
                    'materialTypeId' => $this->input->post('materialTypeId'),
                    'mainProductId' => $this->input->post('mainProductId'),
                    'subProductId' => $this->input->post('subProductId'),
                    'width' => $this->input->post('width'),
                    'height' => $this->input->post('height'),
                    'depth' => $this->input->post('depth'),
                    'finishingId' => $this->input->post('finishingId'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );
                $this->ProductModel->saveData($data);
            } 
            
            $error = $this->db->error();
            if ($error['code']){
                redirect("/settings/product/productde?err=1&code=" . $this->input->post('code') . "&message=" . $error['message']);
            }else{
                redirect("/settings/product/productdelist");
            }
        }

        $data = array();
        // main product query
        $mquery = $this->MainProductModel->selectAllData();
        if (!empty($mquery)) {
            $data['mainProducts'] = $mquery;
        }

        // sub product query
        $squery = $this->SubProductModel->selectAllData();
        if (!empty($squery)) {
            $data['subProducts'] = $squery;
        }

        // finishing query
        $fquery = $this->FinishingModel->selectAllData();
        if (!empty($fquery)) {
            $data['finishings'] = $fquery;
        }

        // customer type query
        $cquery = $this->CustomerTypeModel->selectAllData();
        if (!empty($cquery)) {
            $data['customerTypes'] = $cquery;
        }

        // customer type query
        $mtquery = $this->MaterialTypeModel->selectAllData();
        if (!empty($mtquery)) {
            $data['materialTypes'] = $mtquery;
        }
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
            $records = $this->ProductModel->selectData($_GET['select_id']);            
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];;            
        } 
        
        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/productde', $data, true);
        $this->layout();
    }

    public function productdelist() {
        if (!empty($_GET['del_id'])){
            $this->ProductModel->deleteData($_GET['del_id']);
        }   
        $data = array();

        $query = $this->ProductModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/productdelist', $data, true);

        $this->layout();
    }

    public function mainproduct() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');
            //Setting values for tabel columns
            if ($this->input->post('selected-id') != null) {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );
                $this->MainProductModel->updateData($this->input->post('selected-id'), $data);
            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->MainProductModel->saveData($data);
            }

            redirect("/settings/product/mainproductlist");
        }
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])) {
            $records = $this->MainProductModel->selectData($_GET['select_id']);
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];            
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/mainproduct', $data, true);
        $this->layout();
    }

    public function mainproductlist() {
        if (!empty($_GET['del_id'])) {
            $this->MainProductModel->deleteData($_GET['del_id']);
        }
        $data = array();

        $query = $this->MainProductModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }

        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/mainproductlist', $data, true);
        $this->layout();
    }

    public function sproduct() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');
            //Setting values for tabel columns
            if ($this->input->post('selected-id') != null) {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'mainProductId' => $this->input->post('mainProductId'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );
                $this->SubProductModel->updateData($this->input->post('selected-id'), $data);
            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'mainProductId' => $this->input->post('mainProductId'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->SubProductModel->saveData($data);
            }
            redirect("/settings/product/sproductlist");
        }

        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])) {
            $records = $this->SubProductModel->selectData($_GET['select_id']);
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];
            ;
        }

        $query = $this->MainProductModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }

        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/sproduct', $data, true);
        $this->layout();
    }

    public function sproductlist() {

        if (!empty($_GET['del_id'])) {
            $this->SubProductModel->deleteData($_GET['del_id']);
        }
        $data = array();
        $query = $this->SubProductModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }

        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/sproductlist', $data, true);

        $this->layout();
    }

    public function finishing() {

        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');
            if ($this->input->post('selected-id') != null) {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );
                $this->FinishingModel->updateData($this->input->post('selected-id'), $data);
            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->FinishingModel->saveData($data);
            }

            redirect("/settings/product/finishinglist");
        }
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])) {
            $records = $this->FinishingModel->selectData($_GET['select_id']);
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];
            ;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/finishing', $data, true);
        $this->layout();
    }

    public function finishinglist() {
        if (!empty($_GET['del_id'])) {
            $this->FinishingModel->deleteData($_GET['del_id']);
        }
        $query = $this->FinishingModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }

        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/finishinglist', $data, true);
        $this->layout();
    }

    public function laborc() {
        if ($this->input->post('form-submitted') != null) {
            //Setting values for tabel columns

            date_default_timezone_set('Asia/Bangkok');
            //Setting values for tabel columns
            if ($this->input->post('selected-id') != null) {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'laborCostTypeId' => $this->input->post('laborCostType'),
                    'unitId' => $this->input->post('unit'),
                    'price' => $this->input->post('price'),
                    'costTypeId' => $this->input->post('costType'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );
                $this->LaborCostModel->updateData($this->input->post('selected-id'), $data);
            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'laborCostTypeId' => $this->input->post('laborCostType'),
                    'unitId' => $this->input->post('unit'),
                    'price' => $this->input->post('price'),
                    'costTypeId' => $this->input->post('costType'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->LaborCostModel->saveData($data);
            }

            redirect("/settings/product/laborclist");
        }

        $data = array();
        // labor cost type query
        $lquery = $this->LaborCostTypeModel->selectAllData();
        if (!empty($lquery)) {
            $data['laborCostTypes'] = $lquery;
        }

        // unit query
        $uquery = $this->UnitModel->selectAllData();
        if (!empty($uquery)) {
            $data['units'] = $uquery;
        }

        // cost type query
        $cquery = $this->CostTypeModel->selectAllData();
        if (!empty($cquery)) {
            $data['costTypes'] = $cquery;
        }

        if (!empty($_GET['mode']) && !empty($_GET['select_id'])) {
            $records = $this->LaborCostModel->selectData($_GET['select_id']);
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];
            ;
        }

        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/laborc', $data, true);
        $this->layout();
    }

    public function laborclist() {
        if (!empty($_GET['del_id'])) {
            $this->LaborCostModel->deleteData($_GET['del_id']);
        }
        $data = array();

        $query = $this->LaborCostModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/laborclist', $data, true);
        $this->layout();
    }

    public function laborctype() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');
            //Setting values for tabel columns
            if ($this->input->post('selected-id') != null) {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );
                $this->LaborCostTypeModel->updateData($this->input->post('selected-id'), $data);
            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->LaborCostTypeModel->saveData($data);
            }

            redirect("/settings/product/laborctypelist");
        }
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])) {
            $records = $this->LaborCostTypeModel->selectData($_GET['select_id']);
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];
            ;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/laborctype', $data, true);
        $this->layout();
    }

    public function laborctypelist() {
        if (!empty($_GET['del_id'])) {
            $this->LaborCostTypeModel->deleteData($_GET['del_id']);
        }
        $data = array();

        $query = $this->LaborCostTypeModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/laborctypelist', $data, true);
        $this->layout();
    }

    public function colorp() {
        if ($this->input->post('form-submitted') != null) {

            date_default_timezone_set('Asia/Bangkok');
            //Setting values for tabel columns
            if ($this->input->post('selected-id') != null) {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'materialTypeId' => $this->input->post('materialType'),
                    'colorPage' => $this->input->post('colorPage'),
                    'price' => $this->input->post('price'),
                    'costTypeId' => $this->input->post('costType'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );
                $this->ColorMaterialCostModel->updateData($this->input->post('selected-id'), $data);
            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'materialTypeId' => $this->input->post('materialType'),
                    'colorPage' => $this->input->post('colorPage'),
                    'price' => $this->input->post('price'),
                    'costTypeId' => $this->input->post('costType'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->ColorMaterialCostModel->saveData($data);
            }
            redirect("/settings/product/colorplist");
        }

        $data = array();
        // material type query
        $lquery = $this->MaterialTypeModel->selectAllData();
        if (!empty($lquery)) {
            $data['materialTypes'] = $lquery;
        }

        // cost type query
        $cquery = $this->CostTypeModel->selectAllData();
        if (!empty($cquery)) {
            $data['costTypes'] = $cquery;
        }

        if (!empty($_GET['mode']) && !empty($_GET['select_id'])) {
            $records = $this->ColorMaterialCostModel->selectData($_GET['select_id']);
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];
            ;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/colorp', $data, true);
        $this->layout();
    }

    public function colorplist() {
        if (!empty($_GET['del_id'])) {
            $this->ColorMaterialCostModel->deleteData($_GET['del_id']);
        }
        $data = array();

        $query = $this->ColorMaterialCostModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/product/colorplist', $data, true);
        $this->layout();
    }

}
