<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends MY_Controller {

  function __construct() { 
    parent::__construct(); 
    $this->load->helper('url'); 
    $this->load->database(); 
    $this->load->model("UserModel"); 
    $this->load->model("UserRoleModel");      
  } 

  public function usern() {
    if ($this->input->post('form-submitted') != null) {
            //Setting values for tabel columns
      date_default_timezone_set('Asia/Bangkok');

            //Transfering data to Model
      if ($this->input->post('selected-id') != null){
        $data = array(
          'code' => $this->input->post('code'),
          'name' => $this->input->post('name'),
          'position' => $this->input->post('position'),
          'password' => sha1($this->input->post('password')),
          'dateUpdated' => date('Y-m-d H:i:s')
        );                
        $this->UserModel->updateData($this->input->post('selected-id'), $data);
      }else{
        $data = array(
          'code' => $this->input->post('code'),
          'name' => $this->input->post('name'),
          'position' => $this->input->post('position'),
          'password' => sha1($this->input->post('password')),
          'dateUpdated' => date('Y-m-d H:i:s'),
          'dateCreated' => date('Y-m-d H:i:s')
        );
        $this->UserModel->saveData($data);                                
      }
      redirect("/settings/user/usernlist");
    }

    $data = array();
    if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
      $records = $this->UserModel->selectData($_GET['select_id']);            
      $data['mode'] = $_GET['mode'];
      $data['record'] = $records[0];;            
    }        
    $this->template['middle'] = $this->load->view($this->middle = '/settings/user/usern', $data, true);   
    $this->layout();
  }   

  public function usernlist() {
    if (!empty($_GET['del_id'])){
      $this->UserModel->do_delete_row($_GET['del_id']);
    }
    $data = array();

    $query = $this->UserModel->selectAllData();

    if (!empty($query)) {
      $data['records'] = $query;
    }
    $this->template['middle'] = $this->load->view ($this->middle = '/settings/user/usernlist',$data, true);    
    $this->layout();
  }

  public function userr() {
    if ($this->input->post('form-submitted') != null) {
            //Setting values for tabel columns
      date_default_timezone_set('Asia/Bangkok');

            //Transfering data to Model
      if ($this->input->post('selected-id') != null){
        $data = array(
          'userId' => $this->input->post('user_id'),
          'userCode' => trim(preg_replace('/\s+/', ' ', $this->input->post('user_code'))),
          'userName' => $this->input->post('Uname'),
          'role' => $this->input->post('role'),
          'dateUpdated' => date('Y-m-d H:i:s')
        );                
        $this->UserRoleModel->updateData($this->input->post('selected-id'), $data);
      }else{
        $data = array(
          'userId' => $this->input->post('user_id'),
          'userCode' => trim(preg_replace('/\s+/', ' ', $this->input->post('user_code'))),
          'userName' => $this->input->post('Uname'),
          'role' => $this->input->post('role'),
          'dateUpdated' => date('Y-m-d H:i:s'),
          'dateCreated' => date('Y-m-d H:i:s')
        );
        $this->UserRoleModel->saveData($data);                                
      }
      redirect("/settings/user/userrlist");
    }

    $data = array();
    if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
      $records = $this->UserRoleModel->selectData($_GET['select_id']);            
      $data['mode'] = $_GET['mode'];
      $data['record'] = $records[0];;            
    }
    $query = $this->UserModel->selectAllData();
    if (!empty($query)) {
      $data['users'] = $query;
    }        
    $this->template['middle'] = $this->load->view($this->middle = '/settings/user/userr', $data, true);   
    $this->layout();
  }

  public function userrlist() {
    if (!empty($_GET['del_id'])){
      $this->UserRoleModel->do_delete_row($_GET['del_id']);
    }
    $data = array();

    $query = $this->UserRoleModel->selectAllData();

    if (!empty($query)) {
      $data['records'] = $query;
    }
    $this->template['middle'] = $this->load->view ($this->middle = '/settings/user/userrlist',$data, true);    
    $this->layout();
  }
  public function ajaxUser() {
    if ($this->input->post('get_username') == '1'){
      $user_id =  $this->input->post('user_id');
      $query = $this->UserModel->selectData($user_id);
      if (!empty($query)) {
        echo json_encode($query);
      }else{
        $query = 'Data not found';
        echo json_encode($query);
      }
    }
  }
  
}