<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Material extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model("UnitModel");
        $this->load->model("MaterialTypeModel");
        $this->load->model("ExtraShapeModel");
        $this->load->model("ExtraWasteModel");
        $this->load->model("CostTypeModel");
        $this->load->model("MetalShapeModel");
        $this->load->model("MaterialModel");
        $this->load->model("WasteMaterialModel");
    }

    public function index() {
        $this->middle = '/settings/material/index'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function ingre() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');            
            //Setting values for tabel columns
            $date_price1 = strtotime($this->input->post('date_price1'));
            $date_price1 = date("Y-m-d", $date_price1);
            $date_price2 = strtotime($this->input->post('date_price2'));
            $date_price2 = date("Y-m-d", $date_price2);
            $date_price3 = strtotime($this->input->post('date_price3'));
            $date_price3 = date("Y-m-d", $date_price3);
            $date_price4 = strtotime($this->input->post('date_price4'));
            $date_price4 = date("Y-m-d", $date_price4);
            $date_price5 = strtotime($this->input->post('date_price5'));
            $date_price5 = date("Y-m-d", $date_price5);
            if ($this->input->post('selected-id') != null) {
                $data = array(
                    'code' => $this->input->post('material_code'),
                    'materialTypeId' => $this->input->post('material_type'),
                    'metalShapeId' => $this->input->post('metal_shape'),
                    'extraShapeId' => $this->input->post('extra_shape'),
                    'width' => $this->input->post('width'),
                    'length' => $this->input->post('length'),
                    'diaThickness' => $this->input->post('dia_thickness'),
                    'name' => $this->input->post('material_name'),
                    'unitId' => $this->input->post('unit'),
                    'costTypeId' => $this->input->post('cost_type'),
                    'weight' => $this->input->post('weight'),
                    'area' => $this->input->post('area'),
                    'price1' => $this->input->post('price1'),
                    'datePrice1' => $date_price1,
                    'price2' => $this->input->post('price2'),
                    'datePrice2' => $date_price2,
                    'price3' => $this->input->post('price3'),
                    'datePrice3' => $date_price3,
                    'price4' => $this->input->post('price4'),
                    'datePrice4' => $date_price4,
                    'price5' => $this->input->post('price5'),
                    'datePrice5' => $date_price5,
                    'dateUpdated' => date('Y-m-d H:i:s')                   
                );
                $this->MaterialModel->updateData($this->input->post('selected-id'), $data);                
            }else{
                $data = array(
                    'code' => $this->input->post('material_code'),
                    'materialTypeId' => $this->input->post('material_type'),
                    'metalShapeId' => $this->input->post('metal_shape'),
                    'extraShapeId' => $this->input->post('extra_shape'),
                    'width' => $this->input->post('width'),
                    'length' => $this->input->post('length'),
                    'diaThickness' => $this->input->post('dia_thickness'),
                    'name' => $this->input->post('material_name'),
                    'unitId' => $this->input->post('unit'),
                    'costTypeId' => $this->input->post('cost_type'),
                    'weight' => $this->input->post('weight'),
                    'area' => $this->input->post('area'),
                    'price1' => $this->input->post('price1'),
                    'datePrice1' => $date_price1,
                    'price2' => $this->input->post('price2'),
                    'datePrice2' => $date_price2,
                    'price3' => $this->input->post('price3'),
                    'datePrice3' => $date_price3,
                    'price4' => $this->input->post('price4'),
                    'datePrice4' => $date_price4,
                    'price5' => $this->input->post('price5'),
                    'datePrice5' => $date_price5,
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')                    
                );
                $this->MaterialModel->saveData($data);
            }
            redirect("/settings/material/ingrelist");
        }
        $data = array();
        $query = $this->MaterialTypeModel->selectAllData();
        if (!empty($query)) {
            $data['material_types'] = $query;
        }
        $query_metal_shape = $this->MetalShapeModel->selectAllData();
        if (!empty($query_metal_shape)) {
            $data['metal_shapes'] = $query_metal_shape;
        }
        $query_extra_shape = $this->ExtraShapeModel->selectAllData();
        if (!empty($query_extra_shape)) {
            $data['extra_shapes'] = $query_extra_shape;
        }
        $query_unit = $this->UnitModel->selectAllData();
        if (!empty($query_unit)) {
            $data['units'] = $query_unit;
        }
        $query_cost_type = $this->CostTypeModel->selectAllData();
        if (!empty($query_cost_type)) {
            $data['cost_types'] = $query_cost_type;
        }
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])) {
            $records = $this->MaterialModel->selectData($_GET['select_id']);
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];
        }        
        
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/ingre', $data, true);
        $this->layout();
    }

    public function ingrelist() {
        if (!empty($_GET['del_id'])) {
            $this->MaterialModel->deleteData($_GET['del_id']);
        }        
        $data = array();

        $query = $this->MaterialModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/ingrelist', $data, true);
        $this->layout();
    }

    public function ingrewaste() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');  
            //Setting values for tabel columns
            $date_price1 = strtotime($this->input->post('date_price1'));
            $date_price1 = date("Y-m-d", $date_price1);
            $date_price2 = strtotime($this->input->post('date_price2'));
            $date_price2 = date("Y-m-d", $date_price2);
            $date_price3 = strtotime($this->input->post('date_price3'));
            $date_price3 = date("Y-m-d", $date_price3);
            $date_price4 = strtotime($this->input->post('date_price4'));
            $date_price4 = date("Y-m-d", $date_price4);
            $date_price5 = strtotime($this->input->post('date_price5'));
            $date_price5 = date("Y-m-d", $date_price5);
            if ($this->input->post('selected-id') != null){    
                $data = array(
                    'code' => $this->input->post('waste_code'),
                    'wasteMaterialTypeId' => $this->input->post('waste_type'),
                    'materialTypeId' => $this->input->post('material_type'),
                    'metalShapeId' => $this->input->post('waste_shape'),
                    'extraWasteId' => $this->input->post('extra_waste'),
                    'length' => $this->input->post('length'),
                    'dia' => $this->input->post('dia'),
                    'name' => $this->input->post('waste_name'),
                    'unitId' => $this->input->post('unit'),
                    'costTypeId' => $this->input->post('cost_type'),
                    'weight' => $this->input->post('weight'),
                    'price1' => $this->input->post('price1'),
                    'datePrice1' => $date_price1,
                    'price2' => $this->input->post('price2'),
                    'datePrice2' => $date_price2,
                    'price3' => $this->input->post('price3'),
                    'datePrice3' => $date_price3,
                    'price4' => $this->input->post('price4'),
                    'datePrice4' => $date_price4,
                    'price5' => $this->input->post('price5'),
                    'datePrice5' => $date_price5,
                    'dateUpdated' => date('Y-m-d H:i:s')
                );
                $insert_id = $this->WasteMaterialModel->updateData($this->input->post('selected-id'),$data);                
            }else{
                $data = array(
                    'code' => $this->input->post('waste_code'),
                    'wasteMaterialTypeId' => $this->input->post('waste_type'),
                    'materialTypeId' => $this->input->post('material_type'),
                    'metalShapeId' => $this->input->post('waste_shape'),
                    'extraWasteId' => $this->input->post('extra_waste'),
                    'length' => $this->input->post('length'),
                    'dia' => $this->input->post('dia'),
                    'name' => $this->input->post('waste_name'),
                    'unitId' => $this->input->post('unit'),
                    'costTypeId' => $this->input->post('cost_type'),
                    'weight' => $this->input->post('weight'),
                    'price1' => $this->input->post('price1'),
                    'datePrice1' => $date_price1,
                    'price2' => $this->input->post('price2'),
                    'datePrice2' => $date_price2,
                    'price3' => $this->input->post('price3'),
                    'datePrice3' => $date_price3,
                    'price4' => $this->input->post('price4'),
                    'datePrice4' => $date_price4,
                    'price5' => $this->input->post('price5'),
                    'datePrice5' => $date_price5,
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $insert_id = $this->WasteMaterialModel->saveData($data);
            }
            $error = $this->db->error();
            if ($error['code']){
                redirect("/settings/material/ingrewaste?err=1&code=" . $this->input->post('code') . "&message=" . $error['message']);
            }else{
                redirect("/settings/material/ingrewastelist");
            } 
        }
        $data = array();
        $query = $this->MaterialTypeModel->selectAllData();
        if (!empty($query)) {
            $data['material_types'] = $query;
        }
        $query_metal_shape = $this->MetalShapeModel->selectAllData();
        if (!empty($query_metal_shape)) {
            $data['metal_shapes'] = $query_metal_shape;
        }
        $query_extra_waste = $this->ExtraWasteModel->selectAllData();
        if (!empty($query_extra_waste)) {
            $data['extra_wastes'] = $query_extra_waste;
        }
        $query_unit = $this->UnitModel->selectAllData();
        if (!empty($query_unit)) {
            $data['units'] = $query_unit;
        }
        $query_cost_type = $this->CostTypeModel->selectAllData();
        if (!empty($query_cost_type)) {
            $data['cost_types'] = $query_cost_type;
        }
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
            $records = $this->WasteMaterialModel->selectData($_GET['select_id']);            
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];;            
        }         
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/ingrewaste', $data, true);
        $this->layout();
    }

    public function ingrewastelist() {
        if (!empty($_GET['del_id'])) {
            $this->WasteMaterialModel->deleteData($_GET['del_id']);
        }        
        $data = array();

        $query = $this->WasteMaterialModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/ingrewastelist', $data, true);
        $this->layout();
    }

    public function ingretype() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');            
            //Setting values for tabel columns
            if ($this->input->post('selected-id') != null){
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );                
                $this->MaterialTypeModel->updateData($this->input->post('selected-id'), $data);
            }else{
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->MaterialTypeModel->saveData($data);
            }

            redirect("/settings/material/ingretypelist");
        }
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
            $records = $this->MaterialTypeModel->selectData($_GET['select_id']);            
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];;            
        } 
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/ingretype', $data, true);    
        $this->layout();
    }

    public function ingretypelist() {
        if (!empty($_GET['del_id'])){
            $this->MaterialTypeModel->deleteData($_GET['del_id']);
        }         
        $data = array();

        $query = $this->MaterialTypeModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/ingretypelist', $data, true);

        $this->layout();
    }

    public function ironshape() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');            
            //Setting values for tabel columns
            if ($this->input->post('selected-id') != null){
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );                
                $this->MetalShapeModel->updateData($this->input->post('selected-id'), $data);
            }else{
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->MetalShapeModel->saveData($data);
            }

            redirect("/settings/material/ironshapelist");
        }
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
            $records = $this->MetalShapeModel->selectData($_GET['select_id']);            
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];;            
        } 
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/ironshape', $data, true);   
        $this->layout();
    }

    public function ironshapelist() {
        if (!empty($_GET['del_id'])){
            $this->MetalShapeModel->deleteData($_GET['del_id']);
        }         
        
        $data = array();

        $query = $this->MetalShapeModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/ironshapelist', $data, true);

        $this->layout();
    }

    public function extrashape() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');
            if ($this->input->post('selected-id') != null){
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );                
                $this->ExtraShapeModel->updateData($this->input->post('selected-id'), $data);
            }else{
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->ExtraShapeModel->saveData($data);
            }

            redirect("/settings/material/extrashapelist");
        }
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
            $records = $this->ExtraShapeModel->selectData($_GET['select_id']);            
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];;            
        }           
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/extrashape', $data, true); 

        $this->layout();
    }

    public function extrashapelist() {
        if (!empty($_GET['del_id'])){
            $this->ExtraShapeModel->deleteData($_GET['del_id']);
        }          
        $data = array();

        $query = $this->ExtraShapeModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/extrashapelist', $data, true);

        $this->layout();
    }

    public function extrawaste() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');
            if ($this->input->post('selected-id') != null){
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );                
                $this->ExtraWasteModel->updateData($this->input->post('selected-id'), $data);
            }else{
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->ExtraWasteModel->saveData($data);
            }

            redirect("/settings/material/extrawastelist");
        }
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
            $records = $this->ExtraWasteModel->selectData($_GET['select_id']);            
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];;            
        }           
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/extrawaste', $data, true);
        $this->layout();
    }

    public function extrawastelist() {
        if (!empty($_GET['del_id'])){
            $this->ExtraWasteModel->deleteData($_GET['del_id']);
        }        
        $data = array();

        $query = $this->ExtraWasteModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/extrawastelist', $data, true);

        $this->layout();
    }

    public function costtype() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');
            if ($this->input->post('selected-id') != null){
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );                
                $this->CostTypeModel->updateData($this->input->post('selected-id'), $data);
            }else{
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->CostTypeModel->saveData($data);
            }

            redirect("/settings/material/costtypelist");
        }
        
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
            $records = $this->CostTypeModel->selectData($_GET['select_id']);            
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];;            
        }           
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/costtype', $data, true);    
        $this->layout();
    }

    public function costtypelist() {
        if (!empty($_GET['del_id'])){
            $this->CostTypeModel->deleteData($_GET['del_id']);
        }        
        $data = array();

        $query = $this->CostTypeModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/costtypelist', $data, true);
        $this->layout();
    }

    public function unit() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');            
            //Setting values for tabel columns
            if ($this->input->post('selected-id') != null){
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),                    
                    'dateUpdated' => date('Y-m-d H:i:s')
                );                
                $this->UnitModel->updateData($this->input->post('selected-id'), $data);
            }else{
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')                    
                );
                $this->UnitModel->saveData($data);                
            }
            $error = $this->db->error();
            if ($error['code']){
                redirect("/settings/material/unit?err=1&code=" . $this->input->post('code') . "&message=" . $error['message']);
            }else{
                redirect("/settings/material/unitlist");
            }
        }
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
            $records = $this->UnitModel->selectData($_GET['select_id']);            
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];;            
        }        
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/unit', $data, true);
        $this->layout();
    }

    public function unitlist() {
        if (!empty($_GET['del_id'])){
            $this->UnitModel->deleteData($_GET['del_id']);
        }         
        $data = array();

        $query = $this->UnitModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/material/unitlist', $data, true);
        $this->layout();
    }

}
