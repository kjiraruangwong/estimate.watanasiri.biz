<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model("CustomerTypeModel");
        $this->load->model("CustomerFirstAlphabetModel");
        $this->load->model("CustomerModel");
        $this->load->model("ProjectModel");
    }

    public function index() {
        $this->middle = '/settings/customer/index'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function create() {
        $this->middle = '/settings/customer/create'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function custedit() {
        $this->middle = '/settings/customer/custedit'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function custtype() {
        $this->middle = '/settings/customer/custtype'; // passing middle to function. change this for different views.
        $this->layout();
    }

//Add Customer
    public function customers() {            
        date_default_timezone_set('Asia/Bangkok');
        if ($this->input->post('form-submitted') != null) {
            if ($this->input->post('selected-id') != null){
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'customerFirstCharId' => $this->input->post('first_alphabet'),
                    'customerTypeId' => $this->input->post('customer_type'),
                    'shortName' => $this->input->post('short_name'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );                
                $this->CustomerModel->updateData($this->input->post('selected-id'), $data);
                redirect("/settings/customer/customertlist");                 
            }else{
                //Setting values for tabel columns
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'customerFirstCharId' => $this->input->post('first_alphabet'),
                    'customerTypeId' => $this->input->post('customer_type'),
                    'shortName' => $this->input->post('short_name'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')                     
                );
                //Transfering data to Model
                $insert_id = $this->CustomerModel->saveData($data);
                //redirect("/settings/customer/customertlist");
                
                redirect("/settings/customer/customers?cid=" . $insert_id);
            }
        }

        if (isset($_GET["select_id"]) || $this->input->post('form-contact-submitted') != null) {
            $customer_id = $_GET["select_id"];
            $data = array();
            $query = $this->CustomerModel->selectData($customer_id);
            if (!empty($query)) {
                $data['customer'] = $query;
            }
            $query = $this->CustomerTypeModel->selectAllData();
            if (!empty($query)) {
                $data['records'] = $query;
            }
            $query_alphabet = $this->CustomerFirstAlphabetModel->selectAllData();
            if (!empty($query_alphabet)) {
                $data['alphabet_records'] = $query_alphabet;
            }
            $query_contact = $this->CustomerModel->selectContact($customer_id);
            if (!empty($query_contact)) {
                $data['contacts'] = $query_contact;
            }
            $data['mode'] = $_GET['mode'];            
            $this->template['middle'] = $this->load->view($this->middle = '/settings/customer/customers', $data, true);
            $this->layout();
            if ($this->input->post('form-contact-submitted') != null) {
                //Setting values for tabel columns
                //Transfering data to Model
                if ($this->input->post('selected-id') != null){
                    $data = array(
                        'customerId' => $this->input->post('customer_id'),
                        'firstName' => $this->input->post('contact_firstname'),
                        'lastName' => $this->input->post('contact_lastname'),
                        'address' => $this->input->post('contact_address'),
                        'tel' => $this->input->post('contact_tel'),
                        'fax' => $this->input->post('contact_fax'),
                        'email' => $this->input->post('contact_email'),
                        'dateUpdated' => date('Y-m-d H:i:s')
                    );                
                    $this->CustomerModel->updateData($this->input->post('selected-id'), $data);
                    redirect("/settings/customer/customertlist");                     
                }else{                
                    $data = array(
                        'customerId' => $this->input->post('customer_id'),
                        'firstName' => $this->input->post('contact_firstname'),
                        'lastName' => $this->input->post('contact_lastname'),
                        'address' => $this->input->post('contact_address'),
                        'tel' => $this->input->post('contact_tel'),
                        'fax' => $this->input->post('contact_fax'),
                        'email' => $this->input->post('contact_email'),
                        'dateUpdated' => date('Y-m-d H:i:s'),
                        'dateCreated' => date('Y-m-d H:i:s')                        
                    );
                    //Transfering data to Model
                    $this->CustomerModel->saveContact($data);
                }
                $customer_id = $this->input->post('customer_id');
                redirect("/settings/customer/customers?cid=" . $customer_id);                
            }
        } else {
            $data = array();
            $query = $this->CustomerTypeModel->selectAllData();
            if (!empty($query)) {
                $data['records'] = $query;
            }
            $query_alphabet = $this->CustomerFirstAlphabetModel->selectAllData();
            if (!empty($query_alphabet)) {
                $data['alphabet_records'] = $query_alphabet;
            }
            $this->template['middle'] = $this->load->view($this->middle = '/settings/customer/customers', $data, true);
            $this->layout();
        }
    }

    public function customertlist() {

        if (!empty($_GET['del_id'])){
            $this->CustomerModel->deleteData($_GET['del_id']);
        }        
        
        $data = array();

        $query = $this->CustomerModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/customer/customertlist', $data, true);
        $this->layout();
    }

    public function customertype() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');            
            //Setting values for tabel columns
            if ($this->input->post('selected-id') != null){
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );                
                $this->CustomerTypeModel->updateData($this->input->post('selected-id'), $data);
            }else{
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->CustomerTypeModel->saveData($data);                                
            }            
            $error = $this->db->error();
            if ($error['code']){
                redirect("/settings/customer/customertype?err=1&code=" . $this->input->post('code') . "&message=" . $error['message']);
            }else{
                redirect("/settings/customer/customertypelist");
            }
        }
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
            $records = $this->CustomerTypeModel->selectData($_GET['select_id']);            
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];;            
        }        
        $this->template['middle'] = $this->load->view($this->middle = '/settings/customer/customertype', $data, true); 
        $this->layout();
    }

    public function customertypelist() {
        if (!empty($_GET['del_id'])){
            $this->CustomerTypeModel->deleteData($_GET['del_id']);
        }         
        $data = array();

        $query = $this->CustomerTypeModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/customer/customertypelist', $data, true);
        $this->layout();
    }

    public function custfirst() {
        if ($this->input->post('form-submitted') != null) {
            //Setting values for tabel columns
            date_default_timezone_set('Asia/Bangkok');

            //Transfering data to Model
            if ($this->input->post('selected-id') != null){
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s')
                );                
                $this->CustomerFirstAlphabetModel->updateData($this->input->post('selected-id'), $data);
            }else{
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );
                $this->CustomerFirstAlphabetModel->saveData($data);                                
            }
            $error = $this->db->error();
            if ($error['code']){
                redirect("/settings/customer/custfirs?err=1&code=" . $this->input->post('code') . "&message=" . $error['message']);
            }else{
                redirect("/settings/customer/custfirstlist");
            }
        }
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
            $records = $this->CustomerFirstAlphabetModel->selectData($_GET['select_id']);            
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0];;            
        }        
        $this->template['middle'] = $this->load->view($this->middle = '/settings/customer/custfirst', $data, true);   
        $this->layout();
    }

    public function custfirstlist() {

        if (!empty($_GET['del_id'])){
            $this->CustomerFirstAlphabetModel->deleteData($_GET['del_id']);
        }
        $data = array();

        $query = $this->CustomerFirstAlphabetModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/customer/custfirstlist', $data, true);
        $this->layout();
    }

    public function project() {
        if ($this->input->post('form-submitted') != null) {
            date_default_timezone_set('Asia/Bangkok');            
            //Setting values for tabel columns
            if ($this->input->post('selected-id') != null){
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'ADYear'  => $this->input->post('ADYear'),
                    'customerId'  => $this->input->post('customerId'),
                    'shortName'  => $this->input->post('shortName'),
                    'dateStart'  => date("Y-m-d",strtotime($this->input->post('dateStart'))),
                    'dateEnd'  => date("Y-m-d",strtotime($this->input->post('dateEnd'))),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')
                );             
                $this->ProjectModel->updateData($this->input->post('selected-id'), $data);
            }else{
                $data = array(
                    'name' => $this->input->post('name'),
                    'code' => $this->input->post('code'),
                    'ADYear'  => $this->input->post('ADYear'),
                    'customerId'  => $this->input->post('customerId'),
                    'shortName'  => $this->input->post('shortName'),
                    'dateStart'  => date("Y-m-d",strtotime($this->input->post('dateStart'))),
                    'dateEnd'  => date("Y-m-d",strtotime($this->input->post('dateEnd'))),
                    'dateUpdated' => date('Y-m-d H:i:s'),
                    'dateCreated' => date('Y-m-d H:i:s')                    
                );
                $this->ProjectModel->saveData($data);                
            }
            $error = $this->db->error();
            if ($error['code']){
                redirect("/settings/customer/projec?err=1&code=" . $this->input->post('code') . "&message=" . $error['message']);
            }else{
                redirect("/settings/customer/projectlist");
            }            
        }
        $data = array();
        if (!empty($_GET['mode']) && !empty($_GET['select_id'])){
            $records = $this->ProjectModel->selectData($_GET['select_id']);            
            $data['mode'] = $_GET['mode'];
            $data['record'] = $records[0]; 
            $customer_records = $this->CustomerModel->selectData($records[0]->customerId);    
            $data['customer_record'] = $customer_records[0];
        }         
        $query = $this->CustomerModel->selectAllData();

        if (!empty($query)) {
            $data['customers'] = $query;
        }   
        
        $this->template['middle'] = $this->load->view($this->middle = '/settings/customer/project', $data, true);
        $this->layout();
    }

    public function projectlist() {
        if (!empty($_GET['del_id'])){
            $this->ProjectModel->deleteData($_GET['del_id']);
        }         
        $data = array();

        $query = $this->ProjectModel->selectAllData();

        if (!empty($query)) {
            $data['records'] = $query;
        }
        $this->template['middle'] = $this->load->view($this->middle = '/settings/customer/projectlist', $data, true);
        $this->layout();
    }

    public function paytype() {
        $this->middle = '/settings/customer/paytype'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function paytypelist() {
        $this->middle = '/settings/customer/paytypelist'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function setdirect() {
        $this->middle = '/settings/customer/setdirect'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function setdirectlist() {
        $this->middle = '/settings/customer/setdirectlist'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function settype() {
        $this->middle = '/settings/customer/settype'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function settypelist() {
        $this->middle = '/settings/customer/settypelist'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function pricef() {
        $this->middle = '/settings/customer/pricef'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function priceflist() {
        $this->middle = '/settings/customer/priceflist'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function sellp() {
        $this->middle = '/settings/customer/sellp'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function sellplist() {
        $this->middle = '/settings/customer/sellplist'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function huturn() {
        $this->middle = '/settings/customer/huturn'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function huturnlist() {
        $this->middle = '/settings/customer/huturnlist'; // passing middle to function. change this for different views.
        $this->layout();
    }

    public function combobox() {
        $this->middle = '/settings/customer/combobox'; // passing middle to function. change this for different views.
        $this->layout();
    }

}
