<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LaborCostModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select("lc.*,lct.name as laborCostType,ct.name as costType,u.name as unit", false);
        $this->db->from('LaborCost lc');
        $this->db->join('LaborCostType lct', 'lc.laborCostTypeId = lct.id', 'left');
        $this->db->join('CostType ct', 'lc.costTypeId = ct.id', 'left');
        $this->db->join('Unit u', 'lc.unitId = u.id', 'left');
        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }
    
    public function selectDirectLaborData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select("lc.*,lct.name as laborCostType,ct.name as costType,u.name as unit", false);
        $this->db->from('LaborCost lc');
        $this->db->join('LaborCostType lct', 'lc.laborCostTypeId = lct.id', 'left');
        $this->db->join('CostType ct', 'lc.costTypeId = ct.id', 'left');
        $this->db->join('Unit u', 'lc.unitId = u.id', 'left');
        $this->db->where("ct.name = 'Direct Labor'");        
        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }    
    
    public function selectType1Data() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select("lc.*,lct.name as laborCostType,ct.name as costType,u.name as unit", false);
        $this->db->from('LaborCost lc');
        $this->db->join('LaborCostType lct', 'lc.laborCostTypeId = lct.id', 'left');
        $this->db->join('CostType ct', 'lc.costTypeId = ct.id', 'left');
        $this->db->join('Unit u', 'lc.unitId = u.id', 'left');
        $this->db->where('ct.code in ("1011","1012","1013","1014")');        
        $q = $this->db->get();
        $response = $q->result();

        return $response;
    }    

    public function selectType2Data() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select("lc.*,lct.name as laborCostType,ct.name as costType,u.name as unit", false);
        $this->db->from('LaborCost lc');
        $this->db->join('LaborCostType lct', 'lc.laborCostTypeId = lct.id', 'left');
        $this->db->join('CostType ct', 'lc.costTypeId = ct.id', 'left');
        $this->db->join('Unit u', 'lc.unitId = u.id', 'left');
        $this->db->where('ct.code in ("2011","2012","2013","2014")');
        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }        
    
    function saveData($data) {
        $this->db->insert('LaborCost', $data);
    }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('LaborCost', $data);
    }

    public function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('LaborCost');
    }

    public function selectData($id) {
        $query = $this->db->get_where('LaborCost', array('id' => $id));
        return $query->result();
    }

}
