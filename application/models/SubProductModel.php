<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SubProductModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select("s.*,m.name as mainProductName", false);
        $this->db->from('SubProduct s');
        $this->db->join('MainProduct m', 's.MainProductId = m.id', 'left');

        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }

    function saveData($data) {
        $this->db->insert('SubProduct', $data);
    }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('SubProduct', $data);
    }

    public function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('SubProduct');
    }

    public function selectData($id) {
        $query = $this->db->get_where('SubProduct', array('id' => $id));
        return $query->result();
    }

}
