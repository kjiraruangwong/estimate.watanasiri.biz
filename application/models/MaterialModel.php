<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MaterialModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select("m.*,ct.name as costTypeName,mt.name as materialTypeName, ms.name as metalShapeName, es.name as extraShapeName,u.name as unitName", false);
        $this->db->from('Material m');
        $this->db->join('CostType ct', 'm.costTypeId = ct.id', 'left');
        $this->db->join('MaterialType mt', 'm.materialTypeId = mt.id', 'left');
        $this->db->join('MetalShape ms', 'm.metalShapeId = ms.id', 'left');
        $this->db->join('ExtraShape es', 'm.extraShapeId = es.id', 'left');
        $this->db->join('Unit u', 'm.unitId = u.id', 'left');

        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }

    function saveData($data) {
        $this->db->insert('Material', $data);
    }

    public function selectData($id) {
        $this->db->select("m.*,ct.name as costTypeName,mt.name as materialTypeName, ms.name as metalShapeName, es.name as extraShapeName,u.name as unitName", false);
        $this->db->from('Material m');
        $this->db->join('CostType ct', 'm.costTypeId = ct.id', 'left');
        $this->db->join('MaterialType mt', 'm.materialTypeId = mt.id', 'left');
        $this->db->join('MetalShape ms', 'm.metalShapeId = ms.id', 'left');
        $this->db->join('ExtraShape es', 'm.extraShapeId = es.id', 'left');
        $this->db->join('Unit u', 'm.unitId = u.id', 'left');
        $this->db->where('m.id', $id);
        
        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('Material', $data);
    }

    public function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('Material');
    }    
    
}
