<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select("c.*,ct.name as customerTypeName,cc.firstName as contactFirstName, cc.lastName as contactLastName,cc.tel as contactTel", false);
        $this->db->from('Customer c');
        $this->db->join('CustomerType ct', 'c.customerTypeId = ct.id', 'left');
        $this->db->join('CustomerContactPerson cc', 'c.id = cc.customerId', 'left');

        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }

    function saveData($data) {
        $this->db->insert('Customer', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function selectData($id) {
        // get data from model
        $response = array();

        // Select record
        $this->db->select("c.*,cc.*,ct.id as customerTypeId,ct.name as customerTypeName,c.id as customerId", false);
        $this->db->from('Customer c');
        $this->db->join('CustomerType ct', 'c.customerTypeId = ct.id', 'left');
        $this->db->join('CustomerContactPerson cc', 'c.id = cc.customerId', 'left');
        $this->db->where('c.id = ' . $id);

        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }

    function saveContact($data) {
        $this->db->insert('CustomerContactPerson', $data);
    }

    public function deleteData($id){
        $this -> db -> where('id', $id);
        $this -> db -> delete('Customer');
        
        $this -> db -> where('customerId', $id);
        $this -> db -> delete('CustomerContactPerson');        
    }    
    
    function updateData($id, $data){
        $this->db->where('id', $id);
        $update = $this->db->update('Customer', $data);
    }    
    
    public function selectContact($id) {
        // get data from model
        $response = array();

        // Select record
        $this->db->select("c.*,cc.*,c.id as customerId", false);
        $this->db->from('CustomerContactPerson cc');
        $this->db->join('Customer c', 'cc.customerId = c.id', 'left');
        $this->db->where('cc.customerId = ' . $id);

        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }

}
