<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MetalShapeModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select('*');
        $q = $this->db->get('MetalShape');
        $response = $q->result();

        return $response;
    }

    function saveData($data) {
        $this->db->insert('MetalShape', $data);
    }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('MetalShape', $data);
    }

    public function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('MetalShape');
    }

    public function selectData($id) {
        $query = $this->db->get_where('MetalShape', array('id' => $id));
        return $query->result();
    }

}
