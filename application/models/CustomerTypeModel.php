<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerTypeModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select('*');
        $q = $this->db->get('CustomerType');
        $response = $q->result();

        return $response;
    }

    function saveData($data) {
        $this->db->insert('CustomerType', $data);
    }

    function updateData($id, $data){
        $this->db->where('id', $id);
        $update = $this->db->update('CustomerType', $data);
    }

    public function deleteData($id){
        $this -> db -> where('id', $id);
        $this -> db -> delete('CustomerType');
    }

    public function selectData($id){
        $query = $this->db->get_where('CustomerType', array('id' => $id));
        return $query->result();            
    }

}
