<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerContactPersonModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select('*');
        $q = $this->db->get('CustomerContactPerson');
        $response = $q->result();

        return $response;
    }

    function saveData($data) {
        $this->db->insert('CustomerContactPerson', $data);
    }

    public function selectAllDataByCustomer($customerId) {

        // get data from model
        $response = array();

        $query = $this->db->get_where('CustomerContactPerson', array('customerId' => $customerId));
        $response = $query->result();
        return $response;
    }

    public function selectData($id) {
        $query = $this->db->get_where('CustomerContactPerson', array('id' => $id));
        return $query->result();
    }     
    
}
