<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserRoleModel extends CI_Model  {

	public function __construct() {
		parent::__construct();
	}

  	public function selectAllData(){

	    // get data from model
	    $response = array();
	 
	    // Select record
	    $this->db->select('*');
	    $q = $this->db->get('UserRole');
	    $response = $q->result();
	
	    return $response;

  	}
	
	function saveData($data){
		$this->db->insert('UserRole', $data);
	}

	public function selectData($id){
            $query = $this->db->get_where('UserRole', array('id' => $id));
            return $query->result();            
    }

    function updateData($id, $data){
            $this->db->where('id', $id);
            $update = $this->db->update('UserRole', $data);
        }
        
    public function do_delete_row($id){
            $this -> db -> where('id', $id);
            $this -> db -> delete('UserRole');
        }

}