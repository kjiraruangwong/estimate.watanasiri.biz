<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LaborCostTypeModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select('*');
        $q = $this->db->get('LaborCostType');
        $response = $q->result();

        return $response;
    }

    function saveData($data) {
        $this->db->insert('LaborCostType', $data);
    }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('LaborCostType', $data);
    }

    public function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('LaborCostType');
    }

    public function selectData($id) {
        $query = $this->db->get_where('LaborCostType', array('id' => $id));
        return $query->result();
    }

}
