<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProjectModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select("p.*,c.name as customerName", false);
        $this->db->from('Project p');
        $this->db->join('Customer c', 'p.customerId = c.id', 'left');

        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }

    public function selectAllDataByCustomer($customerId) {
        // get data from model
        $response = array();

        $query = $this->db->get_where('Project', array('customerId' => $customerId));
        $response = $query->result();
        return $response;
    }    
    
    function saveData($data) {
        $this->db->insert('Project', $data);
    }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('Project', $data);
    }

    public function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('Project');
    }

    public function selectData($id) {
        $query = $this->db->get_where('Project', array('id' => $id));
        return $query->result();
    }    
    
    
}
