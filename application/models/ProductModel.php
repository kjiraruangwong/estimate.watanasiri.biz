<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProductModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select("p.*,m.name as mainProductName,s.name as subProductName,f.name as finishingName", false);
        $this->db->from('Product p');
        $this->db->join('MainProduct m', 'p.MainProductId = m.id', 'left');
        $this->db->join('SubProduct s', 'p.SubProductId = s.id', 'left');
        $this->db->join('Finishing f', 'p.FinishingId = f.id', 'left');
        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }

    function saveData($data) {
        $this->db->insert('Product', $data);
    }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('Product', $data);
    }

    public function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('Product');
    }

    public function selectData($id) {
        $query = $this->db->get_where('Product', array('id' => $id));
        return $query->result();
    }

}
