<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WasteMaterialModel extends CI_Model  {

	public function __construct() {
		parent::__construct();
	}

  	public function selectAllData(){

	    // get data from model
	    $response = array();
	 
	    // Select record
	    $this->db->select("wm.*,ct.name as costTypeName,mt.name as materialTypeName,mtw.name as wasteMaterialTypeName, ms.name as metalShapeName, ew.name as extraWasteName,u.name as unitName", false);
		$this->db->from('WasteMaterial wm');
		$this->db->join('CostType ct', 'wm.costTypeId = ct.id', 'left');
		$this->db->join('MaterialType mt', 'wm.materialTypeId = mt.id', 'left');
		$this->db->join('MaterialType mtw', 'wm.wasteMaterialTypeId = mtw.id', 'left');
		$this->db->join('MetalShape ms', 'wm.metalShapeId = ms.id', 'left');
		$this->db->join('ExtraWaste ew', 'wm.extraWasteId = ew.id', 'left');
		$this->db->join('Unit u', 'wm.unitId = u.id', 'left');

	    $q = $this->db->get();
	    $response = $q->result();
	    return $response;

  	}
	
        public function selectData($id) {
	    $this->db->select("wm.*,ct.name as costTypeName,mt.name as materialTypeName,mtw.name as wasteMaterialTypeName, ms.name as metalShapeName, ew.name as extraWasteName,u.name as unitName", false);
		$this->db->from('WasteMaterial wm');
		$this->db->join('CostType ct', 'wm.costTypeId = ct.id', 'left');
		$this->db->join('MaterialType mt', 'wm.materialTypeId = mt.id', 'left');
		$this->db->join('MaterialType mtw', 'wm.wasteMaterialTypeId = mtw.id', 'left');
		$this->db->join('MetalShape ms', 'wm.metalShapeId = ms.id', 'left');
		$this->db->join('ExtraWaste ew', 'wm.extraWasteId = ew.id', 'left');
		$this->db->join('Unit u', 'wm.unitId = u.id', 'left');
            $this->db->where('wm.id', $id);

            $q = $this->db->get();
            $response = $q->result();
            return $response;
        }
        
        public function deleteData($id) {
            $this->db->where('id', $id);
            $this->db->delete('WasteMaterial');
        }        
        
	function saveData($data){
		$this->db->insert('WasteMaterial', $data);
	}
        
        function updateData($id, $data) {
            $this->db->where('id', $id);
            $update = $this->db->update('WasteMaterial', $data);
        }        

}