<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UnitModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select('*');
        $q = $this->db->get('Unit');
        $response = $q->result();

        return $response;
    }

    function saveData($data) {
        $this->db->insert('Unit', $data);
    }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('Unit', $data);
    }

    public function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('Unit');
    }

    public function selectData($id) {
        $query = $this->db->get_where('Unit', array('id' => $id));
        return $query->result();
    }

}
