<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FinishingModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select('*');
        $q = $this->db->get('Finishing');
        $response = $q->result();

        return $response;
    }

    function saveData($data) {
        $this->db->insert('Finishing', $data);
    }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('Finishing', $data);
    }

    public function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('Finishing');
    }

    public function selectData($id) {
        $query = $this->db->get_where('Finishing', array('id' => $id));
        return $query->result();
    }

}
