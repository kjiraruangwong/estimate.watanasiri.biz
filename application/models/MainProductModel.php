<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MainProductModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select('*');
        $q = $this->db->get('MainProduct');
        $response = $q->result();

        return $response;
    }

    function saveData($data) {
        $this->db->insert('MainProduct', $data);
    }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('MainProduct', $data);
    }

    public function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('MainProduct');
    }

    public function selectData($id) {
        $query = $this->db->get_where('MainProduct', array('id' => $id));
        return $query->result();
    }

}
