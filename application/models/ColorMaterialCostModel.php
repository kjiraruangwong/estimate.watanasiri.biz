<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ColorMaterialCostModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function selectAllData() {

        // get data from model
        $response = array();

        // Select record
        $this->db->select("cmc.*,mt.name as materialType,ct.name as costType", false);
        $this->db->from('ColorMaterialCost cmc');
        $this->db->join('MaterialType mt', 'cmc.materialTypeId = mt.id', 'left');
        $this->db->join('CostType ct', 'cmc.costTypeId = ct.id', 'left');
        $q = $this->db->get();
        $response = $q->result();
        return $response;
    }

    function saveData($data) {
        $this->db->insert('ColorMaterialCost', $data);
    }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('ColorMaterialCost', $data);
    }

    public function deleteData($id) {
        $this->db->where('id', $id);
        $this->db->delete('ColorMaterialCost');
    }

    public function selectData($id) {
        $query = $this->db->get_where('ColorMaterialCost', array('id' => $id));
        return $query->result();
    }

}
