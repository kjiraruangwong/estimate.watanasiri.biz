<style>
    h3 {
        color: #000;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

    input[type=checkbox]
{
    width:2vw;
    height:2vh;
}
    
</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มคำนวนต้นทุน</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row formbox box4">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">ใบคิดรายการต้นทุน</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12"> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >บริษัท
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <select class="form-control" required id="customerId" onchange="customerChange();">
                                                <option value="">-- กรุณาเลือก --</option>
                                                <?php
                                                foreach ($customers as $customer) {
                                                    echo "<option value='" . $customer->id . "'>" . $customer->name . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >โครงการ
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <select class="form-control" required id="projectId">
                                                <option value="">-- กรุณาเลือก --</option>                                                
                                                <?php
                                                foreach ($projects as $project) {
                                                    echo "<option value='" . $project->id . "'>" . $project->name . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >เสนอ
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <select class="form-control" required id="customerContactId" onchange="contactPersonChange();">
                                                <option value="">-- กรุณาเลือก --</option>
                                                <?php
                                                foreach ($customerContactPersons as $customerContactPerson) {
                                                    echo "<option value='" . $customerContactPerson->id . "'>" . $customerContactPerson->firstName . " " . $customerContactPerson->lastName . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >โทร
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <input type="text"  class="form-control col-md-7 col-xs-12" id="contactTelNo">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fax
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <input type="text"  class="form-control col-md-7 col-xs-12" id="contactFaxNo">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12"> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >ชื่อสินค้า
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <select class="form-control" required>
                                                <option value="">-- กรุณาเลือก --</option>
                                                <?php
                                                foreach ($products as $product) {
                                                    echo "<option value='" . $product->id . "'>" . $product->name . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Dwg.No.
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <input type="text"  class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >ประเภทงาน
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <select class="form-control" required>
                                                <option value="">-- กรุณาเลือก --</option>
                                                <?php
                                                foreach ($customerTypes as $customerType) {
                                                    echo "<option value='" . $customerType->id . "'>" . $customerType->name . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >วัตถุดิบ
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <select class="form-control">
                                                <option value="">-- กรุณาเลือก --</option>
                                                <?php
                                                foreach ($materialTypes as $materialType) {
                                                    echo "<option value='" . $materialType->id . "'>" . $materialType->name . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Finish
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <select class="form-control">
                                                <option value="">-- กรุณาเลือก --</option>
                                                <?php
                                                foreach ($finishings as $finishing) {
                                                    echo "<option value='" . $finishing->id . "'>" . $finishing->name . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12"> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >วันที่เสนอ
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <fieldset>
                                                <div class="control-group ">
                                                    <div class="controls">
                                                        <div class="input-prepend input-group">
                                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                            <input type="text" class="form-control" id="single_cal1" placeholder="วันที่เสนอ." aria-describedby="inputSuccess2Status">
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Rev.
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <input type="text"  class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >แผ่นที่
                                        </label>
                                        <div class="col-md-9 col-sm-12">
                                            <input type="text"  class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >พื้นที่รวม
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  class="form-control col-md-7 col-xs-12" readonly>
                                        </div>
                                        <label class="control-label">ตรม.</label>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >ราคา/ม2.
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  class="form-control col-md-7 col-xs-12" readonly>
                                        </div>
                                        <label class="control-label">บาท</label>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12"> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >ต้นทุนคิด
                                        </label>
                                        <div class="col-md-3 col-sm-3">
                                            <input type="text"  class="form-control col-md-7 col-xs-12" id="calculateCost" value="1.0" onchange="javascript:predictionCalculate();">
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <select class="form-control">
                                                <option value="">-- กรุณาเลือก --</option>
                                                <?php
                                                foreach ($units as $unit) {
                                                    echo "<option value='" . $unit->id . "'>" . $unit->name . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >จำนวนที่เสนอ
                                        </label>
                                        <div class="col-md-3 col-sm-3">
                                            <input type="text"  class="form-control col-md-7 col-xs-12">
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <select class="form-control">
                                                <option value="">-- กรุณาเลือก --</option>
                                                <?php
                                                foreach ($units as $unit) {
                                                    echo "<option value='" . $unit->id . "'>" . $unit->name . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >%Direct Labor
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >น้ำหนักรวม
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  class="form-control col-md-7 col-xs-12" readonly>
                                        </div>
                                        <label class="control-label">กก.</label>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >ราคา/กก.
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  class="form-control col-md-7 col-xs-12" readonly>
                                        </div>
                                        <label class="control-label">บาท</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row formbox box1">
        <table class="table table-bordered" id="materialTable">
            <input type="hidden" name="materialRowCount" id="materialRowCount" value="4">
            <thead>
                <tr>
                    <th>ลำดับ</th>
                    <th>ชื่อวัตถุดิบ</th>
                    <th class="col-md-1">ทำสี</th>
                    <th>F/S</th>
                    <th colspan="3" class="text-center">ขนาดตัด</th>
                    <th>จำนวนชิ้นที่ตัดได้</th>
                    <th class="col-md-1 text-center" colspan="2">จำนวน</th>
                    <th>ราคา@</th>
                    <th>รวมเป็นเงิน</th>
                </tr>
            </thead>
            <?php for($i=1;$i<=4;$i++){ ?>
            <tbody id="materialRow<?php echo($i);?>">
                <tr>
                    <td class="text-center"><span id="materialRowOrder<?php echo($i);?>"><?php echo $i;?>.</span></td>
                    <input type="hidden" name="originalWidth<?php echo $i;?>" id="originalWidth<?php echo $i;?>">
                    <input type="hidden" name="originalHeight<?php echo $i;?>" id="originalHeight<?php echo $i;?>"> 
                    <input type="hidden" name="unitName<?php echo $i;?>" id="unitName<?php echo $i;?>">
                    <input type="hidden" name="fullArea<?php echo $i;?>" id="fullArea<?php echo $i;?>">   
                    <input type="hidden" name="weightPerItem<?php echo $i;?>" id="weightPerItem<?php echo $i;?>">  
                    <input type="hidden" name="materialCategory<?php echo $i;?>" id="materialCategory<?php echo $i;?>">
                    <input type="hidden" name="areaPerUnit<?php echo $i;?>" id="areaPerUnit<?php echo $i;?>">
                    <input type="hidden" name="cutAmount<?php echo $i;?>" id="cutAmount<?php echo $i;?>">
                    <input type="hidden" name="totalArea<?php echo $i;?>" id="totalArea<?php echo $i;?>">
                    <input type="hidden" name="weightPerUnit<?php echo $i;?>" id="weightPerUnit<?php echo $i;?>">
                    <input type="hidden" name="totalWeight<?php echo $i;?>" id="totalWeight<?php echo $i;?>">
                    <td id="selectColumn<?php echo $i;?>">
                        <select class="selectpicker show-tick form-control" data-live-search="true" onchange="materialChange(<?php echo $i;?>);" id="materialId<?php echo $i;?>">
                            <option value="">-- กรุณาเลือก --</option>
                            <?php
                            foreach ($materials as $material) {
                                echo "<option value='" . $material->id . "'>" . $material->name . "</option>";
                            }
                            ?>
                        </select>

                    </td>
                    <td>
                        <select id="colorMaterialCost<?php echo $i;?>" class="form-control col-md-7 col-xs-12" >
                            <option value="">-- กรุณาเลือก --</option>
                            <?php
                            foreach ($colorMaterialCosts as $colorMaterialCost) {
                                echo "<option value='" . $colorMaterialCost->id . "'>" . $colorMaterialCost->name . "</option>";
                            }
                            ?>
                        </select>
                    </td>
                    <td id="specialCutBox<?php echo $i;?>">
                        <label><input type="checkbox" name="specialCut<?php echo $i;?>" id="specialCut<?php echo $i;?>" value="Y" required="" class="flat" onclick="calculatePrice(<?php echo $i;?>);"></label>
                    </td>
                    <td class="col-md-1 text-center">
                        <input type="text" class="form-control col-md-2 col-xs-2" name="userWidth<?php echo $i;?>" id="userWidth<?php echo $i;?>" onchange="calculatePrice(<?php echo $i;?>);">
                    </td>
                    <td class="text-center" width="1%">
                        <div class="form-group align-items-center">
                            <label class="control-label" >X </label>                                      
                        </div>
                    </td>
                    <td class="col-md-1 text-center">
                        <input type="text" class="form-control col-md-2 col-xs-2" name="userHeight<?php echo $i;?>" id="userHeight<?php echo $i;?>" onchange="calculatePrice(<?php echo $i;?>);">
                    </td>                    
                    <td><span id="cutAmountTxt<?php echo $i;?>"></span> </td>
                    <td class="col-md-1 text-center">
                        <input type="number" class="form-control col-md-2 col-xs-2" id="materialAmountId<?php echo $i;?>" name="materialAmount<?php echo $i;?>" onchange="calculatePrice(<?php echo $i;?>);">
                    </td>
                    <td class="col-md-1">
                        <select class="form-control col-md-7 col-xs-2">
                            <option value="">-- กรุณาเลือก --</option>
                            <?php
                            foreach ($units as $unit) {
                                echo "<option value='" . $unit->id . "'>" . $unit->name . "</option>";
                            }     
                            ?>
                        </select>
                    </td>
                    <td> 
                        <select class="form-control" id="materialPriceId<?php echo $i;?>" onchange="calculatePrice(<?php echo $i;?>);">
                            <option value="">-- กรุณาเลือก --</option>                         
                        </select>
                    </td>
                    <td> 
                        <label id="calculationPrice<?php echo $i;?>">0.00</label>
                    </td>
                </tr>
            </tbody>
            <?php }?>
        </table>
        <tbody>
            <tr>                
                <td > 
                    <button style="float: right;" class="btn btn-success col-md-1 col-xs-1" type="button" onclick="javascript:sumColorMaterialCost();">สรุปค่าทำสี</button>
                    <button style="float: right;" class="btn btn-primary cancelbt" type="button" onclick="javascript:deleteMaterialRow();">ลดวัตถุดิบ</button>
                    <button  style="float: right;" class="btn btn-primary col-md-1 col-xs-1" type="button" onclick="javascript:addMaterialRow();">เพิ่มวัตถุดิบ</button>
                </td>
            </tr>
        </tbody>        
    </div>   
            <h2><b>วัตถุสินเปลืองอื่นๆ </b></h2>

    <div class="row formbox box1">
        <table class="table table-bordered" id="wasteMaterialTable">
            <input type="hidden" name="wasteMaterialRowCount" id="wasteMaterialRowCount" value="2">
            <thead>
                <tr>
                    <th class="text-center">ลำดับ</th>
                    <th class="text-center">ชื่อวัตถุสิ้นเปลือง</th>
                    <th class="text-center col-md-2" colspan="2">จำนวน</th>
                    <th class="text-center col-md-2">ราคา @</th>
                    <th class="text-center">รวมเป็นเงิน</th>
                </tr>
            </thead>
            <?php for($i=1;$i<=2;$i++){ ?>
            <tbody id="wasteMaterialRow<?php echo($i);?>">                
                <tr>
                    <td width="5%" id="wasteMaterialCounter<?php echo($i);?>"><?php echo $i;?>.</td>
                    <td class="col-md-2" id="wasteMaterialSelectColumn<?php echo $i;?>">
                        <select class="selectpicker show-tick form-control" data-live-search="true" onchange="wasteMatChange(<?php echo $i;?>);" id="wasteMaterialId<?php echo $i;?>">
                            <option value="">-- กรุณาเลือก --</option>
                            <?php
                            foreach ($wasteMaterials as $wasteMaterial) {
                                echo "<option value='" . $wasteMaterial->id . "'>" . $wasteMaterial->name . "</option>";
                            }  
                            ?>
                        </select>                       
                    </td>
                    <td class="col-md-1">
                        <input type="text" class="form-control col-md-7 col-xs-2" id="otherMatAmount<?php echo $i;?>" name="otherMatAmount<?php echo $i;?>" onchange="javascript:otherMatCal(<?php echo $i;?>);">
                    </td>
                    <td class="col-md-1">
                        <select class="form-control col-md-7 col-xs-2" id="otherMatUnit<?php echo $i;?>" name="otherMatUnit<?php echo $i;?>">
                            <option value="">-- กรุณาเลือก --</option>
                            <?php
                            foreach ($units as $unit) {
                                echo "<option value='" . $unit->id . "'>" . $unit->name . "</option>";
                            }     
                            ?>
                        </select>
                    </td>                    
                    <td class="col-md-1">
                        <select class="form-control" id="otherMatPrice<?php echo $i;?>"onchange="javascript:otherMatCal(<?php echo $i;?>);">
                            <option value="">-- กรุณาเลือก --</option>                         
                        </select>                        
                    </td>
                    <td class="col-md-1 text-right"> <label id="otherMatSum<?php echo $i;?>">0.00</label></td>
                </tr>                
            </tbody>
            <?php }?>            
        </table>
        <tbody>
            <tr>                
                <td > 
                    <button style="float: right;" class="btn btn-primary cancelbt" type="button" onclick="javascript:deleteWasteMaterialRow();">ลดวัตถุสิ้นเปลือง</button>
                    <button  style="float: right;" class="btn btn-primary" type="button" onclick="javascript:addWasteMaterialRow();">เพิ่มวัตถุสิ้นเปลือง</button>
                </td>
            </tr>
        </tbody>         
    </div>
    <h2><b>สรุปค่าทำสี </b></h2>

    <div class="row formbox box1">
        <input type="hidden" name="ColorMaterialCostRowCount" id="ColorMaterialCostRowCount" value="0">
        <table class="table table-bordered" id="ColorMaterialCostTable">            
            <thead>
                <tr>
                    <th class="text-center">ลำดับ</th>
                    <th class="text-center">รายการ</th>
                    <th class="text-center col-md-2" colspan="2">จำนวน</th>
                    <th class="text-center col-md-2">ราคา @</th>
                    <th class="text-center">รวมเป็นเงิน</th>
                </tr>
            </thead>                        
        </table>
    </div>
    
    <h2><b>วัตถุสิ้นเปลือง/Packing </b></h2>

    <div class="row formbox box1">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ลำดับ</th>
                    <th class="text-center">รายการ</th>
                    <th class="text-center col-md-2" colspan="2">จำนวน</th>
                    <th class="text-center col-md-2">ราคา @</th>
                    <th class="text-center">รวมเป็นเงิน</th>
                </tr>
            </thead>
            <?php for($i=1;$i<=2;$i++){ ?>
            <tbody>                
                <tr>
                    <td width="5%"><?php echo $i;?>.</td>
                    <?php if ($i ==1){ ?>
                        <td class="col-md-2">วัตถุสิ้นเปลือง</td>
                    <?php } else{ ?>
                        <td class="col-md-2">อุปกรณ์ / Packing</td>
                    <?php }?>
                    <td class="col-md-1">
                        <input type="text" class="form-control col-md-7 col-xs-2" id="wasteMatAmount<?php echo $i;?>" name="wasteMatAmount<?php echo $i;?>" onchange="javascript:wasteMatCal(<?php echo $i;?>);">
                    </td>
                    <td class="col-md-1">
                        <select class="form-control col-md-7 col-xs-2" id="wasteMatUnit<?php echo $i;?>" name="wasteMatUnit="wasteMatAmt<?php echo $i;?>">
                            <option value="">-- กรุณาเลือก --</option>
                            <?php
                            foreach ($units as $unit) {
                                echo "<option value='" . $unit->id . "'>" . $unit->name . "</option>";
                            }     
                            ?>
                        </select>
                    </td>                    
                    <td class="col-md-1">
                        <input type="text"  class="form-control col-md-7 col-xs-12" id="wasteMatPrice<?php echo $i;?>" name="wasteMatPrice<?php echo $i;?>" onchange="javascript:wasteMatCal(<?php echo $i;?>);">
                    </td>
                    <td class="col-md-1 text-right"> <label id="wasteMatSum<?php echo $i;?>">0.00</label></td>
                </tr>                
            </tbody>
            <?php }?>            
        </table>
    </div>
    
    <h2>รวมค่า Material</h2>
    <div class="row formbox box2">
        <table class="table table-bordered">
            <tbody>                
                <tr>
                    <td align="right" colspan="2">รวมเป็นเงิน Material</td>
                    <td> <span id="materialPrice">0.00</span></td>
                </tr>
            </tbody>            
            <tbody>
                <tr>
                    <td align="right" class="col-md-9">
                        ค่าใช้จ่าย 
                    </td>
                    <td align="right" class="col-md-2">

                        <div class="d-inline-block col-md-11">   
                            <input type="text" id="materialCostPercent" class="form-control" onchange="javascript:calculateCost();">
                        </div> 

                        <div class="d-inline-block">   
                            %
                        </div>

                    </td>
                    <td align="left" class="col-md-1">
                        <span id="materialCost">0.00</span>
                    </td>                    
                </tr>
            </tbody>

            <tbody>                
                <tr>
                    <td align="right" colspan="2">รวมเป็นเงิน Material</td>
                    <td><span id="materialNetPrice">0.00</span></td>
                </tr>
            </tbody>
        </table>
    </div>

    <div>
    <h2>ค่าแรง</h2> 
    </div>

    <div class="row formbox box3">

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" colspan="2">
                    <div class="d-inline-block col-md-2">    
                        ค่าแรง     
                    </div>
                        
                    <div class="d-inline-block col-md-7">
                        <select class="form-control" id="calculationTypeId" onchange="javascript:changeCalculationType();">
                            <option value="0">แบบไม่แจกแจงค่าแรง</option>
                            <option value="1">แบบแจกแจงค่าแรง</option>                            
                        </select> 
                        </div>
                    </th>
                    <th colspan="2" class="text-center" >คิดเป็น (%,ชม.)</th>
                    <th colspan="2" class="text-center" >% Direct Labor</th>
                    <th class="text-center" colspan="2">จำนวน</th>
                    <th class="text-center">ราคา @</th>
                    <th class="text-center">รวมเป็นเงิน</th>
                </tr>
            </thead>
            
            <tbody>
                <tr>
                    <td>- Direct Labor</td>
                    <td>
                        <div class="d-inline-block col-md-3">   
                            ค่าแรงรายวัน
                        </div>                        
                        <div class="d-inline-block col-md-5">   
                            <input type="text" class="form-control" id="dailyWage"  onchange="javascript:calculateWage();">
                        </div> 

                        <div class="d-inline-block col-md-3"> 
                            บาท/วัน
                        </div>
                    </td>
                    <td><label id="wagePerHour">0.00</label></td>
                    <td class="col-md-1">%</td>
                    <td colspan="2"><input type="text" class="form-control col-md-9 col-xs-12" id="directLaberPercent" onchange="javascript:calculateWage();"></td>
                    <td></td>
                    <td></td>
                            <?php
                            $laborCostPrice = 0.0;
                            foreach ($laborCosts as $laborCost) {
                                if ($laborCost->name == 'Direct Labor'){
                                    $laborCostPrice = $laborCost->price;
                                }
                            }
                            ?>                    
                    <td><label id="directLaborPrice"><?php echo number_format($laborCostPrice,2);?></label></td>
                    <td><label id="totalDirectLabor">0.00</label></td>                                         
                </tr>
            </tbody>
            <thead id="directLaborTableHead" style="display:none;">
                <tr>
                    <th class="text-center">ค่าแรง</th>
                    <th class="text-center">จำนวนคน</th>
                    <th colspan="2" class="text-center">จำนวน</th>
                    <th colspan="2" class="text-center">เวลา @</th>
                    <th colspan="2" class="text-center" colspan="2">รวมเวลา</th>                    
                    <th class="text-center">ราคา @</th>
                    <th class="text-center">รวมเป็นเงิน</th>
                </tr>
            </thead>
            
            <tbody id="directLaborTable" style="display:none;">
                <input type="hidden" id="directLaborRecordCount" value="3">
                <?php for($i=1;$i<=3;$i++){ ?>
                <tr id="directLaborRow<?php echo $i;?>">
                    <td>
                        <select class="form-control" id="laborCostId<?php echo $i;?>" value="" onchange="laborCostChange(<?php echo $i;?>);">
                            <option value="">-- กรุณาเลือก --</option>
                            <?php
                            foreach ($laborCosts as $laborCost) {
                                echo "<option value='" . $laborCost->id . ";" . $laborCost->price . ";" . $laborCost->unit . "'>" . $laborCost->name . "</option>";
                            }
                            ?>
                        </select>
                        <input type="hidden" id="laborCostPrice<?php echo $i;?>" value="">
                    </td>
                    <td>
                        <input type="text"  class="form-control col-md-9 col-xs-12" id="manAmount<?php echo $i;?>" value="" onchange="laborTimeChange(<?php echo $i;?>);">
                    </td>
                    <td>
                        <input type="text"  class="form-control col-md-7 col-xs-12" id="laborAmount<?php echo $i;?>" value="" onchange="laborTimeChange(<?php echo $i;?>);">
                    </td>
                    <td class="col-md-1" id="laborAmountUnit<?php echo $i;?>">
                        ครั้ง
                    </td>
                    <td>
                        <input type="text"  class="form-control col-md-9 col-xs-12" id="laborTime<?php echo $i;?>" value="" onchange="laborTimeChange(<?php echo $i;?>);">
                    </td>
                    <td class="col-md-1" id="laborTimeUnit<?php echo $i;?>">
                        นาที
                    </td>                    
                    <td>
                        <label id="summaryLaborTime<?php echo $i;?>"></label>
                    </td>  
                    <td id="laborTimeUnitTotal<?php echo $i;?>">
                        นาที
                    </td>
                    <td> <label id="LM<?php echo $i;?>"></label></td>
                    <td> <label id="totalCost<?php echo $i;?>"></label></td>
                </tr>
                <?php } ?>
                </tbody>
        <tbody id="directLaborTableButton" style="display:none;">
                <tr>
                    <td colspan="10">
                    <button style="float: right;" class="btn btn-primary cancelbt" type="button" onclick="javascript:deleteDirectLaborRow();">ลดค่าแรง</button>
                    <button  style="float: right;" class="btn btn-primary col-md-1 col-xs-1" type="button" onclick="javascript:addDirectLaborRow();">เพิ่มค่าแรง</button>                               
                    </td>
                </tr>
        </tbody>
        <tbody>
                <tbody id="overheadTable">
                    <input type="hidden" id="overheadRowCount" value="2">
                    <?php for($i=1;$i<=2;$i++){ ?>
                        <tr id="overheadRow<?php echo $i;?>">
                            <td colspan="2">
                                <select class="form-control" id="laborCostType1Id<?php echo $i;?>" onchange="javascript:laborCostType1Change(<?php echo $i;?>);">
                                    <option value="">-- กรุณาเลือก --</option>
                                    <?php
                                    foreach ($laborType1Costs as $laborType1Cost) {
                                        echo "<option value='" . $laborType1Cost->id . ";" . $laborType1Cost->price . ";" . $laborType1Cost->unit . "'>" . $laborType1Cost->name . "</option>";
                                    }
                                    ?>
                                </select>
                            </td>
                            <td>

                            </td>
                            <td class="col-md-1">

                            </td>
                            <td class="col-md-1 text-right">
                                <label id='laborCostType1Percent<?php echo $i;?>'></label>
                            </td>                    
                            <td>
                                <select id="laborCostType1Method<?php echo $i;?>" style="display:none" class="form-control" onchange="javascript:laborCostType1Change(1);">
                                    <option value="">-- กรุณาเลือกวิธีคิดค่าแรง --</option>
                                    <option value="1">คิดเป็น %</option>
                                    <option value="2">คิดตามจำนวนชั่วโมง</option>
                                </select>
                            </td>
                            <td>
                                <input type="text" class="form-control col-md-9 col-xs-12" id="laborCostType1Amount<?php echo $i;?>" onchange="javascript:laborCostType1Change(<?php echo $i;?>);">
                            </td>
                            <td>
                                <label id="laborCostType1UnitText<?php echo $i;?>"></label>
                                <input type="hidden" id="laborCostType1Unit<?php echo $i;?>">                        
                            </td>
                            <td> 
                                <label id="laborCostType1UnitPriceText<?php echo $i;?>"></label>
                            </td>
                            <td> <label id="laborCostType1Total<?php echo $i;?>"></label></td>
                        </tr>
                    <?php } ?>
                </tbody>

                <tr>
                    <td colspan="10">
                        <button style="float: right;" class="btn btn-primary cancelbt" type="button" onclick="javascript:deleteOverheadRow();">ลดค่าโสหุ้ย</button>
                        <button  style="float: right;" class="btn btn-primary col-md-1 col-xs-1" type="button" onclick="javascript:addOverheadRow();">เพิ่มค่าโสหุ้ย</button>                                 
                    </td>
                </tr>

                <tr>
                    <td colspan="8">
                        <b>- โสหุ้ยการผลิต</b>                               
                    </td>
                    <td  align="right">
                        <div class="col-md-10">
                            <input type="text"  class="form-control col-md-9 col-xs-12" value="10" id="overheadPercent" onChange="javascript:calculateOverHead();"> 
                        </div>
                        <div><b>%</b></div>
                    </td>                    
                    <td><label id="overHeadValue">0.00</label></td>
                </tr>

                <tbody id="operationTable">
                    <input type="hidden" id="operationRowCount" value="2">
                    <?php for($i=1;$i<=2;$i++){ ?>
                        <tr id="operationRow<?php echo $i;?>">
                            <td colspan="2">
                                <select class="form-control" id="laborCostType2Id<?php echo $i;?>" onchange="javascript:laborCostType2Change(<?php echo $i;?>);">
                                    <option value="">-- กรุณาเลือก --</option>
                                    <?php
                                    foreach ($laborType2Costs as $laborType2Cost) {
                                        echo "<option value='" . $laborType2Cost->id . ";" . $laborType2Cost->price . ";" . $laborType2Cost->unit . "'>" . $laborType2Cost->name . "</option>";
                                    }
                                    ?>
                                </select>
                            </td>
                            <td>

                            </td>
                            <td class="col-md-1">

                            </td>
                            <td class="col-md-1 text-right">
                                <label id='laborCostType2Percent<?php echo $i;?>'></label>
                            </td>                    
                            <td>
                                <select id="laborCostType2Method<?php echo $i;?>" style="display:none" class="form-control" onchange="javascript:laborCostType2Change(1);">
                                    <option value="">-- กรุณาเลือกวิธีคิดค่าแรง --</option>
                                    <option value="1">คิดเป็น %</option>
                                    <option value="2">คิดตามจำนวนชั่วโมง</option>
                                </select>
                            </td>
                            <td>
                                <input type="text" class="form-control col-md-9 col-xs-12" id="laborCostType2Amount<?php echo $i;?>" onchange="javascript:laborCostType2Change(<?php echo $i;?>);">
                            </td>
                            <td>
                                <label id="laborCostType2UnitText<?php echo $i;?>"></label>
                                <input type="hidden" id="laborCostType2Unit<?php echo $i;?>">                        
                            </td>
                            <td> 
                                <label id="laborCostType2UnitPriceText<?php echo $i;?>"></label>
                            </td>
                            <td> <label id="laborCostType2Total<?php echo $i;?>"></label></td>
                        </tr>
                    <?php } ?>
                </tbody>

                <tr>
                    <td colspan="10">
                        <button style="float: right;" class="btn btn-primary cancelbt" type="button" onclick="javascript:deleteOperationRow();">ลดค่าดำเนินการ</button>
                        <button  style="float: right;" class="btn btn-primary" type="button" onclick="javascript:addOperationRow();">เพิ่มค่าดำเนินการ</button>                                
                    </td>
                </tr>      

                <tr>
                    <td colspan="8">
                        <b>- ค่าดำเนินการ</b>                               
                    </td>
                    <td  align="right">
                        <div class="col-md-10">
                            <input type="text"  class="form-control col-md-9 col-xs-12" value="15" id="operationPercent"> 
                        </div>
                        <div><b>%</b></div>                              
                    </td>                    
                    <td>
                        <label id="operationValue">0.00</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="8" class="text-right alert alert-success">
                        <b>รวมค่าแรง</b>                               
                    </td>
                    <td class="text-right" colspan="2" ><b><span id="labor_grandtotal">0.00</apan></b></td>
                </tr>                
            </tbody> 
            
        </table>
    </div>
    <h2>รวมค่าแรง</h2>
    <div class="row formbox box2">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td class="text-right alert alert-success">รวมเป็นเงินค่า Material + ค่าแรง</td>
                    <td class="text-right alert alert-success"> <span id="labor_grandtotal_plus_mat_no_cost">0.00</apan></td>
                    <td class="text-right alert alert-success"> <span id="labor_grandtotal_plus_mat">0.00</apan></td>                    
                </tr>
            </tbody>
        </table>
    </div>

    <div class="row formbox box1">
        <div class="col-md-6 col-sm-12 col-xs-12" style="padding:10px">

            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>หมายเหตุ</td>
                    </tr>

                    <tr>
                        <td><textarea rows="10" id="message" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"></textarea></td>

                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12" style="padding:10px">
            <table class="table table-bordered">
                <tr>
                    <td></td>
                    <td>หาร 0.80</td>
                    <td>หาร 0.75</td>
                    <td>หาร 0.70</td>
                    <td>หาร 0.65</td>
                    <td>หาร 0.60</td>
                    <td>หาร 0.55</td>
                    <td class="col-md-2 form-inline">     
                        <div>
                            <label for="inputPercentage"> หาร  </label>&nbsp;&nbsp;<input type="text" id="inputPercentage" onchange="javascript:predictionCalculate();" class="form-control input input-sm" style="width:70%">
                        </div>
                    </td>                    
                </tr>

                <tbody>
                    <tr>
                        <td align="center"></td>
                        <td align="center"><label id="divineBy080">0.00</label>  </td>
                        <td align="center"><label id="divineBy075">0.00</label>   </td>
                        <td align="center"><label id="divineBy070">0.00</label>   </td>
                        <td align="center"><label id="divineBy065">0.00</label>   </td>
                        <td align="center"><label id="divineBy060">0.00</label>   </td>
                        <td align="center"><label id="divineBy055">0.00</label>   </td>
                        <td align="center"><label id="divineByInput">0.00</label>   </td>
                    </tr>
                </tbody>
                <tr>
                    <td>PF</td>
                    <td>20%</td>
                    <td>25%</td>
                    <td>30%</td>
                    <td>35%</td>
                    <td>40%</td>
                    <td>45%</td>
                    <td class="col-md-2 form-inline"><input type="text" id="inputAmtPercentage" onchange="javascript:predictionCalculate();" class="form-control" style="width:80%" > <span> % </span></td>                    
                </tr>

                <tbody>
                    <tr>
                        <td>AMT</td>
                        <td><label id="amt020">0.00</label> </td>
                        <td><label id="amt025">0.00</label> </td>
                        <td><label id="amt030">0.00</label> </td>
                        <td><label id="amt035">0.00</label> </td>
                        <td><label id="amt040">0.00</label> </td>
                        <td><label id="amt045">0.00</label></td>
                        <td><label id="amtByInput">0.00</label> </td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td>QUO</td>
                        <td><label id="quo020">0.00</label> </td>
                        <td><label id="quo025">0.00</label> </td>
                        <td><label id="quo030">0.00</label> </td>
                        <td><label id="quo035">0.00</label> </td>
                        <td><label id="quo040">0.00</label> </td>
                        <td><label id="quo045">0.00</label></td>
                        <td><label id="quoByInput">0.00</label> </td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td>เท่า</td>
                        <td><label id="multipleTime020">0.00</label> </td>
                        <td><label id="multipleTime025">0.00</label> </td>
                        <td><label id="multipleTime030">0.00</label> </td>
                        <td><label id="multipleTime035">0.00</label> </td>
                        <td><label id="multipleTime040">0.00</label> </td>
                        <td><label id="multipleTime045">0.00</label></td>
                        <td><label id="multipleTimeByInput">0.00</label> </td>
                    </tr>
                </tbody>
            </table>


            <table class="table table-bordered">
                <tr>
                    <td class="col-md-1">ราคาเสนอ</td>
                    <td align="center" class="col-md-3"><input type="text" class="form-control col-md-9 col-xs-12" id="priceOffer" onchange="javascript:calculatePriceOffer()" value="1.0"></td>
                    <td align="center" class="col-md-1">ชุด</td>
                    <td class="col-md-3">ราคาเสนอต่อหน่วย</td>
                    <td class="col-md-3"><span id="priceOfferPerUnit"></span></td>
                    <td align="center" class="col-md-1">ชุด</td>
                </tr>
                <tr>
                    <td>ค่า Mat.</td>
                    <td><span id="priceOfferMaterial"></span></td>
                    <td align="center">ชุด</td>
                    <td>ค่าแรง</td>
                    <td><span id="priceOfferWage"></span></td>
                    <td align="center">ชุด</td>
                </tr>
            </table>



        </div>
        <table class="table table-bordered">
            <tr>
                <td align="center">
                    <div class="form-group">
                        <div class="col-md-12 " style="margin-top:10px">

                            <button type="button" class="btn btn-primary cancelbt" onclick="$('input, select, div').each(function () {
                                                        $(this).removeAttr('required');
                                                    });
                                                    location.href = 'customertypelist';">ยกเลิก</button>   
                            <button type="submit" class="btn btn-labeled btn-success">ตกลง</button>

                        </div>
                    </div>                        


                </td>
            </tr>

        </table>
    </div>
</div>






<script src="/assets/js/directLaborScript.js"></script>


