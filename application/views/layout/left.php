<style>
    .box1 {
        background: #caf5de
    }

    .box2 {
        background: #ffe8cc
    }

    .box3 {
        background: #d7eaf8
    }

    .box4 {
        background: #f9d7f4
    }

    .formbox {
        border: 2px solid #ccc;
        margin-bottom: 40px;
    }

    .formbox .x_title {
        border-bottom: 2px solid #ccc;
    }

    .formbox .x_panel {
        padding-bottom: 35px;
        margin-bottom: 0;
        background: transparent !important;
    }

    .formbox>.col-md-12 {
        padding: 0 0 0 0;
    }

    .sidebar-footer {
        background: #1A1A1A;
    }

    .main_container {
        background: #1A1A1A;
    }

    .left_col {
        background: #1A1A1A;
    }

    .nav_title {
        background: #1A1A1A;
    }

    .btn-success {
        background: #2B8846
    }

    .top_nav .nav_menu {
        background-image: url(/assets/images/bg_top.jpg);
        height: 95px;
    }

    .top_nav i {
        color: #fff;
    }

    .stepnum {
        color: #fff !important;
        background: #EF5A2B;
        padding: 10px 0 0 0;
        display: block;
        border-radius: 50%;
        width: 40px;
        float: left;
        text-align: center;
        height: 40px;
    }

    .titleform {
        color: #000 !important;
        padding: 10px 0 0 20px;
        display: block;
        border-radius: 50%;
        float: left;
        text-align: center;
        height: 40px;
    }

    .cancelbt {
        background: #ccc;
        border: 1px solid #ccc;
    }

    .btn-success {
        width: 250px;
    }

</style>

<div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
        <a href="index.html" class="site_title"><img src="<?php echo base_url(); ?>/assets/images/watalogo-png.png" width="100%"></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
        <div class="profile_pic">
            <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
            <span>Welcome,</span>
            <h2>John Doe</h2>
        </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">

            <div class="menu_section">
                 <h3>MENU</h3>   
                 <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i> ใบรายการคิดต้นทุน <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/calculation/form/directlaborlist">ใบรายการคิดต้นทุน</a></li>
                            <li><a href="/calculation/form/directlabor">ฟอร์มคิดต้นทุน</a></li>
                        </ul>
                    </li>


                    <li><a><i class="fa fa-edit"></i> บริษัท <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/settings/customer/customertlist">บริษัท</a></li>
                            <li><a href="/settings/customer/customertypelist">ประเภทบริษัท/งาน</a></li>
                            <li><a href="/settings/customer/custfirstlist">ตัวอักษรตัวแรก</a></li>
                            <li><a href="/settings/customer/projectlist">โครงการ</a></li>
                        </ul>
                    </li>


                    <li><a><i class="fa fa-edit"></i> สินค้า / ค่าแรง <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/settings/product/productdelist">สินค้า</a></li>
                            <li><a href="/settings/product/mainproductlist">ประเภทสินค้าหลัก</a></li>
                            <li><a href="/settings/product/sproductlist">ประเภทสินค้าย่อย</a></li>
                            <li><a href="/settings/product/finishinglist">Finishing</a></li>
                            <li><a href="/settings/product/colorplist">ค่าวัตถุดิบทำสี</a></li>
                            <li><a href="/settings/product/laborclist">ค่าแรง</a></li>
                            <li><a href="/settings/product/laborctypelist">ประเภทค่าแรง</a></li>
                        </ul>
                    </li>
                   

                    <li><a><i class="fa fa-edit"></i> วัตถุดิบ/วัสดุสิ้นเปลือง <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/settings/material/ingrelist">วัตถุดิบ</a></li>
                            <li><a href="/settings/material/ingrewastelist">วัสดุสิ้นเปลือง</a></li>
                            <li><a href="/settings/material/ingretypelist">ประเภทวัตถุดิบ/โลหะ/วัสดุสิ้นเปลือง</a></li>
                            <li><a href="/settings/material/ironshapelist">รูปร่างโลหะ / ลักษณะวัตถุดิบ / แผนกวัสดุสิ้นเปลือง</a></li>
                            <li><a href="/settings/material/unitlist">หน่วย</a></li>
                            <li><a href="/settings/material/extrashapelist">ลักษณะพิเศษของโลหะ / ชื่อเฉพาะวัตถุดิบ / วัสดุสิ้นเปลือง</a></li>
                            <li><a href="/settings/material/extrawastelist">ลักษณะพิเศษวัสดุสิ้นเปลือง</a></li>
                            <li><a href="/settings/material/costtypelist">ประเภทต้นทุน</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-edit"></i> ขาย <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/settings/customer/priceflist">ใบเสนอราคา</a></li>
                            <li><a href="/settings/customer/sellplist">ใบสั่งขาย</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-edit"></i> ต้นทุนย้อนกลับ <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/settings/customer/huturnlist">รายการต้นทุนย้อนกลับ</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-edit"></i> จัดการสิทธิ์ <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/settings/user/userrlist">จัดการสิทธิ์</a></li>
                            <li><a href="/settings/user/usernlist">ผู้ใช้</a></li>
                        </ul>
                    </li>

                </ul>

                <br><br><br>
                <h3>Expand menu</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-bug"></i> ข้อมูลบริษัท <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/settings/customer/custedit">ข้อมูลบริษัท</a></li>
                            <li><a href="/settings/customer/custtype">ประเภทบริษัท</a></li>
                            <li><a href="/settings/customer/customers">ฟอร์มบริษัท</a></li>
                            <li><a href="/settings/customer/customertlist">รายการบริษัท</a></li>
                            <li><a href="/settings/customer/customertype">ฟอร์มประเภทบริษัท/งาน</a></li>
                            <li><a href="/settings/customer/customertypelist">รายการประเภทบริษัท/งาน</a></li>
                            <li><a href="/settings/customer/custfirst">ฟอร์มตัวอักษรตัวแรก</a></li>
                            <li><a href="/settings/customer/custfirstlist">รายการตัวอักษรตัวแรก</a></li>
                            <li><a href="/settings/customer/project">ฟอร์มโครงการ</a></li>
                            <li><a href="/settings/customer/projectlist">รายการโครงการ</a></li>
                            <li><a href="/settings/customer/ingre">ฟอร์มวัตถุดิบ</a></li>
                            <li><a href="/settings/customer/ingrelist">รายการวัตถุดิบ</a></li>
                            <li><a href="/settings/customer/ingrewaste">ฟอร์มวัสดุสิ้นเปลือง</a></li>
                            <li><a href="/settings/customer/ingrewastelist">รายการวัสดุสิ้นเปลือง</a></li>
                            <li><a href="/settings/customer/ingretype">ฟอร์มประเภทวัสดุสิ้นเปลือง</a></li>
                            <li><a href="/settings/customer/ingretypelist">รายการประเภทวัสดุสิ้นเปลือง</a></li>
                            <li><a href="/settings/customer/ironshape">ฟอร์มรูปร่างโลหะ</a></li>
                            <li><a href="/settings/customer/ironshapelist">รายการรูปร่างโลหะ</a></li>
                            <li><a href="/settings/customer/extrashape">ฟอร์มลักษณะพิเศษของโลหะ</a></li>
                            <li><a href="/settings/customer/extrashapelist">รายการลักษณะพิเศษของโลหะ</a></li>
                            <li><a href="/settings/customer/extrawaste">ฟอร์มลักษณะพิเศษวัสดุสิ้นเปลือง</a></li>
                            <li><a href="/settings/customer/extrawastelist">รายการลักษณะพิเศษวัสดุสิ้นเปลือง</a></li>
                            <li><a href="/settings/customer/costtype">ฟอร์มประเภทต้นทุน</a></li>
                            <li><a href="/settings/customer/costtypelist">รายการประเภทต้นทุน</a></li>
                            <li><a href="/settings/customer/unit">ฟอร์มหน่วย</a></li>
                            <li><a href="/settings/customer/unitlist">รายการหน่วย</a></li>
                            <li><a href="/settings/customer/productde">ฟอร์มสินค้า</a></li>
                            <li><a href="/settings/customer/productdelist">รายการสินค้า</a></li>
                            <li><a href="/settings/customer/mainproduct">ฟอร์มประเภทสินค้าหลัก</a></li>
                            <li><a href="/settings/customer/mainproductlist">รายการประเภทสินค้าหลัก</a></li>
                            <li><a href="/settings/customer/sproduct">ฟอร์มประเภทสินค้าย่อย</a></li>
                            <li><a href="/settings/customer/sproductlist">รายการประเภทสินค้าย่อย</a></li>
                            <li><a href="/settings/customer/finishing">ฟอร์มfinishing</a></li>
                            <li><a href="/settings/customer/finishinglist">รายการfinishing</a></li>
                            <li><a href="/settings/customer/laborc">ฟอร์มค่าแรง</a></li>
                            <li><a href="/settings/customer/laborclist">รายการค่าแรง</a></li>
                            <li><a href="/settings/customer/laborctype">ฟอร์มประเภทค่าแรง</a></li>
                            <li><a href="/settings/customer/laborctypelist">รายการประเภทค่าแรง</a></li>
                            <li><a href="/settings/customer/colorp">ฟอร์มวัตถุดิบทำสี</a></li>
                            <li><a href="/settings/customer/colorplist">รายการวัตถุดิบทำสี</a></li>
                            <li><a href="/settings/customer/directlabor">ฟอร์มค่าแรงแบบ Direct Labor</a></li>
                            <li><a href="/settings/customer/directlaborlist">รายการค่าแรงแบบ Direct Labor</a></li>
                            <li><a href="/settings/customer/paytype">ฟอร์มประเภทค่าแรงแบบแจกแจง</a></li>
                            <li><a href="/settings/customer/paytypelist">รายการประเภทค่าแรงแบบแจกแจง</a></li>
                            <li><a href="/settings/customer/setdirect">ฟอร์มสินค้าชุด แบบค่าแรงแบบ Direct Labor</a></li>
                            <li><a href="/settings/customer/setdirectlist">รายการสินค้าชุด แบบค่าแรงแบบ Direct Labor</a></li>
                            <li><a href="/settings/customer/settype">ฟอร์มสินค้าชุด แบบแจกแจงค่าแรง</a></li>
                            <li><a href="/settings/customer/settypelist">รายการสินค้าชุด แบบแจกแจงค่าแรง</a></li>
                            <li><a href="/settings/customer/pricef">ฟอร์มใบเสนอราคา</a></li>
                            <li><a href="/settings/customer/priceflist">รายการใบเสนอราคา</a></li>
                            <li><a href="/settings/customer/sellp">ฟอร์มใบสั่งขาย</a></li>
                            <li><a href="/settings/customer/sellplist">รายการใบสั่งขาย</a></li>
                            <li><a href="/settings/customer/huturn">ฟอร์มต้นทุนย้อนกลับ</a></li>
                            <li><a href="/settings/customer/huturnlist">รายการต้นทุนย้อนกลับ</a></li>
                            <li><a href="/settings/customer/userr">ฟอร์มจัดการสิทธิ์</a></li>
                            <li><a href="/settings/customer/userrlist">รายการจัดการสิทธิ์</a></li>
                            <li><a href="/settings/customer/usern">ฟอร์มผู้ใช้</a></li>
                            <li><a href="/settings/customer/usernlist">รายการผู้ใช้</a></li>
                            <li><a href="/settings/customer/combobox">ฟอร์มชุดสินค้า</a></li>
                            <li><a href="profile.html">Profile</a></li>




                        </ul>
                    </li>





                </ul>
            </div>





            <h3>General</h3>
            <ul class="nav side-menu">
                <li><a><i class="fa fa-home"></i> รายงาน <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="index.html">รายงาน 1</a></li>
                        <li><a href="index2.html">รายงาน 2</a></li>
                        <li><a href="index3.html">รายงาน 3</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-home"></i>การตั้งค่า <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="/settings/customer">บริษัท</a></li>
                        <li><a href="/settings/contact">ผู้ติดต่อ</a></li>
                        <li><a href="index2.html">ประเภทบริษัท/งาน</a></li>
                        <li><a href="index3.html">ตัวอักษรตัวแรก</a></li>
                        <li><a href="index3.html">โครงการ</a></li>
                        <li><a href="index3.html">สินค้า</a></li>
                        <li><a href="index3.html">ประเภทวัตถุดิบ</a></li>
                        <li><a href="index3.html">ประเภทสินค้าหลัก</a></li>
                        <li><a href="index3.html">ประเภทสินค้าย่อย</a></li>
                        <li><a href="index3.html">Finishing</a></li>
                        <li><a href="index3.html">วัตถุดิบ</a></li>
                        <li><a href="index3.html">วัสดุสิ้นเปลือง</a></li>
                        <li><a href="index3.html">รูปร่างโลหะ</a></li>
                        <li><a href="index3.html">ลักษณะพิเศษของโลหะ</a></li>
                        <li><a href="index3.html">หน่วย</a></li>
                        <li><a href="index3.html">ค่าวัตถุดิบทำสี</a></li>
                        <li><a href="index3.html">ค่าแรง</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-edit"></i> ฟอร์มการคำนวณ <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="/form/cost">คิดราคาต้นทุน</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-desktop"></i> UI Elements <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="general_elements.html">General Elements</a></li>
                        <li><a href="media_gallery.html">Media Gallery</a></li>
                        <li><a href="typography.html">Typography</a></li>
                        <li><a href="icons.html">Icons</a></li>
                        <li><a href="glyphicons.html">Glyphicons</a></li>
                        <li><a href="widgets.html">Widgets</a></li>
                        <li><a href="invoice.html">Invoice</a></li>
                        <li><a href="inbox.html">Inbox</a></li>
                        <li><a href="calendar.html">Calendar</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="tables.html">Tables</a></li>
                        <li><a href="tables_dynamic.html">Table Dynamic</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="chartjs.html">Chart JS</a></li>
                        <li><a href="chartjs2.html">Chart JS2</a></li>
                        <li><a href="morisjs.html">Moris JS</a></li>
                        <li><a href="echarts.html">ECharts</a></li>
                        <li><a href="other_charts.html">Other Charts</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="fixed_sidebar.html">Fixed Sidebar</a></li>
                        <li><a href="fixed_footer.html">Fixed Footer</a></li>
                    </ul>
                </li>
            </ul>
        </div>






        <div class="menu_section">
            <h3>Live On</h3>
            <ul class="nav side-menu">
                <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="e_commerce.html">E-commerce</a></li>
                        <li><a href="projects.html">Projects</a></li>
                        <li><a href="project_detail.html">Project Detail</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="profile.html">Profile</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="page_403.html">403 Error</a></li>
                        <li><a href="page_404.html">404 Error</a></li>
                        <li><a href="page_500.html">500 Error</a></li>
                        <li><a href="plain_page.html">Plain Page</a></li>
                        <li><a href="login.html">Login Page</a></li>
                        <li><a href="pricing_tables.html">Pricing Tables</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="#level1_1">Level One</a>
                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li class="sub_menu"><a href="level2.html">Level Two</a>
                                </li>
                                <li><a href="#level2_1">Level Two</a>
                                </li>
                                <li><a href="#level2_2">Level Two</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
            </ul>
        </div>

    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
        <a data-toggle="tooltip" data-placement="top" title="Settings">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Lock">
            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
    </div>
    <!-- /menu footer buttons -->
</div>
