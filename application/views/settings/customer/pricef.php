<style>
    h3 {
        color: #000;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มใบเสนอราคา</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row formbox box4">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">ใบเสนอราคา</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">เลขที่ใบเสนอราคา
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">วันที่เสนอราคา
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">REV.
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">บริษัท
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">เรียน
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">โครงการ
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">เบอร์โทร
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fax
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">คำเสนอ
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row formbox box1">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ลำดับ</th>
                    <th>ชื่อสินค้า</th>
                    <th>Dwg.No.</th>
                    <th>คำอธิบาย</th>
                    <th>หนายเหตุ</th>
                    <th>จำนวน</th>
                    <th>ราคา Mat@</th>
                    <th>ราคาค่าแรง@</th>
                    <th>รวมราคา@</th>
                    <th>รวมเป็นเงิน</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                    </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                    </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                    </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                    </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="row formbox box1">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>รวมเป็นเงินสุทธิ</td>
                    <td> </td>
                    <td> </td>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>ภาษีมูลค่าเพิ่ม
                        <div>
                            <input type="radio" class="flat" name="gen" id="genderM" value="M" checked="" required="" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                            : มี VAT
                            <input type="radio" class="flat" name="gen" id="genderF" value="F" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                            : ไม่มี VAT
                        </div>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td>รวมเป็นเงินทั้งสิ้น</td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>
        </table>
    </div>


    <div class="row formbox box4">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td>หมายเหตุรวม</td>
                    <td>เงื่อนไขการชำระเงิน</td>

                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td>
                        <textarea id="message" required="required" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"></textarea></td>
                    <td>
                        <textarea id="message" required="required" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"></textarea>
                    </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ผู้เสนอ</label>
                            <div class="col-md-6 col-sm-6 ">
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </div>
                        </div>
                    </td>

                    <td>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ผู้อนุมัติ</label>
                        </div>
                        <div>
                            <input type="radio" class="flat" name="gender" id="genderM" value="M" checked="" required="" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                            : อนุมัติ
                            <input type="radio" class="flat" name="gender" id="genderF" value="F" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                            : ไม่อนุมัติ
                        </div>
                    </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ตำแหน่ง
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </div>
                        </div>
                    </td>
                    <td> </td>
                </tr>
            </tbody>
        </table>
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button class="btn btn-primary cancelbt" type="button">ยกเลิก</button>
            <button type="button" class="btn btn-labeled btn-success">ตกลง</button>
        </div>
    </div>
</div>
