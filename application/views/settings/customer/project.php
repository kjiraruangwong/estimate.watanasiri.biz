<style>
    h3 {
        color: #000;
    }

    h5 {
        color: red;
        text-align: center;
    }

    .btnadd {
        background: #1d6a96;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnadd:hover {
        background: #85b8cb;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มโครงการ</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <?php if (!empty($_GET['err'])) { ?>
            <div class="alert alert-danger text-center" role="alert">                
                <b>รหัส : <?php echo $_GET['code'] ?> มีอยู่แล้ว กรุณากรอกรหัสใหม่! ( <?php echo $_GET['message'] ?>  )</b>
            </div>
        <?php }?>        
        
        <div class="row formbox box1">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">โครงการ</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />

                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">รหัสโครงการ
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="code_input" readonly class="form-control col-md-7 col-xs-12" value="<?php if (!empty($record)) echo $record->code; ?>">
                                </div>
                            </div>
                            <h5>กรุณาเลือกปี ค.ศ. ชื่อย่อลูกค้า และชื่อย่อโครงการเพื่อสร้างรหัสโครงการแบบอัตโนมัติ</h5>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ปี ค.ศ.
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" id="ADYear_input" <?php if (!empty($mode) && $mode == 'view') echo "disabled" ?> required>
                                        <option value="">-- กรุณาเลือก --</option>
                                        <?php
                                        $year = date("Y");
                                        $year = $year + 2;
                                        for ($x = 0; $x <= 50; $x++) {
                                            if (!empty($record) && ($record->ADYear == $year)) {
                                                echo "<option value='" . $year . "' selected>" . $year . "</option>";
                                            } else {
                                                echo "<option value='" . $year . "'>" . $year . "</option>";
                                            }
                                            $year--;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อย่อลูกค้า
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" id="customerId_input" onclick="customerIdChange()" <?php if (!empty($mode) && $mode == 'view') echo "disabled" ?> required>
                                        <option value="">-- กรุณาเลือก --</option>
                                        <?php
                                        foreach ($customers as $customer) {
                                            if (!empty($record) && ($record->customerId == $customer->id)) {
                                                echo "<option value='" . $customer->id . ";" . $customer->name . "' selected>" . $customer->code . "</option>";
                                            } else {
                                                echo "<option value='" . $customer->id . ";" . $customer->name . "'>" . $customer->code . "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อลูกค้า
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="customerName" readonly required="required" class="form-control col-md-7 col-xs-12" value="<?php if (!empty($customer_record)) echo $customer_record->name; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อย่อโครงการ
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="shortName_input" required="required" class="form-control col-md-7 col-xs-12"  value="<?php if (!empty($record)) echo $record->shortName; ?>" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                </div>

                                <?php if (empty($mode) || $mode == 'edit') { ?>
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style="margin-top:10px">
                                        <a class="btn btnadd" onclick="generateProjectCode()">เพิ่มรหัสโครงการ</a>
                                    </div>
                                <?php } ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row formbox box2">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">

                        <form id="target-form" data-parsley-validate class="form-horizontal form-label-left" action="/settings/customer/project" method="post">
                            <?php if (!empty($mode) && $mode == 'edit') { ?>
                                <input type="hidden" name="selected-id" value="<?php if (!empty($record)) echo $record->id; ?>">
<?php } ?>
                            <input type="hidden" name="form-submitted" value="1">
                            <input type="hidden" name="code" id="code" value="1">
                            <input type="hidden" name="shortName" id="shortName" value="1">
                            <input type="hidden" name="ADYear" id="ADYear" value="1">  
                            <input type="hidden" name="customerId" id="customerId" value="1"> 
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อโครงการ
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="name" name="name" required="required" class="form-control col-md-7 col-xs-12" value="<?php if (!empty($record)) echo $record->name; ?>" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="first-name">วันที่เริ่มโครงการ
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <div class="input-prepend input-group">
                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" id="single_cal1" name="dateStart" placeholder="วันที่เริ่มโครงการ" aria-describedby="inputSuccess2Status"  value="<?php if (!empty($record)) echo $record->dateStart; ?>" <?php if (!empty($mode) && $mode == 'view') echo "disabled" ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="first-name">วันที่จบโครงการ
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <div class="input-prepend input-group">
                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" id="single_cal2" name="dateEnd" placeholder="วันที่จบโครงการ" aria-describedby="inputSuccess2Status"  value="<?php if (!empty($record)) echo $record->dateEnd; ?>" <?php if (!empty($mode) && $mode == 'view') echo "disabled" ?>>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style="margin-top:10px">
                                    <?php if (!empty($mode) && $mode == 'view') { ?>
                                        <button type="button" class="btn btn-labeled btn-success" onclick="$('input, select, div').each(function() { $(this).removeAttr('required');});location.href = 'projectlist';">ตกลง</button>
<?php } else { ?>
                                        <button type="button" class="btn btn-primary cancelbt" onclick="$('input, select, div').each(function() { $(this).removeAttr('required');});location.href = 'projectlist';">ยกเลิก</button>   
                                        <button type="submit" class="btn btn-labeled btn-success" onclick="submitform()">ตกลง</button>
<?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function customerIdChange() {
        var customerInfo = $("#customerId_input").val();
        var res = customerInfo.split(";");
        var customerName = res[1];
        $("#customerName").val(customerName);
    }

    function generateProjectCode() {
        if (($("#customerName").val() != "") && ($("#shortName_input").val() != "")) {
            $("#code_input").val("CODE-GENERATION");
        }
    }
    //Submit Data
    function submitform() {
        event.preventDefault();
        var customerInfo = $("#customerId_input").val();
        var res = customerInfo.split(";");
        var customerId = res[0];

        $("#code").val($("#code_input").val());
        $("#shortName").val($("#shortName_input").val());
        $("#ADYear").val($("#ADYear_input").val());
        $("#customerId").val(customerId);
        $("#target-form").submit();
    }
</script>