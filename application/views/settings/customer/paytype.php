<style>
    h3 {
        color: #000;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มประเภทค่าแรงแบบแจกแจง</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row formbox box4">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">ใบคิดรายการต้นทุน</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">บริษัท
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">โครงการ
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">เสนอ
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">โทร
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fax
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อสินค้า
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Dwg.No.
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ประเภทงาน
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">วัตถุดิบ
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Finish
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">วันที่เสนอ
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Rev.
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">แผ่นที่
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">พื้นที่รวม
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                                <label class="control-label">ตรม.</label>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ราคา/ม2.
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                                <label class="control-label">บาท</label>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ต้นทุนคิด
                                </label>
                                <div class="col-md-3 col-sm-3">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">จำนวนที่เสนอ
                                </label>
                                <div class="col-md-3 col-sm-3">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">%Direct Labor
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">น้ำหนักรวม
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                                <label class="control-label">กก.</label>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ราคา/กก.
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                                <label class="control-label">บาท</label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row formbox box1">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ลำดับ</th>
                    <th>ชื่อวัตถุดิบ</th>
                    <th>ทำสี</th>
                    <th>F</th>
                    <th>ขนาดตัด</th>
                    <th>จำนวนชิ้นที่ตัดได้</th>
                    <th>จำนวน</th>
                    <th>ราคา@</th>
                    <th>รวมเป็นเงิน</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="row formbox box1">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>ค่าทำสี</td>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td> </td>
                    <td>ตรม.</td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td> </td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td> </td>
                    <td>ตรม.</td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="row formbox box1">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>วัตถุสิ้นเปลือง/Packing</td>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td> </td>
                    <td>วัตถุสิ้นเปลือง</td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td> </td>
                    <td>อุปกรณ์ / Packing</td>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="row formbox box2">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>รวมค่า Material</td>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>
                        <a>ค่าใช้จ่าย</a>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12"><a>%</a>
                    </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td>รวมเป็นเงิน Material</td>
                    <td> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="row formbox box4">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><span class="titleform">ค่าแรง</span></h2>
                    <div class="clearfix"></div>

                    <div>
                        <input type="radio" class="flat" name="gender" id="genderM" value="M" checked="" required="" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                        : Direct Labor
                        <input type="radio" class="flat" name="gender" id="genderF" value="F" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                        : แจกแจงค่าแรง
                    </div>

                </div>
            </div>
        </div>
    </div>
    
    <div class="row formbox box3">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ค่าแรง</th>
                    <th>จำนวนคน</th>
                    <th>จำนวนครั้ง</th>
                    <th>เวลาที่ใช้ @</th>
                    <th>รวมเวลา</th>
                    <th>L/M</th>
                    <th>รวมเป็นเงิน</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td>
                        <select class="form-control">
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                            <option> </option>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td>
                        <input type="text" id="first-name" required="required" class="form-control col-md-9 col-xs-12">
                    </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="row formbox box2">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>รวมค่าแรง</td>
                    <td> </td>
                    <td> </td>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>
                        <a>รวมค่าแรง</a>
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12"><a>%</a>
                    </td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <td>รวมเป็นเงินค่าMaterial+ค่าแรง</td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="row formbox box1">
        <div class="col-md-6 col-sm-12 col-xs-12">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>หมายเหตุ</td>

                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td><textarea id="message" required="required" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"></textarea></td>

                    </tr>
                </tbody>
            </table>
        </div>
        
        <div class="col-md-6 col-sm-12 col-xs-12">
            <table class="table table-bordered">
                <tr>
                    <td>หาร 0.80</td>
                    <td>หาร 0.75</td>
                    <td>หาร 0.70</td>
                    <td>หาร 0.65</td>
                    <td>หาร 0.60</td>
                    <td>หาร 0.55</td>
                </tr>

                <tbody>
                    <tr>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <table class="table table-bordered">
                <tr>
                    <td>PF</td>
                    <td>20%</td>
                    <td>25%</td>
                    <td>30%</td>
                    <td>35%</td>
                    <td>40%</td>
                    <td>45%</td>
                </tr>

                <tbody>
                    <tr>
                        <td>AMT</td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td>QUO</td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td>เท่า</td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </tbody>
            </table>
            
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table class="table table-bordered">
                    <tr>
                        <td>ราคาเสนอ</td>
                        <td> </td>
                        <td>ราคาเสนอต่อหน่วย</td>
                        <td> </td>
                    </tr>

                    <tbody>
                        <tr>
                            <td>ค่า Mat.</td>
                            <td> </td>
                            <td>ค่าแรง</td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
