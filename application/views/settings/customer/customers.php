<style>
    h3{color:#000;
    }
    h5{color:red;
       text-align: center;
    }

    .btnadd{background: #1d6a96;
            border-radius: 3px;
            border: 1px solid #1d6a96;
            color: #fff;
    }

    .btnadd:hover{
        background: #85b8cb;
    }
    .top_nav .navbar-right li a{color:#fff!important;}
    .form-horizontal .control-label {color:#000;}
</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มบริษัท</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <?php if (!empty($_GET['err'])) { ?>
            <div class="alert alert-danger text-center" role="alert">                
                <b>รหัส : <?php echo $_GET['code'] ?> มีอยู่แล้ว กรุณากรอกรหัสใหม่! ( <?php echo $_GET['message'] ?>  )</b>
            </div>
        <?php }?>
        
        <div class="row formbox box1">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">บริษัท</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />

                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/settings/customer/customers" method="post">
                            <input type="hidden" name="form-submitted" value="1">
                            <input type="hidden" name="name" id="name" value="1">
                            <input type="hidden" name="short_name" id="short_name" value="1">
                            <input type="hidden" name="code" id="code" value="1">
                            <?php if (!empty($mode) && $mode == 'edit') { ?>
                                <input type="hidden" name="selected-id" value="<?php if (isset($customer)) echo $customer[0]->customerId; ?>">
                            <?php } ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >รหัสบริษัท <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="customer_code" name="code" disabled="disabled" class="form-control col-md-7 col-xs-12" value="<?php
                                    if (isset($customer)) {
                                        echo $customer[0]->code;
                                    }
                                    ?>">
                                </div>
                            </div>
                            <h5>กรุณาเลือกประเภทบริษัท และตัวอักษรตัวแรกของชื่อบริษัท เพื่อสร้างรหัสบริษัทแบบอัตโนมัติ</h5>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >ประเภทบริษัท
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select id="customer_type" name="customer_type" class="form-control" <?php if (!empty($mode) && $mode == 'view') echo "disabled" ?> required>
                                        <option value="">-- กรุณาเลือก --</option>
                                        <?php
                                        foreach ($records as $record) {
                                            ?>
                                            <option <?php if (isset($customer) && $customer[0]->customerTypeId == $record->id) { ?>selected="selected"<?php } ?>value="<?php echo $record->id; ?>"><?php echo $record->code; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >ตัวอักษรตัวแรก <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select id="first_alphabet" name="first_alphabet" class="form-control" <?php if (!empty($mode) && $mode == 'view') echo "disabled" ?> required>
                                        <option value="">-- กรุณาเลือก --</option>
                                        <?php
                                        foreach ($alphabet_records as $alphabet_record) {
                                            ?>
                                            <option <?php if (isset($customer) && $customer[0]->customerFirstCharId == $alphabet_record->id) { ?>selected="selected"<?php } ?>value="<?php echo $alphabet_record->id; ?>"><?php echo $alphabet_record->code; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <h5>(ตัวอักษรภาษาอังกฤษตัวแรกของชื่อบริษัท)</h5>
                            <?php if (empty($mode) || $mode != 'view') { ?>
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="button" id="generate-company-code" onclick="autoGenerate();" class="btn btn-labeled btn-success">เพิ่มรหัสบริษัท</button>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div> 
        </div>


        <div class="row formbox box2">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">

                        <br />

                        <form id="demo-form3" data-parsley-validate class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >ชื่อบริษัท
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  id="name_input" name="name_input" class="form-control col-md-7 col-xs-12" value="<?php
                                    if (isset($customer)) {
                                        echo $customer[0]->name;
                                    }
                                    ?>" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >ชื่อย่อบริษัท <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="short_name_input" name="short_name_input" class="form-control col-md-7 col-xs-12" value="<?php
                                    if (isset($customer)) {
                                        echo $customer[0]->shortName;
                                    }
                                    ?>" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                </div>
                            </div>
                            <h5>(ตัวอักษรภาษาอังกฤษ 3 ตัวอักษร)</h5>
                            <?php if (empty($mode) || $mode != 'view') { ?>
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button class="btn btn-primary cancelbt" type="button">ยกเลิก</button>
                                    <button type="button" class="btn btn-labeled btn-success" onclick="submitform()">ตกลง</button>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style="margin-top:30px;">
                        <a href="/settings/customer/customertlist"><button style="float: right;" class="btn btn-primary" type="button">กลับไปหน้าแรกบริษัท</button></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row formbox box3">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php if (empty($mode) || $mode != 'view') { ?>
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><span class="titleform">เพิ่มผู้ติดต่อ</span></h2>
                            <div class="clearfix"></div>
                            <br />

                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/settings/customer/customers" method="post">
                                <input type="hidden" name="form-contact-submitted" value="1">
                                <input type="hidden" name="customer_id" id="customer_id" value="<?php
                                if (isset($customer)) {
                                    echo $customer[0]->customerId;
                                }
                                ?>">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >ชื่อ
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="contact_firstname" value="" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >นามสกุล
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="contact_lastname" value="" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >ที่อยู่
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="contact_address" value="" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >เบอร์โทร
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="contact_tel" value="" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >เบอร์ Fax
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="contact_fax" value="" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >อีเมล
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="contact_email" value="" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style="margin-top:10px">
                                        <button class="btn btn-primary cancelbt" type="button">ยกเลิก</button>
                                        <button type="submit" class="btn btn-labeled btn-success">ตกลง</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                <?php } ?>

                <!-- Start Contact Person List -->
                <?php
                if (isset($contacts)) {
                    $i = 0;
                    foreach ($contacts as $contact) {
                        $i++;
                        ?>
                        <div class="x_panel">
                            <div class="x_title">
                                <h2><span class="titleform">ผู้ติดต่อคนที่ <?php echo $i; ?></span></h2>
                                <div class="clearfix"></div>
                                <br />

                                <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/settings/customer/customers" method="post">
                                    <input type="hidden" name="form-contact-submitted" value="1">
                                    <input type="hidden" name="customer_id" value="<?php
                                    if (isset($contact)) {
                                        echo $contact->customerId;
                                    }
                                    ?>">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >ชื่อ
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  name="contact_firstname" value="<?php
                                            if (isset($contact)) {
                                                echo $contact->firstName;
                                            }
                                            ?> " class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >นามสกุล
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="contact_lastname" value=" <?php
                                            if (isset($contact)) {
                                                echo $contact->lastName;
                                            }
                                            ?> " class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >ที่อยู่
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="contact_address" value="<?php
                                            if (isset($contact)) {
                                                echo $contact->address;
                                            }
                                            ?> " class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >เบอร์โทร
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  name="contact_tel" value="<?php
                                            if (isset($contact)) {
                                                echo $contact->tel;
                                            }
                                            ?>" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >เบอร์ Fax
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  name="contact_fax" value="<?php
                                            if (isset($contact)) {
                                                echo $contact->fax;
                                            }
                                            ?>" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >อีเมล
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="contact_email" value="<?php
                                            if (isset($contact)) {
                                                echo $contact->email;
                                            }
                                            ?>" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
                <!-- End Contact Person List -->

                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style="margin-top:10px; margin-bottom: 30px;">
                    <a href="/settings/customer/customertlist"><button style="float: right;" class="btn btn-primary" type="button">กลับไปหน้าแรกบริษัท</button></a>
                </div>

            </div>
        </div>


    </div>
</div>
<script>
    //Auto Generate Company Code
    function autoGenerate() {
        var customer_type = $("#customer_type option:selected").text();
        var first_alphabet = $("#first_alphabet option:selected").text();
        var customer_type_val = $("#customer_type").val();
        var first_alphabet_val = $("#first_alphabet").val();
        var date = new Date();
        var current_time = date.getTime();
        if (customer_type_val == '' || first_alphabet_val == '') {
            alert('กรุณาเลือกประเภทบริษัท / กรุณาเลือกตัวอักษรตัวแรก');
        } else {
            $("#customer_code").val(customer_type + first_alphabet + current_time);
        }
    }
    //Submit Data
    function submitform() {
        $("#name").val($("#name_input").val());
        $("#short_name").val($("#short_name_input").val());
        $("#code").val($("#customer_code").val());
        var letters = /^[A-Za-z]+$/;
        var short_name = $("#short_name").val();

        if ($("#customer_code").val() == '') {
            alert('กรุณาเพิ่มรหัสบริษัท');
        } else {
            if ($("#name_input").val() != '' && $("#short_name_input").val() != '') {
                if (short_name.match(letters) && short_name.length == 3) {
                    $("#demo-form2").submit();
                } else {
                    alert("กรุณากรอกชื่อย่อบริษัทเป็นภาษาอังกฤษ 3 ตัวอักษร");
                    return false;
                }
            } else {
                alert("กรุณากรอกชื่อบริษัท / ชื่อย่อบริษัท");
                return false;
            }
        }

    }
</script>