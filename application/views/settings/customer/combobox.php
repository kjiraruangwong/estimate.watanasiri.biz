<style>
    h3 {
        color: #000;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

    .btnadd {
        background: #1d6a96;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnadd:hover {
        background: #85b8cb;
    }

    .btnde {
        background: #900000;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnde:hover {
        background: #fd0000;
    }

    .btnedit {
        background: #a13e97;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnedit:hover {
        background: #d3b7d8;
    }

    .btncancle {
        background: #bfbfbf;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btncancle:hover {
        background: #e6e6e6;
    }

    .btnsuc {
        background: #2B8846;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnsuc:hover {
        background: #26B99A;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnsee {
        background: #fe7773;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnsee:hover {
        background: #ffc2c3;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มชุดสินค้า</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row formbox box4">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">ชุดสินค้า</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">สินค้า
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row formbox box1">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_content">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>รหัส</th>
                                <th>ชื่อย่อ</th>
                                <th>ชื่อเต็ม</th>
                                <th>ประเภท</th>
                                <th>ราคา</th>
                            </tr>
                        </thead>


                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btnsuc" type="button">ตกลง</button>

                <button class="btn btncancle" type="button">ยกเลิก</button>

                <button class="btn btnsee" type="button">ค้นหา</button>

                <button class="btn btnde" type="button">เรียงใหม่</button>

                <button class="btn btnadd" type="button">เพิ่ม</button>

                <button class="btn btnedit" type="button">แก้ไข</button>

            </div>
        </div>
    </div>
</div>
