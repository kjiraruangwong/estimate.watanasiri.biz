<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ตารางสินค้าชุด แบบค่าแรงแบบ Direct Labor</h3>
            </div>

        </div>

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div>
                            <h2>รายละเอียดสินค้าชุด แบบค่าแรงแบบ Direct Labor<span>&nbsp;&nbsp;</span></h2>

                        </div>

                        <div>
                            <a type="button" class="btn btn-sm btn-success" href="/settings/customer/setdirect">เพิ่มสินค้าชุด แบบค่าแรงแบบ Direct Labor</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>บริษัท</th>
                                    <th>โครงการ</th>
                                    <th>สินค้า</th>
                                    <th>วันที่</th>
                                    <th>REV.</th>
                                    <th>รูป</th>
                                    <th>ราคา @</th>
                                    <th>หน่วย</th>
                                    <th>ประเภทค่าแรง</th>
                                    <th>ผู้ใช้</th>
                                    <th>สถานะ</th>
                                    <th></th>
                                </tr>
                            </thead>


                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> รายละเอียด </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> แก้ไข </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ลบ </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> รายละเอียด </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> แก้ไข </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ลบ </a>
                                    </td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

