<style>
    h3 {
        color: #000;
    }

    h5 {
        color: red;
        text-align: center;
    }

    .btnadd {
        background: #1d6a96;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnadd:hover {
        background: #85b8cb;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มต้นทุนย้อนกลับ</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>


        <div class="row formbox box1">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">รายการต้นทุนย้อนกลับ</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />

                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">เลขที่ใบสั่งขาย
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">วันที่
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">บริษัท
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">โครงการ
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">เลขที่สั่งขาย
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="row formbox box2">
            <div class="x_panel">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    ชื่อสินค้า
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">

                    <select class="form-control">
                        <option> </option>
                        <option> </option>
                        <option> </option>
                        <option> </option>
                        <option> </option>
                    </select>

                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    จำนวน
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">

                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">

                </div>
            </div>
        </div>

        <div class="row formbox box3">
            <div class="x_panel">
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">Direct Material
                    </label>
                    <div class="col-md-3 col-sm-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                    <label class="control-label">บาท</label>
                </div>
            </div>

            <div class="col-md-12 col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รายการ</th>
                            <th>จำนวน</th>
                            <th>ราคา@</th>
                            <th>รวมเป็นเงิน</th>
                            <th>วันที่บันทึก</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td> </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">รวมเป็นเงิน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">กำไร/ขาดทุน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">คิดเป็น
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>
            </div>
        </div>



        <div class="row formbox box3">
            <div class="x_panel">
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">Consumption
                    </label>
                    <div class="col-md-3 col-sm-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                    <label class="control-label">บาท</label>
                </div>
            </div>

            <div class="col-md-12 col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รายการ</th>
                            <th>จำนวน</th>
                            <th>ราคา@</th>
                            <th>รวมเป็นเงิน</th>
                            <th>วันที่บันทึก</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td> </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">รวมเป็นเงิน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">กำไร/ขาดทุน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">คิดเป็น
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>
            </div>
        </div>



        <div class="row formbox box3">
            <div class="x_panel">
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">Purchase Part
                    </label>
                    <div class="col-md-3 col-sm-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                    <label class="control-label">บาท</label>
                </div>
            </div>

            <div class="col-md-12 col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รายการ</th>
                            <th>จำนวน</th>
                            <th>ราคา@</th>
                            <th>รวมเป็นเงิน</th>
                            <th>วันที่บันทึก</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td> </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">รวมเป็นเงิน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">กำไร/ขาดทุน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">คิดเป็น
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>
            </div>
        </div>



        <div class="row formbox box3">
            <div class="x_panel">
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">Surface
                    </label>
                    <div class="col-md-3 col-sm-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                    <label class="control-label">บาท</label>
                </div>
            </div>

            <div class="col-md-12 col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รายการ</th>
                            <th>จำนวน</th>
                            <th>ราคา@</th>
                            <th>รวมเป็นเงิน</th>
                            <th>วันที่บันทึก</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td> </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">รวมเป็นเงิน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">กำไร/ขาดทุน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">คิดเป็น
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>
            </div>
        </div>



        <div class="row formbox box3">
            <div class="x_panel">
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">Packaging
                    </label>
                    <div class="col-md-3 col-sm-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                    <label class="control-label">บาท</label>
                </div>
            </div>

            <div class="col-md-12 col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รายการ</th>
                            <th>จำนวน</th>
                            <th>ราคา@</th>
                            <th>รวมเป็นเงิน</th>
                            <th>วันที่บันทึก</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td> </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">รวมเป็นเงิน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">กำไร/ขาดทุน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">คิดเป็น
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>
            </div>
        </div>



        <div class="row formbox box3">
            <div class="x_panel">
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">Direct Labor
                    </label>
                    <div class="col-md-3 col-sm-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                    <label class="control-label">บาท</label>
                </div>
            </div>

            <div class="col-md-12 col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รายการ</th>
                            <th>จำนวน</th>
                            <th>ราคา@</th>
                            <th>รวมเป็นเงิน</th>
                            <th>วันที่บันทึก</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td> </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">รวมเป็นเงิน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">กำไร/ขาดทุน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">คิดเป็น
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>
            </div>
        </div>



        <div class="row formbox box3">
            <div class="x_panel">
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">Sub Contract
                    </label>
                    <div class="col-md-3 col-sm-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                    <label class="control-label">บาท</label>
                </div>
            </div>

            <div class="col-md-12 col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รายการ</th>
                            <th>จำนวน</th>
                            <th>ราคา@</th>
                            <th>รวมเป็นเงิน</th>
                            <th>วันที่บันทึก</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td> </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">รวมเป็นเงิน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">กำไร/ขาดทุน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">คิดเป็น
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>
            </div>
        </div>



        <div class="row formbox box3">
            <div class="x_panel">
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">Transport
                    </label>
                    <div class="col-md-3 col-sm-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                    <label class="control-label">บาท</label>
                </div>
            </div>

            <div class="col-md-12 col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รายการ</th>
                            <th>จำนวน</th>
                            <th>ราคา@</th>
                            <th>รวมเป็นเงิน</th>
                            <th>วันที่บันทึก</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td> </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">รวมเป็นเงิน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">กำไร/ขาดทุน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">คิดเป็น
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>
            </div>
        </div>



        <div class="row formbox box3">
            <div class="x_panel">
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">Others
                    </label>
                    <div class="col-md-3 col-sm-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                    <label class="control-label">บาท</label>
                </div>
            </div>

            <div class="col-md-12 col-sm-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รายการ</th>
                            <th>จำนวน</th>
                            <th>ราคา@</th>
                            <th>รวมเป็นเงิน</th>
                            <th>วันที่บันทึก</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td> </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </td>
                            <td>
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">รวมเป็นเงิน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">กำไร/ขาดทุน
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">คิดเป็น
                        </label>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                        <label class="control-label">บาท</label>
                    </div>
                </div>
            </div>
        </div>



        <div class="row formbox box1">
            <div class="x_panel">
                <div class="col-md-12 col-sm-12">
                    <div class="x_panel">
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">ยอดรวมสินค้าเป็นเงิน
                            </label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                            <label class="control-label">บาท</label>
                        </div>
                    </div>

                    <div class="x_panel">
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">ต้นทุนย้อนกลับเป็นเงิน
                            </label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                            <label class="control-label">บาท</label>
                        </div>
                    </div>

                    <div class="x_panel">
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">กำไร/ขาดทุน
                            </label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                            <label class="control-label">บาท</label>
                        </div>
                    </div>

                    <div class="x_panel">
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-12 col-xs-12" for="first-name">คิดเป็น
                            </label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                            <label class="control-label">บาท</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row formbox box1">
            <div class="x_panel">
                <h2><span class="titleform">ยอดรวมโครงการ</span></h2>
                <div class="col-md-12 col-sm-12">
                    <div class="x_content">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th> </th>
                                    <th>Direct labor</th>
                                    <th>Consumption</th>
                                    <th>Purchase part</th>
                                    <th>Surface</th>
                                    <th>Packaging</th>
                                    <th>Direct Labor</th>
                                    <th>Sub Contract</th>
                                    <th>Transport</th>
                                    <th>Others</th>
                                    <th>Total</th>
                                </tr>
                            </thead>


                            <tbody>
                                <tr>
                                    <td>ต้นทุน</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>

                            <tbody>
                                <tr>
                                    <td>ต้นทุนย้อนกลับ</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>

                            <tbody>
                                <tr>
                                    <td>กำไร/ขาดทุน</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>

                            <tbody>
                                <tr>
                                    <td>%</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
