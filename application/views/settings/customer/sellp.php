<style>
    h3 {
        color: #000;
    }

    h5 {
        color: red;
        text-align: center;
    }

    .btnadd {
        background: #1d6a96;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnadd:hover {
        background: #85b8cb;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มใบสั่งขาย</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row formbox box1">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">ใบสั่งขาย</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />

                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">เลขที่ใบสั่งขาย
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">วันที่
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">บริษัท
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">โครงการ
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Quo.No.
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control">
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                        <option> </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">PO.
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="row formbox box2">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>ลำดับ</td>
                        <td>ชื่อสินค้า</td>
                        <td>จำนวน</td>
                        <td>ราคา @</td>
                        <td>รวมเป็นเงิน</td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td> </td>
                        <td>
                            <div class="col-md-6 col-sm-6 ">
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <div class="col-md-6 col-sm-6 ">
                                <select class="form-control">
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                    <option> </option>
                                </select>
                            </div>
                        </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="row formbox box3">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>รวมเป็นเงิน</td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td>ภาษีมูลค่าเพิ่ม
                            <input type="radio" class="flat" name="gend" id="genderM" value="M" checked="" required="" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                            : มี VAT
                            <input type="radio" class="flat" name="gend" id="genderF" value="F" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                            : ไม่มี VAT

                        </td>
                        <td>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </td>
                        <td> </td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td>รวมเป็นเงินทั้งสิ้น</td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </tbody>
            </table>
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btn-primary cancelbt" type="button">ยกเลิก</button>
                <button type="button" class="btn btn-labeled btn-success">ตกลง</button>
            </div>
        </div>
    </div>
</div>
