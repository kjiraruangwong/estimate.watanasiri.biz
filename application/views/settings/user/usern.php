<style>
    h3 {
        color: #000;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มผู้ใช้</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row formbox box4" style="margin-bottom:80px">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">ผู้ใช้</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="addUserForm" data-parsley-validate class="form-horizontal form-label-left" action="/settings/user/usern" method="post">
                            <input type="hidden" name="form-submitted" value="1">
                            <input type="hidden" name="user_code" id="user_code" value="1">
                            <?php if (!empty($mode) && $mode == 'edit') { ?>
                                <input type="hidden" name="selected-id" value="<?php if (!empty($record)) echo $record->id; ?>">
                            <?php }?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">รหัสผู้ใช้
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> type="text" id="code" name="code" required="required" class="form-control col-md-7 col-xs-12" value="<?php if (!empty($record)) echo $record->code; ?>" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ชื่อผู้ใช้
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->name; ?>" type="text" id="name" name="name" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="position">ตำแหน่ง
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->position; ?>" id="position" name="position" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> id="password" name="password" value="<?php if (!empty($record)) echo $record->password; ?>" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group" <?php if (!empty($mode) && $mode == 'view'){ ?> style="display: none;" <?php } ?> >
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm_password">Confirm Password
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" id="confirm_password" name="confirm_password" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style="margin-top:10px">
                                    <?php if (!empty($mode) && $mode == 'view') { ?>
                                        <button type="button" class="btn btn-labeled btn-success" onclick="$('input, select, div').each(function() { $(this).removeAttr('required');});location.href = 'usernlist';">ตกลง</button>
                                    <?php } else { ?>
                                        <button class="btn btn-primary cancelbt" onclick="$('input, select, div').each(function() { $(this).removeAttr('required');});location.href = 'usernlist';" type="button">ยกเลิก</button>
                                        <button type="button" id="add_user" onclick="addUser();" class="btn btn-labeled btn-success">ตกลง</button>
                                    <?php } ?>
                                    


                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  //Check Password and Confirm Password is match?
  function addUser(){
    var password = $("#password").val();
    var confirm_password = $("#confirm_password").val();
    if(password == confirm_password){
        $('form#addUserForm').submit();
    }else{
        alert("Password Mismatch!");
    }
    var code = $("#code").val();
    $("#user_code").val(code);
}
</script>