<style>
    h3 {
        color: #000;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

    .btnadd {
        background: #1d6a96;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnadd:hover {
        background: #85b8cb;
    }

    .btnde {
        background: #900000;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnde:hover {
        background: #fd0000;
    }

    .btnedit {
        background: #04a100;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnedit:hover {
        background: #04fc00;
    }
    
    .btncancle {
        background: #bfbfbf;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btncancle:hover {
        background: #e6e6e6;
    }

</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มผู้ใช้</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row formbox box4" style="margin-bottom:80px">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">จัดการสิทธิ์</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="addUserForm" data-parsley-validate class="form-horizontal form-label-left" action="/settings/user/userr" method="post">
                            <input type="hidden" name="form-submitted" value="1">
                            <?php if (!empty($mode) && $mode == 'edit') { ?>
                                <input type="hidden" name="selected-id" value="<?php if (!empty($record)) echo $record->id; ?>">
                                <input type="hidden" id="user_code" name="user_code" value="<?php echo $record->userCode; ?>">
                                <input type="hidden" id="Uname" name="Uname" value="<?php echo $record->userName; ?>">
                            <?php }else{ ?>
                                <input type="hidden" id="user_code" name="user_code" value="1">
                                <input type="hidden" id="get_username" name="get_username" value="0">
                                <input type="hidden" id="Uname" name="Uname" value="1">
                            <?php } ?>    

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_id">รหัสผู้ใช้
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select <?php if (!empty($mode) && $mode == 'view'){ ?>  disabled="disabled" <?php } ?> required="required" class="form-control" name="user_id" id="user_id" onchange="getUserName()">
                                        <option value="">กรุณาเลือกรหัสผู้ใช้</option>
                                        <?php 
                                        foreach($users as $user) {
                                          ?>
                                        <option <?php if (!empty($record) && $record->userId == $user->id){ ?> selected="selected" <?php } ?> value="<?php if (!empty($record) && $record->userId == $user->id){ echo $record->userId; }else{ echo $user->id; } ?>">
                                            <?php if (!empty($record) && $record->userId == $user->id){ echo $record->userCode; }else{ echo $user->code; } ?>
                                                
                                            </option>
                                      <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_name">ชื่อผู้ใช้
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select required="required" disabled="disabled" class="form-control" name="user_name" id="user_name">
                                        <option value="">กรุณาเลือกชื่อผู้ใช้</option>
                                        <?php 
                                        foreach($users as $user) {
                                          ?>
                                        <option <?php if (!empty($record) && $record->userId == $user->id){ ?> selected="selected" <?php } ?> value="<?php if (!empty($record) && $record->userId == $user->id){ echo $record->userId; }else{ echo $user->id; } ?>"><?php if (!empty($record) && $record->userId == $user->id){ echo $record->userName; }else{ echo $user->name; } ?></option>
                                      <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">สิทธิ
                                </label>

                                <div style="margin-top:5px">
                                    <input <?php if (!empty($mode) && $mode == 'view'){ ?>  disabled="disabled" <?php } ?> <?php if (!empty($record) && $record->role == 1) echo "checked" ?> type="radio" class="flat" name="role" id="role1" value="1" checked="" required="" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                                    :ประมาณราคา
                                    <input <?php if (!empty($mode) && $mode == 'view'){ ?>  disabled="disabled" <?php } ?> <?php if (!empty($record) && $record->role == 2) echo "checked" ?> type="radio" class="flat" name="role" id="role2" value="2" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                                    :ประสานงานขาย
                                    <input <?php if (!empty($mode) && $mode == 'view'){ ?>  disabled="disabled" <?php } ?> <?php if (!empty($record) && $record->role == 3) echo "checked" ?> type="radio" class="flat" name="role" id="role3" value="3" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                                    :จัดซื้อ
                                    <input <?php if (!empty($mode) && $mode == 'view'){ ?>  disabled="disabled" <?php } ?> <?php if (!empty($record) && $record->role == 4) echo "checked" ?> type="radio" class="flat" name="role" id="role4" value="4" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                                    :ต้อนทุนย้อนกลับ
                                    <input <?php if (!empty($mode) && $mode == 'view'){ ?>  disabled="disabled" <?php } ?> <?php if (!empty($record) && $record->role == 5) echo "checked" ?> type="radio" class="flat" name="role" id="role5" value="5" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                                    :ผู้อนุมัติ
                                    <input <?php if (!empty($mode) && $mode == 'view'){ ?>  disabled="disabled" <?php } ?> <?php if (!empty($record) && $record->role == 6) echo "checked" ?> type="radio" class="flat" name="role" id="role6" value="6" data-parsley-multiple="gender" style="position: absolute; opacity: 0;">
                                    :Admin
                                </div>

                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style="margin-top:10px">
                                    <?php if (!empty($mode) && $mode == 'view') { ?>
                                        <button type="button" class="btn btn-labeled btn-success" onclick="$('input, select, div').each(function() { $(this).removeAttr('required');});location.href = 'userrlist';">ตกลง</button>
                                    <?php } else { ?>
                                        <?php if (!empty($mode) && $mode == 'edit') { ?>
                                        <button class="btn btnadd" type="submit">อัพเดท</button>
                                    <?php }else{ ?>
                                        <button class="btn btnadd" type="submit">เพิ่ม</button>
                                    <?php } ?>    
                                        <button class="btn btncancle" type="button" onclick="$('input, select, div').each(function() { $(this).removeAttr('required');});location.href = 'userrlist';" >ยกเลิก</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getUserName(){
            var user_code = $("#user_id option:selected").text();
            $("#user_code").val(user_code);
            var user_id = $("#user_id option:selected").val();
            var get_username = 1;
            $.ajax({
                 type: "POST",
                 url: "/settings/user/ajaxUser",
                 dataType: "json",
                 data: {user_id: user_id,get_username: get_username},
                 success: function(response){
                    $("#user_name option[value='"+response[0].id+"']").prop('selected',true);
                    var user_name = $("#user_name option:selected").text();
                    $("#Uname").val(user_name);                 },
                 error: function(){
                    alert('Data not found');
                 },
            });
    }

    
</script>
