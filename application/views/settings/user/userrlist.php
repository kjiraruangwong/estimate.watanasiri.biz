<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ตารางจัดการสิทธิ์ผู้ใช้</h3>
            </div>

        </div>

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div>
                            <h2> รายละเอียดผู้ใช้<span>&nbsp;&nbsp;</span></h2>

                        </div>

                        <div>
                            <a type="button" class="btn btn-sm btn-success" href="/settings/user/userr">เพิ่มผู้ใช้</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>รหัสผู้ใช้</th>
                                    <th>ชื่อผู้ใช้</th>
                                    <th>สิทธิ์</th>
                                    <th></th>
                                </tr>
                            </thead>


                            <tbody>
                                <?php 
                                foreach($records as $record) {
                                    if($record->role == '1'){
                                        $role = 'ประมาณราคา';
                                    }else if($record->role == '2'){
                                        $role = 'ประสานงานขาย';
                                    }else if($record->role == '3'){
                                        $role = 'จัดซื้อ';
                                    }else if($record->role == '4'){
                                        $role = 'ต้นทุนย้อนกลับ';
                                    }else if($record->role == '5'){
                                        $role = 'ผู้อนุมัติ';
                                    }else{
                                        $role = 'Admin';
                                    }
                                   echo "<tr>"; 
                                   echo "<td><a>".  $record->userCode. "</a></td>"; 
                                   echo "<td>". $record->userName. "</td>"; 
                                   echo "<td>". $role. "</td>";    
                                   echo "<td>";
                                   echo "<a href='userr?mode=view&select_id=" . $record->id . "' class='btn btn-primary btn-xs'><i class='fa fa-folder''></i> รายละเอียด </a>";
                                    echo "<a href='userr?mode=edit&select_id=" . $record->id . "' class='btn btn-info btn-xs'><i class='fa fa-pencil''></i> แก้ไข </a>";
                                    echo "<a href='userrlist?del_id=" . $record->id . "' class='btn btn-danger btn-xs'"
                                            . "onclick=\"return confirm('กรุณายืนยันว่าต้องการที่จะลบข้อมูลที่เลือก?');\"\">"
                                            . "<i class='fa fa-trash-o''></i> ลบ </a>";                       
                                   echo "</td>"; 
                                   echo "</tr>"; 
                               }
                               ?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
