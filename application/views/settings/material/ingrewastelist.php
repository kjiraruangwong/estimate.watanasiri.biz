<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ตารางรายการวัสดุสิ้นเปลือง</h3>
            </div>

        </div>

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div>
                            <h2> รายละเอียดวัสดุสิ้นเปลือง<span>&nbsp;&nbsp;</span></h2>

                        </div>

                        <div>                    
                            <button type="button" class="btn btn-sm btn-success" onclick="window.location.href = '/settings/material/ingrewaste'">เพิ่มวัสดุสิ้นเปลือง</button>                                        
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>รหัสวัสดุสิ้นเปลือง</th>
                                    <th>ชื่อวัสดุสิ้นเปลือง</th>
                                    <th>หน่วย</th>
                                    <th>น้ำหนัก</th>
                                    <th>ประเภทต้นทุน</th>
                                    <th>ราคา 1+วันที่</th>
                                    <th>ราคา 2+วันที่</th>
                                    <th>ราคา 3+วันที่</th>
                                    <th>ราคา 4+วันที่</th>
                                    <th>ราคา 5+วันที่</th>
                                    <th></th>
                                </tr>
                            </thead>


                            <tbody>

                                <?php
                                foreach ($records as $record) {
                                    echo "<tr>";
                                    echo "<td><a>" . $record->code . "</a></td>";
                                    echo "<td>" . $record->name . "</td>";
                                    echo "<td>" . $record->unitName . "</td>";
                                    echo "<td>" . number_format($record->weight, 2) . "</td>";
                                    echo "<td>" . $record->costTypeName . "</td>";
                                    echo "<td>" . number_format($record->price1, 2) . " | " . date('d-m-Y', strtotime($record->datePrice1)) . "</td>";
                                    echo "<td>" . number_format($record->price2, 2) . " | " . date('d-m-Y', strtotime($record->datePrice2)) . "</td>";
                                    echo "<td>" . number_format($record->price3, 2) . " | " . date('d-m-Y', strtotime($record->datePrice3)) . "</td>";
                                    echo "<td>" . number_format($record->price4, 2) . " | " . date('d-m-Y', strtotime($record->datePrice4)) . "</td>";
                                    echo "<td>" . number_format($record->price5, 2) . " | " . date('d-m-Y', strtotime($record->datePrice5)) . "</td>";
                                    echo "<td>";
                                    echo "<a href='ingrewaste?mode=view&select_id=" . $record->id . "' class='btn btn-primary btn-xs'><i class='fa fa-folder''></i> รายละเอียด </a>";
                                    echo "<a href='ingrewaste?mode=edit&select_id=" . $record->id . "' class='btn btn-info btn-xs'><i class='fa fa-pencil''></i> แก้ไข </a>";
                                    echo "<a href='ingrewastelist?del_id=" . $record->id . "' class='btn btn-danger btn-xs'"
                                    . "onclick=\"return confirm('กรุณายืนยันว่าต้องการที่จะลบข้อมูลที่เลือก?');\"\">"
                                    . "<i class='fa fa-trash-o''></i> ลบ </a>";
                                    echo "</td>";
                                    echo "</tr>";
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>