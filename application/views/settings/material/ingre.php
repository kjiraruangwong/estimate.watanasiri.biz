<style>
    h3 {
        color: #000;
    }

    h5 {
        color: red;
        text-align: center;
    }

    .btnadd {
        background: #1d6a96;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnadd:hover {
        background: #85b8cb;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มวัตถุดิบ</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/settings/material/ingre" method="post">
            <div class="row formbox box1">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><span class="titleform">วัตถุดิบ</span></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />

                            <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                <input type="hidden" name="form-submitted" value="1">
                                <input type="hidden" name="material_code" id="material_code" value="1">
                                <?php if (!empty($mode) && $mode == 'edit') { ?>
                                    <input type="hidden" name="selected-id" value="<?php if (!empty($record)) echo $record->id; ?>">
                                <?php } ?>                                 

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">รหัสวัตถุดิบ
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" disabled="disabled" id="code" name="code" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->code; ?>">
                                    </div>
                                </div>
                                <h5>กรุณาเลือกประเภทวัตถุดิบ รูปร่างโลหะ ลักษณะพิเศษของโลหะ ขนาดกว้างยาว Dia./หนา เพื่อสร้างรหัสลูกค้าแบบอัตโนมัติ</h5>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="material_type">ประเภทวัตถุดิบ
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <select class="form-control" name="material_type" id="material_type" required <?php if (!empty($mode) && $mode == 'view') echo "disabled" ?>>
                                            <option value="">-- กรุณาเลือก --</option>
                                            <?php
                                            foreach ($material_types as $material_type) {
                                                if (!empty($record) && ($record->materialTypeId == $material_type->id)) {
                                                    echo "<option value='" . $material_type->id . "' selected>" . $material_type->name . "</option>";
                                                } else {
                                                    echo "<option value='" . $material_type->id . "'>" . $material_type->name . "</option>";
                                                }
                                            }
                                            ?>                                                
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metal_shape">รูปร่างโลหะ
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <select class="form-control" name="metal_shape" id="metal_shape" required <?php if (!empty($mode) && $mode == 'view') echo "disabled" ?>>
                                            <option value="">-- กรุณาเลือก --</option>
                                            <?php
                                            foreach ($metal_shapes as $metal_shape) {
                                                if (!empty($record) && ($record->metalShapeId == $metal_shape->id)) {
                                                    echo "<option value='" . $metal_shape->id . "' selected>" . $metal_shape->name . "</option>";
                                                } else {
                                                    echo "<option value='" . $metal_shape->id . "'>" . $metal_shape->name . "</option>";
                                                }
                                            }
                                            ?>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="extra_shape">ลักษณะพิเศษของโลหะ
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <select class="form-control" id="extra_shape" name="extra_shape" required <?php if (!empty($mode) && $mode == 'view') echo "disabled" ?>>
                                            <option value="">-- กรุณาเลือก --</option>
                                            <?php
                                            foreach ($extra_shapes as $extra_shape) {
                                                if (!empty($record) && ($record->extraShapeId == $extra_shape->id)) {
                                                    echo "<option value='" . $extra_shape->id . "' selected>" . $extra_shape->name . "</option>";
                                                } else {
                                                    echo "<option value='" . $extra_shape->id . "'>" . $extra_shape->name . "</option>";
                                                }
                                            }
                                            ?>   
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="width">ขนาดความกว้าง
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="width" name="width" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->width; ?>">
                                    </div>
                                    <label class="control-label">มม.</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="length">ความยาว
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="length" name="length" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->length; ?>">
                                    </div>
                                    <label class="control-label">มม.</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dia_thickness">ขนาด Dia./หนา
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="dia_thickness" name="dia_thickness" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->diaThickness; ?>">
                                    </div>
                                    <label class="control-label">มม.</label>
                                </div>
                                <?php if (empty($mode) || $mode == 'edit') { ?>

                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="button" onclick="autoGenerateMaterialCode();" id="add_material_code" class="btn btnadd">เพิ่มรหัสวัตถุดิบ</button>
                                    </div>

                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row formbox box2">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">

                            <br />
                            <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="material_name">ชื่อวัตถุดิบ
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="material_name" name="material_name" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->name; ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12 " for="unit">หน่วย
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <select class="form-control" id="unit" name="unit" required <?php if (!empty($mode) && $mode == 'view') echo "disabled" ?>>
                                            <option value="">-- กรุณาเลือก --</option>
                                            <?php
                                            foreach ($units as $unit) {
                                                if (!empty($record) && ($record->unitId == $unit->id)) {
                                                    echo "<option value='" . $unit->id . "' selected>" . $unit->name . "</option>";
                                                } else {
                                                    echo "<option value='" . $unit->id . "'>" . $unit->name . "</option>";
                                                }
                                            }
                                            ?>  
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12 " for="cost_type">ประเภทต้นทุน
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <select class="form-control" id="cost_type" name="cost_type" required <?php if (!empty($mode) && $mode == 'view') echo "disabled" ?>>
                                            <option value="">-- กรุณาเลือก --</option>
                                            <?php
                                            foreach ($cost_types as $cost_type) {
                                                if (!empty($record) && ($record->costTypeId == $cost_type->id)) {
                                                    echo "<option value='" . $cost_type->id . "' selected>" . $cost_type->name . "</option>";
                                                } else {
                                                    echo "<option value='" . $cost_type->id . "'>" . $cost_type->name . "</option>";
                                                }
                                            }
                                            ?>  
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="weight">น้ำหนัก
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="weight" name="weight" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->weight; ?>">
                                    </div>
                                    <label class="control-label">กก.</label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="area">พื้นที่
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="area" name="area" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->area; ?>">
                                    </div>
                                    <label class="control-label">ตรม.</label>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price1">ราคา 1
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="price1" name="price1" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->price1; ?>">
                                    </div>
                                    <label class="control-label">บาท</label>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="date_price1">วันที่
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <fieldset>
                                            <div class="control-group ">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control date_price1" name="date_price1" id="single_cal1" placeholder="" aria-describedby="inputSuccess2Status" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->datePrice1; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price2">ราคา 2
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="price2" name="price2" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->price2; ?>">
                                    </div>
                                    <label class="control-label">บาท</label>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="date_price2">วันที่
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <fieldset>
                                            <div class="control-group ">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control date_price2" name="date_price2" id="single_cal2" placeholder="" aria-describedby="inputSuccess2Status" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->datePrice2; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price3">ราคา 3
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="price3" name="price3" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->price3; ?>">
                                    </div>
                                    <label class="control-label">บาท</label>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="date_price3">วันที่
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <fieldset>
                                            <div class="control-group ">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control date_price3" name="date_price3" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->datePrice3; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price4">ราคา 4
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="price4" name="price4" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->price4; ?>">
                                    </div>
                                    <label class="control-label">บาท</label>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="date_price4">วันที่
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <fieldset>
                                            <div class="control-group ">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control date_price4" name="date_price4" id="single_cal4" placeholder="" aria-describedby="inputSuccess2Status" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->datePrice4; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price5">ราคา 5
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="price5" name="price5" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->price5; ?>">
                                    </div>
                                    <label class="control-label">บาท</label>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="date_price5">วันที่
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <fieldset>
                                            <div class="control-group ">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control date_price5" name="date_price5" id="single_cal5" placeholder="" aria-describedby="inputSuccess2Status" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->datePrice5; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 submit-btn">
                                    <?php if (!empty($mode) && $mode == 'view') { ?>
                                        <button type="button" class="btn btn-labeled btn-success" onclick="$('input, select, div').each(function () {
                                                            $(this).removeAttr('required');
                                                        });
                                                        location.href = 'ingrelist';" formnovalidat>ตกลง</button>
                                    <?php } else { ?>
                                        <button type="button" class="btn btn-primary cancelbt" onclick="$('input, select, div').each(function () {
                                                            $(this).removeAttr('required');
                                                        });
                                                        location.href = 'ingrelist';" formnovalidat>ยกเลิก</button>   
                                        <button type="submit" class="btn btn-labeled btn-success">ตกลง</button>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script>
            //Auto Generate Company Code
            function autoGenerateMaterialCode() {
                var material_type = $("#material_type option:selected").val();
                var metal_shape = $("#metal_shape option:selected").val();
                var extra_shape = $("#extra_shape option:selected").val();
                var width = $("#width").val();
                var length = $("#length").val();
                var dia_thickness = $("#dia_thickness").val();
                /*var date = new Date();
                 var current_time = date.getTime();*/
                //if(customer_type_val == '' || first_alphabet_val == ''){
                //alert('กรุณาเลือกประเภทบริษัท / กรุณาเลือกตัวอักษรตัวแรก');
                //}else{
                $("#code").val(material_type + metal_shape + extra_shape + width + length + dia_thickness);
                $("#material_code").val(material_type + metal_shape + extra_shape + width + length + dia_thickness);
                //}
            }
        </script>
