<style>
    h3{color:#000;
    }
    h5{color:red;
       text-align: center;
    }

    .btnadd{background: #1d6a96;
            border-radius: 3px;
            border: 1px solid #1d6a96;
            color: #fff;
    }

    .btnadd:hover{
        background: #85b8cb;
    }
    .top_nav .navbar-right li a{color:#fff!important;}
    .form-horizontal .control-label {color:#000;}
</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มวัสดุสิ้นเปลือง</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        
        <?php if (!empty($_GET['err'])) { ?>
            <div class="alert alert-danger text-center" role="alert">                
                <b>รหัส : <?php echo $_GET['code'] ?> มีอยู่แล้ว กรุณากรอกรหัสใหม่! ( <?php echo $_GET['message'] ?>  )</b>
            </div>
        <?php }?> 
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/settings/material/ingrewaste" method="post">
            <?php if (!empty($mode) && $mode == 'edit') { ?>
                <input type="hidden" name="selected-id" value="<?php if (!empty($record)) echo $record->id; ?>">
            <?php }?>            
            <input type="hidden" name="form-submitted" value="1">
            <input type="hidden" name="waste_code" id="waste_code" value="<?php if (!empty($record)) echo $record->code; ?>">
            <div class="row formbox box1">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><span class="titleform">วัสดุสิ้นเปลือง</span></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">รหัสวัสดุสิ้นเปลือง
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="code" name="code" disabled="disabled" required="required" class="form-control col-md-7 col-xs-12" value="<?php if (!empty($record)) echo $record->code; ?>">
                                    </div>
                                </div>
                                <?php if (empty($mode) || $mode != 'view') { ?>
                                    <h5>กรุณาเลือกประเภทวัตถุดิบ รูปร่างโลหะ ลักษณะพิเศษของโลหะ ขนาดกว้างยาว Dia./หนา เพื่อสร้างรหัสวัสดุสิ้นเปลืองแบบอัตโนมัติ</h5>
                                <?php } ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ประเภท วัสดุสิ้นเปลือง
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <select class="form-control" name="waste_type" id="waste_type" required <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                            <option value="">-- กรุณาเลือก --</option>
                                            <?php
                                            foreach ($material_types as $material_type) {
                                                ?>
                                                <option <?php if (isset($record) && $record->wasteMaterialTypeId == $material_type->id) { ?>selected="selected"<?php } ?> value="<?php echo $material_type->id; ?>"><?php echo $material_type->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="waste_shape">ลักษณะวัสดุสิ้นเปลือง/แผนก
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <select class="form-control" name="waste_shape" id="waste_shape" required <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                            <option value="">-- กรุณาเลือก --</option>
                                            <?php
                                            foreach ($metal_shapes as $metal_shape) {
                                                ?>
                                                <option <?php if (isset($record) && $record->metalShapeId == $metal_shape->id) { ?>selected="selected"<?php } ?> value="<?php echo $metal_shape->id; ?>"><?php echo $metal_shape->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="material_type">ประเภทวัตถุดิบ/ชื่อเฉพาะ
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <select class="form-control" name="material_type" id="material_type" required <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                            <option value="">-- กรุณาเลือก --</option>
                                            <?php
                                            foreach ($material_types as $material_type) {
                                                ?>
                                                <option <?php if (isset($record) && $record->materialTypeId == $material_type->id) { ?>selected="selected"<?php } ?> value="<?php echo $material_type->id; ?>"><?php echo $material_type->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="extra_waste">ลักษณะพิเศษ
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" id="extra_waste" name="extra_waste" required <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                        <option value="">-- กรุณาเลือก --</option>
                                        <?php
                                        foreach ($extra_wastes as $extra_waste) {
                                            ?>
                                            <option <?php if (isset($record) && $record->extraWasteId == $extra_waste->id) { ?>selected="selected"<?php } ?> value="<?php echo $extra_waste->id; ?>"><?php echo $extra_waste->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dia">ขนาด Dia.
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="dia" name="dia" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->dia; ?>">
                                </div>
                                <label class="control-label">มม.</label>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="length">ความยาว
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="length" name="length" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->length; ?>">
                                </div>
                                <label class="control-label">มม.</label>
                            </div>
                            <?php if (empty($mode) || $mode != 'view') { ?>
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="button" onclick="autoGenerateWasteMaterialCode();" id="add_waste_material_code" class="btn btnadd">เพิ่มรหัสวัตถุดิบ</button>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div> 
            </div>
    </div>


    <div class="row formbox box2">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">

                    <br />

                    <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="waste_name">ชื่อวัสดุสิ้นเปลือง
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="waste_name" id="waste_name" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->name; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="cost_type">ประเภทต้นทุน
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <select class="form-control" id="cost_type" name="cost_type" required <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                    <option value="">-- กรุณาเลือก --</option>
                                    <?php
                                    foreach ($cost_types as $cost_type) {
                                        ?>
                                        <option <?php if (isset($record) && $record->costTypeId == $cost_type->id) { ?>selected="selected"<?php } ?> value="<?php echo $cost_type->id; ?>"><?php echo $cost_type->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="unit">หน่วย</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <select class="form-control" id="unit" name="unit" required <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                    <option value="">-- กรุณาเลือก --</option>
                                    <?php
                                    foreach ($units as $unit) {
                                        ?>
                                        <option <?php if (isset($record) && $record->unitId == $unit->id) { ?>selected="selected"<?php } ?> value="<?php echo $unit->id; ?>"><?php echo $unit->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="weight">น้ำหนัก
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="weight" name="weight" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->weight; ?>">
                            </div>
                            <label class="control-label">กก.</label>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price1">ราคา 1
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="price1" name="price1" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->price1; ?>">
                            </div>
                            <label class="control-label">บาท</label>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="date_price1">วันที่
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <fieldset>
                                    <div class="control-group ">
                                        <div class="controls">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                <input type="text" class="form-control date_price1" name="date_price1" id="single_cal1" placeholder="" aria-describedby="inputSuccess2Status" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->datePrice1; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price2">ราคา 2
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="price2" name="price2" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->price2; ?>">
                            </div>
                            <label class="control-label">บาท</label>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="date_price2">วันที่
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <fieldset>
                                    <div class="control-group ">
                                        <div class="controls">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                <input type="text" class="form-control date_price2" name="date_price2" id="single_cal2" placeholder="" aria-describedby="inputSuccess2Status" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->datePrice2; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price3">ราคา 3
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="price3" name="price3" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->price3; ?>">
                            </div>
                            <label class="control-label">บาท</label>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="date_price3">วันที่
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <fieldset>
                                    <div class="control-group ">
                                        <div class="controls">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                <input type="text" class="form-control date_price3" name="date_price3" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->datePrice3; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price4">ราคา 4
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="price4" name="price4" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->price4; ?>">
                            </div>
                            <label class="control-label">บาท</label>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="date_price4">วันที่
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <fieldset>
                                    <div class="control-group ">
                                        <div class="controls">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                <input type="text" class="form-control date_price4" name="date_price4" id="single_cal4" placeholder="" aria-describedby="inputSuccess2Status" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->datePrice4; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price5">ราคา 5
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="price5" name="price5" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->price5; ?>">
                            </div>
                            <label class="control-label">บาท</label>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="date_price5">วันที่
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <fieldset>
                                    <div class="control-group ">
                                        <div class="controls">
                                            <div class="input-prepend input-group">
                                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                <input type="text" class="form-control date_price5" name="date_price5" id="single_cal5" placeholder="" aria-describedby="inputSuccess2Status" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->datePrice5; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <?php if (empty($mode) || $mode != 'view') { ?>
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 submit-btn">
                                <button class="btn btn-primary cancelbt" type="button">ยกเลิก</button>
                                <button type="submit" class="btn btn-labeled btn-success">ตกลง</button>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div> 
        </div>
        </form>

    </div>
</div>

<script>
    //Auto Generate Company Code
    function autoGenerateWasteMaterialCode() {
        var waste_type = $("#waste_type option:selected").val();
        var waste_shape = $("#waste_shape option:selected").val();
        var material_type = $("#material_type option:selected").val();
        var extra_waste = $("#extra_waste option:selected").val();
        var dia = $("#dia").val();
        var length = $("#length").val();

        $("#code").val(waste_type + waste_shape + material_type + extra_waste + dia + length);
        $("#waste_code").val(waste_type + waste_shape + material_type + extra_waste + dia + length);

    }
</script>