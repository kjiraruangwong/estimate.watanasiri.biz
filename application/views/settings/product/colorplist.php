<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ตารางวัตถุดิบทำสี</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div>
                            <h2>รายละเอียดวัตถุดิบทำสี<span>&nbsp;&nbsp;</span></h2>

                        </div>
                        <div>                    
                            <a type="button" class="btn btn-sm btn-success" href="/settings/product/colorp">เพิ่มวัตถุดิบทำสี</a>                                        
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>รหัสวัตถุดิบทำสี</th>
                                    <th>ชื่อวัตถุดิบทำสี</th>
                                    <th>ประเภทวัตถุดิบ</th>
                                    <th>ทำสี (หน้า)</th>
                                    <th>ราคา @</th>
                                    <th>ประเภทต้นทุน</th>
                                    <th>วันที่แก้ไข</th>
                                    <th>วันที่สร้าง</th>                                    
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                foreach ($records as $record) {
                                    echo "<tr>";
                                    echo "<td><a>" . $record->code . "</a></td>";
                                    echo "<td>" . $record->name . "</td>";
                                    echo "<td><a>" . $record->materialType . "</a></td>";
                                    echo "<td>" . $record->colorPage . "</td>";
                                    echo "<td><a>" . $record->price . "</a></td>";
                                    echo "<td>" . $record->costType . "</td>";
                                    echo "<td>" . $record->dateUpdated . "</td>";
                                    echo "<td>" . $record->dateCreated . "</td>";
                                    echo "<td>";
                                    echo "<a href='colorp?mode=view&select_id=" . $record->id . "' class='btn btn-primary btn-xs'><i class='fa fa-folder''></i> รายละเอียด </a>";
                                    echo "<a href='colorp?mode=edit&select_id=" . $record->id . "' class='btn btn-info btn-xs'><i class='fa fa-pencil''></i> แก้ไข </a>";
                                    echo "<a href='colorplist?del_id=" . $record->id . "' class='btn btn-danger btn-xs'"
                                    . "onclick=\"return confirm('กรุณายืนยันว่าต้องการที่จะลบข้อมูลที่เลือก?');\"\">"
                                    . "<i class='fa fa-trash-o''></i> ลบ </a>";
                                    echo "</td>";
                                    echo "</tr>";
                                }
                                ?>                          
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>