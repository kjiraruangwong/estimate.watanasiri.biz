<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ตารางสินค้า</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div>
                            <h2> รายละเอียดสินค้า<span>&nbsp;&nbsp;</span></h2>

                        </div>

                        <div>                    
                            <a type="button" class="btn btn-sm btn-success" href="/settings/product/productde">เพิ่มสินค้า</a>                                        
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>รหัสสินค้า</th>
                                    <th>ชื่อสินค้า</th>
                                    <th>สินค้าหลัก</th>
                                    <th>สินค้าย่อย</th>
                                    <th>Finishing</th>
                                    <th>Picture</th>
                                    <th>วันที่แก้ไข</th>
                                    <th>วันที่สร้าง</th>                           
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($records as $record) {
                                    echo "<tr>";
                                    echo "<td><a>" . $record->code . "</a></td>";
                                    echo "<td>" . $record->name . "</td>";
                                    echo "<td>" . $record->mainProductName . "</td>";
                                    echo "<td>" . $record->subProductName . "</td>";
                                    echo "<td>" . $record->finishingName . "</td>";
                                    echo "<td>not implemented</td>";
                                    echo "<td>" . $record->dateUpdated . "</td>";
                                    echo "<td>" . $record->dateCreated . "</td>";
                                    echo "<td>";
                                    echo "<a href='productde?mode=view&select_id=" . $record->id . "' class='btn btn-primary btn-xs'><i class='fa fa-folder''></i> รายละเอียด </a>";
                                    echo "<a href='productde?mode=edit&select_id=" . $record->id . "' class='btn btn-info btn-xs'><i class='fa fa-pencil''></i> แก้ไข </a>";
                                    echo "<a href='productdelist?del_id=" . $record->id . "' class='btn btn-danger btn-xs'"
                                    . "onclick=\"return confirm('กรุณายืนยันว่าต้องการที่จะลบข้อมูลที่เลือก?');\"\">"
                                    . "<i class='fa fa-trash-o''></i> ลบ </a>"; 
                                    echo "</td>";
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>