<style>
    h3 {
        color: #000;
    }

    h5 {
        color: red;
        text-align: center;
    }

    .btnadd {
        background: #1d6a96;
        border-radius: 3px;
        border: 1px solid #1d6a96;
        color: #fff;
    }

    .btnadd:hover {
        background: #85b8cb;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มสินค้า</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if (!empty($_GET['err'])) { ?>
            <div class="alert alert-danger text-center" role="alert">                
                <b>รหัส : <?php echo $_GET['code'] ?> มีอยู่แล้ว กรุณากรอกรหัสใหม่! ( <?php echo $_GET['message'] ?>  )</b>
            </div>
        <?php }?> 
        <?php if (!empty($_GET['errUpload'])) { ?>
            <div class="alert alert-danger text-center" role="alert">                
                <b>การ Upload รูปภาพห้ามมีขนาดเกิน 400x264! ( <?php echo $_GET['message'] ?>  )</b>
            </div>
        <?php }?>         

        <div class="row formbox box1">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">สินค้า</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />

                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/settings/product/productde" method="post" enctype="multipart/form-data">
                            <?php if (!empty($mode) && $mode == 'edit') { ?>
                                <input type="hidden" name="selected-id" value="<?php if (!empty($record)) echo $record->id; ?>">
                            <?php } ?>                              
                            <input type="hidden" name="form-submitted" value="1">
                            <input type="hidden" name="name" id="name" value="1">
                            <input type="hidden" name="finishingId" id="finishingId" value="1">
                            <input name="product_image" id="product_image" onchange="$('#file_path').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">รหัสสินค้า
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="code" name="code" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->code; ?>">
                                </div>
                            </div>
                            <?php if (empty($mode) || $mode != 'view') { ?>
                                <h5>กรุณาเลือกประเภทงาน ประเภทวัสดุ ชื่อสินค้าหลัก ชื่อสินค้าย่อย ขนาด กว้าง ยาว สูง/หนา เพื่อสร้างรหัสสินค้าแบบอัตโนมัติ</h5>
                            <?php } ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ประเภทงาน
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" name="customerTypeId" required <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                        <option value="">-- กรุณาเลือก --</option>

                                        <?php
                                        foreach ($customerTypes as $customerType) {
                                            ?>
                                            <option <?php if (isset($record) && $record->customerTypeId == $customerType->id) { ?>selected="selected"<?php } ?> value="<?php echo $customerType->id; ?>"><?php echo $customerType->name; ?></option>
                                        <?php } ?>                                        

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ประเภทวัตถุดิบ
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control"  name="materialTypeId" required <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                        <option value="">-- กรุณาเลือก --</option>
                                        <?php
                                        foreach ($materialTypes as $materialType) {
                                            ?>
                                            <option <?php if (isset($record) && $record->materialTypeId == $materialType->id) { ?>selected="selected"<?php } ?> value="<?php echo $materialType->id; ?>"><?php echo $materialType->name; ?></option>
                                        <?php } ?> 
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อสินค้าหลัก
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control"  name="mainProductId" required <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                        <option value="">-- กรุณาเลือก --</option>
                                        <?php
                                        foreach ($mainProducts as $mainProduct) {
                                            ?>
                                            <option <?php if (isset($record) && $record->mainProductId == $mainProduct->id) { ?>selected="selected"<?php } ?> value="<?php echo $mainProduct->id; ?>"><?php echo $mainProduct->name; ?></option>
                                        <?php } ?> 
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อสินค้าย่อย
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select class="form-control" name="subProductId" required <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                        <option value="">-- กรุณาเลือก --</option>
                                        <?php
                                        foreach ($subProducts as $subProduct) {
                                            ?>
                                            <option <?php if (isset($record) && $record->subProductId == $subProduct->id) { ?>selected="selected"<?php } ?> value="<?php echo $subProduct->id; ?>"><?php echo $subProduct->name; ?></option>
                                        <?php } ?> 
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ความกว้าง
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  id="width" name="width"  required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->width; ?>">
                                </div>
                                <label class="control-label">มม.</label>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ความยาว
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="height" name="height"  required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->height; ?>">
                                </div>
                                <label class="control-label">มม.</label>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ความสูง/หนา
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="depth" name="depth" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->depth; ?>">
                                </div>
                                <label class="control-label">มม.</label>
                            </div>

                        </form>
                        <?php if (empty($mode) || $mode != 'view') { ?>
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="button" class="btn btnadd">เพิ่มรหัสสินค้า</button>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row formbox box2">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <br />
                    <form data-parsley-validate class="form-horizontal form-label-left"  enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อสินค้า
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="name_textbox" name="name_textbox" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->name; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="first-name">Finishing
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <select class="form-control" name="finishingId_select" id="finishingId_select" required <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?>>
                                    <option value="">-- กรุณาเลือก --</option>
                                    <?php
                                    foreach ($finishings as $finishing) {
                                        ?>
                                        <option <?php if (isset($record) && $record->finishingId == $finishing->id) { ?>selected="selected"<?php } ?> value="<?php echo $finishing->id; ?>"><?php echo $finishing->name; ?></option>
                                    <?php } ?> 
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-1 " for="first-name">รูป
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                
                                <div class="input-group ">
                                    <span class="input-group-btn">
                                        <span class="btn btn-primary" <?php if (!empty($mode) && $mode == 'view'){ echo "disabled";}else{ ?> onclick="$('#product_image').click();" <?php } ?>>Browse</span>
                                        
                                    </span>
                                    <span class="form-control" id="file_path" name="file_path"></span>
                                </div>
                                 <?php if (isset($record) && file_exists("./assets/images/uploads/" . $record->code . ".jpg")) { ?>
                                    <div>
                                        <a href="#" id="pop" onclick="showImage();">
                                            <img id="imageresource" src="/assets/images/uploads/<?php echo $record->code;?>.jpg?dummy=<?php echo rand();?>" class="img-thumbnail" style="width:30%;cursor:zoom-in">
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                            
                        </div>

                        <?php if (empty($mode) || $mode != 'view') { ?>
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button class="btn btn-primary cancelbt" type="button">ยกเลิก</button>
                                <button type="button" class="btn btn-labeled btn-success" onclick="submitform()">ตกลง</button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Image preview</h4>
      </div>
      <div class="modal-body">
        <img src="" id="imagepreview" width="100%" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>

    function submitform() {
        $("#name").val($("#name_textbox").val());
        $("#finishingId").val($("#finishingId_select").val());
        $("#demo-form2").submit();
    }
    
    function showImage(){
        
        $('#imagepreview').attr('src', $('#imageresource').attr('src')); // here asign the image to the modal when the user click the enlarge link
        $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
     }
</script>
