<style>
    h3 {
        color: #000;
    }

    .top_nav .navbar-right li a {
        color: #fff !important;
    }

    .form-horizontal .control-label {
        color: #000;
    }

</style>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ฟอร์มประเภทสินค้าหลัก</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row formbox box4" style="margin-bottom:80px">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><span class="titleform">ประเภทสินค้าหลัก</span></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="/settings/product/mainproduct" method="post">
                            <?php if (!empty($mode) && $mode == 'edit') { ?>
                                <input type="hidden" name="selected-id" value="<?php if (!empty($record)) echo $record->id; ?>">
                            <?php }?>
                            <input type="hidden" name="form-submitted" value="1">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">รหัสประเภทบริษัท/งาน
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="code" name="code" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->code; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">ชื่อประเภทบริษัท/งาน</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="name" name="name" required="required" class="form-control col-md-7 col-xs-12" <?php if (!empty($mode) && $mode == 'view') echo "readonly" ?> value="<?php if (!empty($record)) echo $record->name; ?>">
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style="margin-top:10px">
                                        <?php if (!empty($mode) && $mode == 'view') { ?>
                                            <button type="button" class="btn btn-labeled btn-success" onclick="$('input, select, div').each(function() { $(this).removeAttr('required');});location.href = 'mainproductlist';">ตกลง</button>
                                        <?php } else { ?>
                                            <button type="button" class="btn btn-primary cancelbt" onclick="$('input, select, div').each(function() { $(this).removeAttr('required');});location.href = 'mainproductlist';">ยกเลิก</button>   
                                            <button type="submit" class="btn btn-labeled btn-success">ตกลง</button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
