<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>ตารางค่าแรง</h3>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <div>
                      <h2>รายละเอียดค่าแรง<span>&nbsp;&nbsp;</span></h2>
                        
                    </div>
                 
                    <div>                    
                      <a type="button" class="btn btn-sm btn-success" href="/settings/product/laborc">เพิ่มรายการค่าแรง</a>                                        
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>รหัสค่าแรง</th>
                          <th>ชื่อค่าแรง</th>
                          <th>ประเภทค่าแรง</th>
                          <th>หน่วย</th>
                          <th>ราคา @</th>
                          <th>ประเภทต้นทุน</th>
                          <th>วันที่แก้ไข</th>
                          <th>วันที่สร้าง</th>                            
                          <th></th>
                        </tr>
                      </thead>


                      <tbody>
                        <?php foreach($records as $record) {
                            
                            echo "<tr>";
                            echo "<td><a>" . $record->code . "</a></td>";
                            echo "<td>" . $record->name . "</td>";
                            echo "<td><a>" . $record->laborCostType . "</a></td>";
                            echo "<td>" . $record->unit . "</td>";
                            echo "<td><a>" . $record->price . "</a></td>";
                            echo "<td>" . $record->costType . "</td>";                                    
                            echo "<td>" . $record->dateUpdated . "</td>";
                            echo "<td>" . $record->dateCreated . "</td>";
                            echo "<td>";
                            echo "<a href='laborc?mode=view&select_id=" . $record->id . "' class='btn btn-primary btn-xs'><i class='fa fa-folder''></i> รายละเอียด </a>";
                            echo "<a href='laborc?mode=edit&select_id=" . $record->id . "' class='btn btn-info btn-xs'><i class='fa fa-pencil''></i> แก้ไข </a>";
                            echo "<a href='laborclist?del_id=" . $record->id . "' class='btn btn-danger btn-xs'"
                            . "onclick=\"return confirm('กรุณายืนยันว่าต้องการที่จะลบข้อมูลที่เลือก?');\"\">"
                            . "<i class='fa fa-trash-o''></i> ลบ </a>";
                            echo "</td>";
                            echo "</tr>";                            
                            
                       } ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>